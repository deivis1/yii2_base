<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */
/* @var $name string */
/* @var $startText string */
/* @var $actionText string */
/* @var $actionUrl string */
/* @var $endText string */
/* @var $sender string */

?>
<small><font face="Tahoma" color="#333">
<?php echo Yii::t('app', 'EMAIL_CONGRATULATION', ['name' => $name]); ?><br>
<br>
<?php echo $startText . Html::a($actionText, $actionUrl) . $endText; ?><br>
<br>
<br>
<?php echo Yii::t('app', 'EMAIL_REGARDS'); ?><br>
<?php echo $sender ?><br>
</font></small>