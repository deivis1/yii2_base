#!/usr/bin/env php
<?php

// use app\components\IoServer;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use app\components\Messenger;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/config/console.php';
$application = new yii\console\Application($config);

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Messenger()
        )
    ),
    8080
);

$server->run();