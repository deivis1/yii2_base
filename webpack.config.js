var path = require('path')
var webpack = require('webpack')

module.exports = {
  entry: {
      'code': './vue_src/code.js',
      'category': './vue_src/category.js',
      'demo': './vue_src/demo.js',
      'password-reset': './vue_src/password-reset.js',
      'password-reset-request': './vue_src/password-reset-request.js',
      'email-verification-form': './vue_src/email-verification-form.js',
      'contact': './vue_src/contact.js',
      'signup': './vue_src/signup.js',
      'account': './vue_src/account.js',
      'login': './vue_src/login.js',
      'index': './vue_src/index.js',
      'shared': './vue_src/shared.js'
  },
  output: {
    path: path.resolve(__dirname, './web/js'),
    filename: 'vue-[name].js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ],
      },      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: false
  },
  devtool: '#eval-source-map'
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
}
