<?php
namespace app\components;

use Ratchet\MessageComponentInterface;
use React\EventLoop\Factory as LoopFactory;
use React\Socket\SecureServer as SecureReactor;
use Ratchet\Server\IoServer as IoServerParent;

/**
 * Creates an open-ended socket to listen on a port for incoming connections.
 * Events are delegated through this to attached applications
 */
class IoServer extends IoServerParent {
    /**
     * @param  \Ratchet\MessageComponentInterface $component  The application that I/O will call when events are received
     * @param  int                                $port       The port to server sockets on
     * @param  string                             $address    The address to receive sockets on (0.0.0.0 means receive connections from any)
     * @return IoServer
     */
    public static function factory(MessageComponentInterface $component, $port = 80, $address = '0.0.0.0') {
        $loop = LoopFactory::create();
        $socket = new SecureReactor($address . ':' . $port, $loop, [
            'local_cert' => '/etc/letsencrypt/live/deividas.me/fullchain.pem',
            'allow_self_signed' => true,
            'verify_peer' => false
        ]);

        return new static($component, $socket, $loop);
    }
}
