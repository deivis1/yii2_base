<?php
namespace app\components;

use yii\web\ErrorAction as ErrorActionBase;
use app\models\BlockedIp;

class ErrorAction extends ErrorActionBase {
  
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        
        if ($this->exception->statusCode === 404 && YII_ENV === 'prod') {
            BlockedIp::log();
        }
    }
}
