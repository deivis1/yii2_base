<?php

namespace app\components;

use Yii;
use yii\i18n\PhpMessageSource as VendorPhpMessageSource;

class PhpMessageSource extends VendorPhpMessageSource
{
    public static function getMessages($category)
    {
        $model = new static;
        return $model->loadMessages($category, Yii::$app->language);
    }
}
