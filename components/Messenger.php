<?php
namespace app\components;

use Ratchet\MessageComponentInterface;
use Ratchet\WebSocket\WsServerInterface;
use Ratchet\ConnectionInterface;
use app\models\Code;

class Messenger implements MessageComponentInterface, WsServerInterface {
    const TYPE_SEARCH = 0;
  
    protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        echo "Websocket server started\n";
        echo "Waiting for connections...\n";
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $sender, $message) {
        $data = json_decode($message, true);
        echo "Message from {$sender->resourceId} received: {$message} \n";
        
        $searchData = $data['data'];
        
        $results = Code::getSearchResults($searchData);
        
        switch ($data['type']) {
            case self::TYPE_SEARCH:
                $response = [
                    'type' => $data['type'],
                    'data' => $results,
                ];
                $sender->send(json_encode($response));
                break;
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Resource id {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
    
    public function broadcast(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send($msg);
            }
        }
    }
    
    /**
     * If any component in a stack supports a WebSocket sub-protocol return each supported in an array
     * @return array
     * @todo This method may be removed in future version (note that will not break code, just make some code obsolete)
     */
    public function getSubProtocols()
    {
        return ['myProtocol'];
    }
}