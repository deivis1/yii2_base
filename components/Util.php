<?php
namespace app\components;

use Yii;
use yii\bootstrap4\Html;
use yii\base\ErrorException;
use \PHPMailer\PHPMailer\PHPMailer;

class Util
{
    /**
     * Check whether full height parameter is set
     * 
     * @param View $view
     * @return boolean
     */
    public static function isFullHeight($view)
    {
        return !isset($view->params['fullHeight']) || 
            $view->params['fullHeight'] === true;
    }
    
    /**
     * Return menu active class string for specified route
     * 
     * @param string $route
     * @return string
     */
    public static function getMenuActive($view, $route)
    {
        return $view->context->route == $route ? ' active' : '';
    }
    
    /**
     * Return hidden CSRF input field
     * 
     * @return string
     */
    public static function getCSRFField()
    {
        return Html::hiddenInput(
            Yii::$app->request->csrfParam,
            Yii::$app->request->getCsrfToken()
        );
    }
    
    /**
     * Sends email
     * 
     * @param string $view
     * @param array $params
     * @param string $receiver Receiver email address
     * @param text $subject
     * @return boolean whether message was successfully sent
     */
    public static function sendEmail($view, $params, $receiver, $subject)
    {
        try {
            $result = Util::sendMail([
                'email' => $receiver,
                'name' => '',
                'replyEmail' => '',
                'replyName' => '',
                'subject' => $subject,
                'message' => Yii::$app->view->render(
                    '@app/mail/user-security', $params),
            ]);
            
//            $result = Yii::$app->mailer->compose($view, $params)
//                ->setTo($receiver)
//                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
//                ->setSubject($subject)
//                ->send();
        } catch (ErrorException $e) {
            $result = static::sendEmail($view, $params, $receiver, $subject);
        }
        return $result;
    }
    
    /**
     * Encrypts and encodes string
     * 
     * @param string $data
     * @return string
     */
    public static function encrypt($data)
    {
        return base64_encode(Yii::$app->security->encryptByKey(
            $data,
            Yii::$app->request->cookieValidationKey
        ));
    }
    
    /**
     * Decodes and decrypts string
     * 
     * @param string $data
     * @return string
     */
    public static function decrypt($data)
    {
        return Yii::$app->security->decryptByKey(
            base64_decode($data),
            Yii::$app->request->cookieValidationKey
        );
    }
    
    /**
     * Sets language param from session variable
     */
    public static function setLanguage()
    {
        if (Yii::$app->session->has('lang')) {
            Yii::$app->language = Yii::$app->session->get('lang');
        } else {
            Yii::$app->language = 'en';
        }
    }
    
    /**
     * Checks whether user needs to be shown email verification message
     * 
     * @return boolean
     */
    public static function redirectVerifyEmail()
    {
        return !Yii::$app->user->isGuest && 
            !Yii::$app->user->identity->isEmailVerified();
    }
    
    /**
     * Returns logged in user
     * 
     * @return User|null
     */
    public static function currentUser()
    {
        return Yii::$app->user->isGuest ? null : Yii::$app->user->identity;
    }
    
    /**
     * Returns logged in user's id
     * 
     * @param integer|null $guestUserId Id to return for guest user
     * @return integer|null
     */
    public static function currentUserId($guestUserId = null)
    {
        return Yii::$app->user->isGuest ? 
            $guestUserId : Yii::$app->user->identity->id;
    }
    
    /**
     * Returns escaped code string to avoid json parsing errors
     * 
     * @param string $code
     * @return string
     */
    public static function stringifyCode($code)
    {
        $result = addslashes(json_encode($code));
        return $result;
    }
    
    /**
     * Prints icon svg element
     * 
     * @param string $icon
     * @return string
     */
    public static function icon($icon)
    {
        switch ($icon) {
          case 'user':
              $element = '<svg class="svg-inline--fa fa-user fa-w-14" aria-hidden="true" data-prefix="fa" data-icon="user" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="" style="width: 14px; height: 16px;">' .
                  '<path fill="currentColor" d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"></path>' . 
              '</svg>';
              break;
          case 'lock':
              $element = '<svg class="svg-inline--fa fa-lock fa-w-14" aria-hidden="true" data-prefix="fa" data-icon="lock" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="" style="width: 14px; height: 16px;">' .
                  '<path fill="currentColor" d="M400 224h-24v-72C376 68.2 307.8 0 224 0S72 68.2 72 152v72H48c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V272c0-26.5-21.5-48-48-48zm-104 0H152v-72c0-39.7 32.3-72 72-72s72 32.3 72 72v72z"></path>' .
              '</svg>';
              break;
          case 'check':
              $element = '<svg class="svg-inline--fa fa-check fa-w-16" aria-hidden="true" data-prefix="fa" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="" style="width: 16px; height: 16px; vertical-align: -.125em;">' . 
                  '<path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path>' .
              '</svg>';
              break;
          default:
              $element = '';
        }
        return $element;
    }
    
    /**
     * Sets language by specified token
     */
    public static function setLanguageByToken($token)
    {
        $languageId = substr($token, -1);
        $lang = User::getLanguageNameById($languageId);
        Yii::$app->session->set('lang', $lang);
    }
    
    /**
     * Sends email with PhpMailer
     * 
     * @param array $data
     * @return boolean whether send success
     */
    public static function sendMail($data)
    {   
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->IsSMTP();
        $mail->Mailer = "smtp";

        $mail->SMTPDebug  = 0;  
        $mail->SMTPAuth   = TRUE;
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;
        $mail->Host       = "smtp-mail.outlook.com";
        $mail->Username   = Yii::$app->params['senderEmail'];
        $mail->Password   = Yii::$app->params['senderPassword'];

        $mail->IsHTML(true);
        $mail->AddAddress($data['email'], $data['name']);
        $mail->addReplyTo($data['replyEmail'], $data['replyName']);
        $mail->SetFrom(
            Yii::$app->params['senderEmail'],
            Yii::$app->params['senderName']
        );
        $mail->Subject = $data['subject'];
        $mail->MsgHTML($data['message']);

        return $mail->Send();
    }
}
