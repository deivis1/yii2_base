<?php

return [
    'MODAL_TITLE_CONFIRM_DELETE' => 'Confirm delete',
    'MODAL_TEXT_CONFIRM_DELETE_CODE' => 'Do you want to remove code',
    'MODAL_TEXT_CONFIRM_DELETE_CATEGORY' => 'Do you want to remove category',
    'MODAL_BUTTON_DELETE' => 'Delete',
    'MODAL_BUTTON_CANCEL' => 'Cancel',
    'MODAL_BUTTON_CONTINUE' => 'Continue',
    'MODAL_BUTTON_OK' => 'OK',
    'MODAL_TITLE_SHARE_CODE' => 'Code sharing',
    'MODAL_BUTTON_SHARE' => 'Share',
    'MODAL_BUTTON_STOP_SHARING' => 'Stop sharing',
    'MODAL_TITLE_UNSAVED_CHANGES' => 'Unsaved changes',
    'MODAL_TEXT_CONFIRM_UNSAVED_CHANGES' => 'You have unsaved changes. Do you want to continue?',
    'NOTICE_CREATED' => 'Created',
    'NOTICE_UPDATED' => 'Updated',
    'NOTICE_UPDATE_FAILED' => 'Update failed',
    'NOTICE_DELETED' => 'Deleted',
    'NOTICE_PLEASE_SELECT_DELETE_ITEM' => 'Please select item to delete',
    'NOTICE_SEARCH_IS_NOT_WORKING' => 'Search is not working',
    'FILTER_KEYWORDS_NONE' => 'none',
    'PASSWORD_REQUEST_SENT_TEXT' => "Please check your email, in the message click on password reset link",
    'VERIFICATION_EMAIL_SEND_SUCCESS' => "Verification email has been sent",
    'VERIFICATION_EMAIL_SEND_ERROR' => "Verification email sending failed",
    'EMAIL_SEND_SUCCESS' => "Email has been successfully sent",
    'EMAIL_SEND_FAILED' => 'Email send failed',
    'ACCOUNT_UPDATE_SUCCESS' => 'Account has been updated',
    'PASSWORD_CHANGE_SUCCESS' => 'Password has been changed',
    'PASSWORD_CHANGE_FAILED' => 'Password was not changed',
    'ACCOUNT_UPDATE_FIELDS_MUST_BE_FILLED' => 'All fields must be filled',
    'COPY_CODE_TO_CLIPBOARD_SUCCESS' => 'Code copied to clipboard',
    'COPY_SHARE_LINK_TO_CLIPBOARD' => 'Share link copied',
    'MESSAGE_CODE_WAS_NOT_SHARED' => 'Code was not shared',
    'SHARING_NOT_AVAILABLE_IN_DEMO' => 'Sharing is not available in demo mode',
    'MODAL_CATEGORY_IS_USED' => 'There are codes with this category.',
    'MODAL_TITLE_CREATE_CATEGORY' => 'Create category',
    'MODAL_TITLE_EDIT_CATEGORY' => 'Edit category',
    'MODAL_CATEGORY_NAME_EXISTS' => 'Such category name already exists',
    'MODAL_CATEGORY_NAME_EMPTY' => 'Category name cannot be empty',
];