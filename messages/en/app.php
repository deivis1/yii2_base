<?php

return [
    'TITLE_LOGIN' => 'Login',
    'TITLE_CONTACT' => 'Contact',
    'TITLE_NEW_ACCOUNT' => 'New account',
    'TITLE_ACCOUNT' => 'Account',
    'TITLE_CODE' => 'Code',
    'TITLE_CATEGORY' => 'Category',
    'TITLE_PASSWORD_RESET_REQUEST' => 'Password recovery',
    'TITLE_PASSWORD_RESET' => 'Password change',
    'TITLE_EMAIL_VERIFICATION' => 'Please verify your email',
    'TITLE_PRIVACY_POLICY' => 'Privacy Policy',
    'TITLE_SHARED_CODE' => 'Shared Code',

    'MENU_HOME' => 'Home',
    'MENU_ABOUT' => 'About',
    'MENU_CONTACT' => 'Contact',
    'MENU_CODE' => 'Code',
    'MENU_LANGUAGE' => 'Language',
    'MENU_LANGUAGE_EN' => 'English',
    'MENU_LANGUAGE_LT' => 'Lithuanian',
    'MENU_LOGIN' => 'Login',
    'MENU_SIGNUP' => 'Sign up',
    'MENU_MY_ACCOUNT' => 'My account',
    'MENU_LOGOUT' => 'Logout',

    'ACCOUNT_TAB_PROFILE' => 'Profile',
    'ACCOUNT_TAB_CHANGE_PASSWORD' => 'Change password',

    'LANGUAGE_EN' => 'English',
    'LANGUAGE_LT' => 'Lithuanian',

    'USER_LABEL_ID' => 'Id',
    'USER_LABEL_EMAIL' => 'Email',
    'USER_LABEL_NAME' => 'Name',
    'USER_LABEL_CURRENT_PASSWORD' => 'Current',
    'USER_LABEL_PASSWORD' => 'Password',
    'USER_LABEL_PASSWORD_CONFIRM' => 'Confirm password',
    'USER_LABEL_VERIFY_CODE' => 'Verify code',
    'USER_LABEL_VERIFY_EMAIL_TOKEN' => 'Verify email token',
    'USER_LABEL_AUTH_KEY' => 'Authorization key',
    'USER_LABEL_PASSWORD_HASH' => 'Password hash',
    'USER_LABEL_PASSWORD_RESET_TOKEN' => 'Password reset token',
    'USER_LABEL_ROLE' => 'Role',
    'USER_LABEL_LOCKED' => 'Locked',
    'USER_LABEL_LANGUAGE' => 'Language',
    'USER_LABEL_CREATED_AT' => 'Created at',
    'USER_LABEL_UPDATED_AT' => 'Updated at',

    'USER_LABEL_NEW_PASSWORD' => 'New password',
    'USER_LABEL_CONFIRM_NEW_PASSWORD' => 'Confirm',

    'USER_NAME_INVALID_PATTERN' => 'Name must contain letters only.',
    'PASSWORD_CONFIRMATION_DOES_NOT_MATCH' => '„{attribute}“ must match password.',
    'NEW PASSWORD_CONFIRMATION_DOES_NOT_MATCH' => '„Confirm“ must match new password.',

    'PASSSWORD_RESET_REQUEST_TEXT' => 'Please enter account email address',

    'BUTTON_OK' => 'OK',
    'BUTTON_SIGNUP' => 'Signup',
    'BUTTON_SEND' => 'Send',
    'BUTTON_RESEND' => 'Resend',
    'BUTTON_BACK' => 'Back',
    'BUTTON_CHANGE' => 'Change',
    'BUTTON_GET_STARTED' => 'Get started',
    'BUTTON_SUBMIT' => 'Submit',
    'BUTTON_UPDATE' => 'Update',
    'BUTTON_RESET' => 'Reset',
    'BUTTON_TRY_DEMO' => 'Try demo',

    'CATEGORY_LABEL_ID' => 'Id',
    'CATEGORY_LABEL_USER_ID' => 'User',
    'CATEGORY_LABEL_NAME' => 'Name',
    'CATEGORY_LABEL_ORDER' => 'Order',
    'CATEGORY_LABEL_CREATED_AT' => 'Created at',
    'CATEGORY_LABEL_UPDATED_AT' => 'Updated at',

    'CATEGORY_CODE_LABEL_ID' => 'Id',
    'CATEGORY_CODE_LABEL_CATEGORY_ID' => 'Category',
    'CATEGORY_CODE_LABEL_CODE_ID' => 'Code',
    'CATEGORY_CODE_LABEL_CREATED_AT' => 'Created at',
    'CATEGORY_CODE_LABEL_UPDATED_AT' => 'Updated at',

    'CODE_LABEL_ID' => 'Id',
    'CODE_LABEL_USER_ID' => 'User id',
    'CODE_LABEL_DESCRIPTION' => 'Description',
    'CODE_LABEL_KEYWORDS' => 'Keywords',
    'CODE_LABEL_CODE' => 'Code',
    'CODE_LABEL_LANGUAGE_ID' => 'Language',
    'CODE_LABEL_SHARE_TOKEN' => 'Share token',
    'CODE_LABEL_CREATED_AT' => 'Created at',
    'CODE_LABEL_UPDATED_AT' => 'Updated at',

    'LANGUAGE_LABEL_ID' => 'Id',
    'LANGUAGE_LABEL_NAME' => 'Name',
    'LANGUAGE_LABEL_LANGUAGE_ID' => 'Language',
    'LANGUAGE_LABEL_ORDER' => 'Order',
    'LANGUAGE_LABEL_CREATED_AT' => 'Created at',
    'LANGUAGE_LABEL_UPDATED_AT' => 'Updated at',

    'CONTACT_FORM_LABEL_EMAIL' => 'Email',
    'CONTACT_FORM_LABEL_NAME' => 'Name',
    'CONTACT_FORM_LABEL_SUBJECT' => 'Subject',
    'CONTACT_FORM_LABEL_MESSAGE' => 'Message',
    'CONTACT_FORM_LABEL_VERIFY_CODE' => 'Verification code',

    'CONTACT_SUBJECT_GUEST_VISITOR_PREFIX' => 'Guest visitor message: ',
    'CONTACT_SUBJECT_LOGGED_IN_VISITOR_PREFIX' => 'Logged in visitor message: ',

    'MODEL_NOT_FOUND' => 'Model {model} not found',
    'MODEL_INVALID_ID' => 'Invalid Id',
    'MODEL_INVALID_EMAIL' => 'Invalid email address',
    'MODEL_INVALID_USER_ID' => 'Invalid user id',
    'USER_INVALID_CURRENT_PASSWORD' => 'Incorrect current password.',

    'LOGIN_FORM_LABEL_USERNAME' => 'Email',
    'LOGIN_FORM_LABEL_PASSWORD' => 'Password',
    'LOGIN_FORM_LABEL_REMEMBER_ME' => 'Remember me',
    'LOGIN_FORM_INVALID_INCORRECT_USERNAME_OR_PASSWORD' => 'Incorrect username or password.',

    'USER_LOG_LABEL_ID' => 'Id',
    'USER_LOG_LABEL_USER_ID' => 'User',
    'USER_LOG_LABEL_IP' => 'IP address',
    'USER_LOG_LABEL_ACTION_ID' => 'Action',
    'USER_LOG_LABEL_CREATED_AT' => 'Created at',
    'USER_LOG_LABEL_UPDATED_AT' => 'Updated at',

    'USER_LOG_ACTION_LABEL_ID' => 'Id',
    'USER_LOG_ACTION_LABEL_NAME' => 'Name',
    'USER_LOG_ACTION_LABEL_CREATED_AT' => 'Created at',
    'USER_LOG_ACTION_LABEL_UPDATED_AT' => 'Updated at',

    'TEXT_SIGNIN' => 'Sign in',
    'LOGIN_EMAIL_PLACEHOLDER' => 'Email',
    'BUTTON_LOGIN' => 'Login',
    'LINK_FORGOT_PASSWORD' => 'Forgot password?',

    'MESSAGE_CODE_ADDED' => 'Code added',
    'TEXT_FOOTER' => 'DekaSoft',

    'BAD_POST_REQUEST' => 'Bad post request',
    'BAD_REQUEST_ARGUMENT' => 'Bad post argument',
    'ERROR_WRONG_OWNER' => 'Wrong owner',
    'INVALID_CODE_SHARE_LINK' => 'Invalid link',
    'CATEGORY_NAME_EXISTS' => 'Such category name already exists',

    'LABEL_SEARCH' => 'Search',
    'LABEL_FILTER_CATEGORIES' => 'Filter categories',
    'LABEL_CATEGORIES' => 'Categories',
    'SELECT_PLACEHOLDER' => ' ',
    'SEARCH_CRITERIA_INFO' => 'Change search criteria to see item in list',
    'LABEL_INPUT_DESCRIPTION' => 'Description',
    'LABEL_INPUT_KEYWORDS' => 'Keywords',
    'LABEL_KEYWORDS' => 'Keywords',
    'LABEL_UPDATED' => 'Updated',
    'LABEL_SHARED' => 'Shared',
    'LABEL_COPY_LINK_TO_CLIPBOARD' => 'Copy link to clipboard',
    'LABEL_SHARED_CODE_AUTHOR' => 'Shared by',
    'BUTTON_TITLE_SAVE' => 'Save',
    'BUTTON_TITLE_CREATE_NEW' => 'Create new',
    'BUTTON_TITLE_REMOVE' => 'Remove',
    'BUTTON_TITLE_COPY_TO_CLIPBOARD' => 'Copy code to clipboard',
    'BUTTON_TITLE_SHARE' => 'Share',
    'BUTTON_TITLE_EDIT' => 'Edit',
    'BUTTON_TITLE_SORT_UP' => 'Move up',
    'BUTTON_TITLE_SORT_DOWN' => 'Move down',
    'BUTTON_EDIT_CATEGORIES' => 'Edit categories',
    'LINK_GO_BACK_TO_CODE' => 'go back to code',

    'HOME_HEADER' => 'Code snippet manager',
    'HOME_FEATURE_1' => 'All code in one place',
    'HOME_FEATURE_2' => 'Easy to use <a href="https://ace.c9.io/" target="_blank" class="btn-link">Ace</a> editor',
    'HOME_FEATURE_3' => 'Fast search by keywords, categories',
    'HOME_FEATURE_4' => 'Share code with your friends and colleagues',
    'HOME_FEATURE_5' => 'View code changing history',

    'SIGNUP_SUCCESS_MESSAGE' => 'Registration was successful. You can login',

    'EMAIL_CONGRATULATION' => 'Hi {name},',
    'EMAIL_LINK' => 'link',
    'EMAIL_REGARDS' => 'Kind regards,',

    'PASSWORD_RESET_SUBJECT' => 'Slaptažodžio atstatymas',
    'PASSWORD_RESET_START_TEXT' => 'to reset your password please go to ',

    'PASSWORD_CHANGE_SUCCESS_MESSAGE' => "Password has been changed",

    'MESSAGE_PLEASE_VERIFY_EMAIL' => 'Please verify your email address',

    'VERIFY_EMAIL_SUBJECT' => 'Email verification',
    'VERIFY_EMAIL_START_TEXT' => 'to verify your email please go to ',

    'ERROR_MESSAGE_INVALID_TOKEN' => "Token is invalid",
    'ERROR_MESSAGE_EXPIRED_TOKEN' => "Token is expired",

    'INVALID_REQUEST' => 'Invalid request',
    'EMAIL_SEND_FAILED' => 'Email send failed',

    'TITLE_COPY_LINK_TO_CLIPBOARD' => 'Copy link to clipboard',

    'WEBSITE_IS_IN_TEST_MODE' => 'Until 24th June website is running in test mode',

    'ACCOUNT_UPDATE_EMAIL_CHANGED' => 'Please verify your new email address',

    'COOKIE_MESSAGE' => 'We use cookies to enhance your experience. By continuing to visit this site you agree to our use of cookies and our ',
    'PRIVACY_POLICY_LINK' => 'Privacy Policy',

    'PRIVACY_POLICY_PARAGRAPH_1' => "This privacy policy sets out how “mycodebase.io” uses and protects any information that you give “mycodebase.io” when you use this website.<br>" .
      "“mycodebase.io” is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement.<br>" .
      "“mycodebase.io” may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes.<br>" .
      "<br><strong>What we collect</strong>&nbsp;<br>We may collect the following information:",

    'PRIVACY_POLICY_PARAGRAPH_2' => "<ul>" .
      "<li>name and job title</li>" .
      "<li>contact information including email address</li>" .
      "<li>demographic information such as postcode, preferences and interests</li>" .
      "<li>other information relevant to customer surveys and/or offers</li>" .
      "</ul>",

    'PRIVACY_POLICY_PARAGRAPH_3' => "<strong>What we do with the information we gather</strong><br>" .
      "We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:",

    'PRIVACY_POLICY_PARAGRAPH_4' => "<ul>" .
      "<li>Internal record keeping.</li>" .
      "<li>We may use the information to improve our products and services.</li>" .
      "<li>We may periodically send promotional email about new products, special offers or other information which we think you may find interesting using the email address which you have provided.&nbsp;</li>" .
      "<li>From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail.</li>" .
      "<li>We may use the information to customize the website according to your interests.</li>" .
      "<li>We may provide your information to our third party partners for marketing or promotional purposes.</li>" .
      "<li>We will never sell your information.</li>" .
      "</ul>",

    'PRIVACY_POLICY_PARAGRAPH_5' => "<strong>Security</strong>&nbsp;<br>" .
      "We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.",

    'PRIVACY_POLICY_PARAGRAPH_6' => "<strong>How we use cookies</strong>&nbsp;<br>" .
      "A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyze web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.&nbsp;<br>" .
      "<br>We use traffic log cookies to identify which pages are being used. This helps us analyze data about web page traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.&nbsp;<br>" .
      "<br>Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.&nbsp;<br>" .
      "<br>You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website.<br>" .
      "<br><strong>Links to other websites</strong><br>" .
      "Our website may contain links to enable you to visit other websites of interest easily. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.",
];
