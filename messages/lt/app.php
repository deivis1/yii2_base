<?php

return [
    'TITLE_LOGIN' => 'Prisijungimas',
    'TITLE_CONTACT' => 'Susisiekti',
    'TITLE_NEW_ACCOUNT' => 'Nauja paskyra',
    'TITLE_ACCOUNT' => 'Paskyra',
    'TITLE_CODE' => 'Kodas',
    'TITLE_CATEGORY' => 'Kategorija',
    'TITLE_PASSWORD_RESET_REQUEST' => 'Slaptažodžio atstatymas',
    'TITLE_PASSWORD_RESET' => 'Slaptažodžio keitimas',
    'TITLE_EMAIL_VERIFICATION' => 'Patvirtinkite savo el. paštą',
    'TITLE_PRIVACY_POLICY' => 'Privatumo politika',
    'TITLE_SHARED_CODE' => 'Pabendrintas kodas',

    'MENU_HOME' => 'Pradžia',
    'MENU_ABOUT' => 'Apie',
    'MENU_CONTACT' => 'Susisiekti',
    'MENU_CODE' => 'Kodas',
    'MENU_LANGUAGE' => 'Kalba',
    'MENU_LANGUAGE_EN' => 'Anglų',
    'MENU_LANGUAGE_LT' => 'Lietuvių',
    'MENU_LOGIN' => 'Prisijungti',
    'MENU_SIGNUP' => 'Registruotis',
    'MENU_MY_ACCOUNT' => 'Mano paskyra',
    'MENU_LOGOUT' => 'Atsijungti',

    'ACCOUNT_TAB_PROFILE' => 'Duomenys',
    'ACCOUNT_TAB_CHANGE_PASSWORD' => 'Keisti slaptažodį',

    'LANGUAGE_EN' => 'Anglų',
    'LANGUAGE_LT' => 'Lietuvių',

    'USER_LABEL_ID' => 'Id',
    'USER_LABEL_EMAIL' => 'El. paštas',
    'USER_LABEL_NAME' => 'Vardas',
    'USER_LABEL_CURRENT_PASSWORD' => 'Dabartinis',
    'USER_LABEL_PASSWORD' => 'Slaptažodis',
    'USER_LABEL_PASSWORD_CONFIRM' => 'Pakartoti slaptažodį',
    'USER_LABEL_VERIFY_CODE' => 'Patvirtinkite kodą',
    'USER_LABEL_VERIFY_EMAIL_TOKEN' => 'El. pašto patvirtinimo žetonas',
    'USER_LABEL_AUTH_KEY' => 'Autorizavimo raktas',
    'USER_LABEL_PASSWORD_HASH' => 'Slaptažodžio hashas',
    'USER_LABEL_PASSWORD_RESET_TOKEN' => 'Slaptažodžio atstatymo žetonas',
    'USER_LABEL_ROLE' => 'Rolė',
    'USER_LABEL_LOCKED' => 'Užrakintas',
    'USER_LABEL_LANGUAGE' => 'Kalba',
    'USER_LABEL_CREATED_AT' => 'Sukurta',
    'USER_LABEL_UPDATED_AT' => 'Atnaujinta',

    'USER_LABEL_NEW_PASSWORD' => 'Naujas slaptažodis',
    'USER_LABEL_CONFIRM_NEW_PASSWORD' => 'Patvirtinti',

    'USER_NAME_INVALID_PATTERN' => 'Vardas turi būti sudarytas tik iš raidžių.',
    'PASSWORD_CONFIRMATION_DOES_NOT_MATCH' => '„{attribute}“ turi sutapti su slaptažodžiu.',
    'NEW_PASSWORD_CONFIRMATION_DOES_NOT_MATCH' => '„Patvirtinti“ turi sutapti su nauju slaptažodžiu.',

    'PASSSWORD_RESET_REQUEST_TEXT' => 'Įveskite paskyros el. pašto adresą',

    'BUTTON_OK' => 'Gerai',
    'BUTTON_SIGNUP' => 'Registruotis',
    'BUTTON_SEND' => 'Siųsti',
    'BUTTON_RESEND' => 'Siųsti dar kartą',
    'BUTTON_BACK' => 'Atgal',
    'BUTTON_CHANGE' => 'Keisti',
    'BUTTON_GET_STARTED' => 'Pradėti',
    'BUTTON_SUBMIT' => 'Siųsti',
    'BUTTON_UPDATE' => 'Atnaujinti',
    'BUTTON_RESET' => 'Nulinti',
    'BUTTON_TRY_DEMO' => 'Išbandyk',

    'CATEGORY_LABEL_ID' => 'Id',
    'CATEGORY_LABEL_USER_ID' => 'Vartototjas',
    'CATEGORY_LABEL_NAME' => 'Pavadinimas',
    'CATEGORY_LABEL_ORDER' => 'Rūšiavimas',
    'CATEGORY_LABEL_CREATED_AT' => 'Sukurta',
    'CATEGORY_LABEL_UPDATED_AT' => 'Atnaujinta',

    'CATEGORY_CODE_LABEL_ID' => 'Id',
    'CATEGORY_CODE_LABEL_CATEGORY_ID' => 'Kategorija',
    'CATEGORY_CODE_LABEL_CODE_ID' => 'Kodas',
    'CATEGORY_CODE_LABEL_CREATED_AT' => 'Sukurta',
    'CATEGORY_CODE_LABEL_UPDATED_AT' => 'Atnaujinta',

    'CODE_LABEL_ID' => 'Id',
    'CODE_LABEL_USER_ID' => 'Vartototjas',
    'CODE_LABEL_DESCRIPTION' => 'Aprašymas',
    'CODE_LABEL_KEYWORDS' => 'Raktažodžiai',
    'CODE_LABEL_CODE' => 'Kodas',
    'CODE_LABEL_LANGUAGE_ID' => 'Kalba',
    'CODE_LABEL_SHARE_TOKEN' => 'Dalinimosi žetonas',
    'CODE_LABEL_CREATED_AT' => 'Sukurta',
    'CODE_LABEL_UPDATED_AT' => 'Atnaujinta',

    'LANGUAGE_LABEL_ID' => 'Id',
    'LANGUAGE_LABEL_NAME' => 'Pavadinimas',
    'LANGUAGE_LABEL_LANGUAGE_ID' => 'Kalba',
    'LANGUAGE_LABEL_ORDER' => 'Rūšiavimas',
    'LANGUAGE_LABEL_CREATED_AT' => 'Sukurta',
    'LANGUAGE_LABEL_UPDATED_AT' => 'Atnaujinta',

    'CONTACT_FORM_LABEL_EMAIL' => 'El. paštas',
    'CONTACT_FORM_LABEL_NAME' => 'Vardas',
    'CONTACT_FORM_LABEL_SUBJECT' => 'Tema',
    'CONTACT_FORM_LABEL_MESSAGE' => 'Žinutė',
    'CONTACT_FORM_LABEL_VERIFY_CODE' => 'Patvirtinimo kodas',

    'CONTACT_SUBJECT_GUEST_VISITOR_PREFIX' => 'Neprisijungusio lankytojo žinutė: ',
    'CONTACT_SUBJECT_LOGGED_IN_VISITOR_PREFIX' => 'Prisijungusio lankytojo žinutė: ',

    'MODEL_NOT_FOUND' => 'Modelis {model} nerastas',
    'MODEL_INVALID_ID' => 'Neteisingas Id',
    'MODEL_INVALID_EMAIL' => 'Neteisingas el. pašto adresas',
    'MODEL_INVALID_USER_ID' => 'Neteisingas vartotojo id',
    'USER_INVALID_CURRENT_PASSWORD' => 'Neteisingas dabartinis slaptažodis.',

    'LOGIN_FORM_LABEL_USERNAME' => 'El. paštas',
    'LOGIN_FORM_LABEL_PASSWORD' => 'Slaptažodis',
    'LOGIN_FORM_LABEL_REMEMBER_ME' => 'Įsiminti',
    'LOGIN_FORM_INVALID_INCORRECT_USERNAME_OR_PASSWORD' => 'Neteisingas el. pašto adresas arba slaptažodis.',

    'USER_LOG_LABEL_ID' => 'Id',
    'USER_LOG_LABEL_USER_ID' => 'Vartotojas',
    'USER_LOG_LABEL_IP' => 'IP adresas',
    'USER_LOG_LABEL_ACTION_ID' => 'Veiksmas',
    'USER_LOG_LABEL_CREATED_AT' => 'Sukurta',
    'USER_LOG_LABEL_UPDATED_AT' => 'Atnaujinta',

    'USER_LOG_ACTION_LABEL_ID' => 'Id',
    'USER_LOG_ACTION_LABEL_NAME' => 'Pavadinimas',
    'USER_LOG_ACTION_LABEL_CREATED_AT' => 'Sukurta',
    'USER_LOG_ACTION_LABEL_UPDATED_AT' => 'Atnaujinta',

    'TEXT_SIGNIN' => 'Prisijungimas',
    'LOGIN_EMAIL_PLACEHOLDER' => 'El. paštas',
    'BUTTON_LOGIN' => 'Prisijungti',
    'LINK_FORGOT_PASSWORD' => 'Pamiršote slaptažodį?',

    'MESSAGE_CODE_ADDED' => 'Kodas pridėtas',
    'TEXT_FOOTER' => 'DekaSoft',

    'BAD_POST_REQUEST' => 'Neteisinga POST užklausa',
    'BAD_REQUEST_ARGUMENT' => 'Neteisingas užklausos argumentas',
    'ERROR_WRONG_OWNER' => 'Nesate savininkas',
    'INVALID_CODE_SHARE_LINK' => 'Neteisinga nuoroda',
    'CATEGORY_NAME_EXISTS' => 'Toks kategorijos pavadinimas jau yra',

    'LABEL_SEARCH' => 'Paieška',
    'LABEL_FILTER_CATEGORIES' => 'Filtruoti kategorijas',
    'LABEL_CATEGORIES' => 'Kategorijos',
    'SELECT_PLACEHOLDER' => ' ',
    'SEARCH_CRITERIA_INFO' => 'Pakeiskite paiešką, kad matytumėte įrašą sąraše',
    'LABEL_INPUT_DESCRIPTION' => 'Pavadinimas',
    'LABEL_INPUT_KEYWORDS' => 'Raktažodžiai',
    'LABEL_KEYWORDS' => 'Raktažodžiai',
    'LABEL_UPDATED' => 'Atnaujinta',
    'LABEL_SHARED' => 'Pabendrintas',
    'LABEL_COPY_LINK_TO_CLIPBOARD' => 'Kopijuoti nuorodą į atmintį',
    'LABEL_SHARED_CODE_AUTHOR' => 'Pabendrino autorius',
    'BUTTON_TITLE_SAVE' => 'Išsaugoti',
    'BUTTON_TITLE_CREATE_NEW' => 'Sukurti naują',
    'BUTTON_TITLE_REMOVE' => 'Ištrinti',
    'BUTTON_TITLE_COPY_TO_CLIPBOARD' => 'Kopijuoti kodą į atmintį',
    'BUTTON_TITLE_SHARE' => 'Bendrinti',
    'BUTTON_TITLE_EDIT' => 'Redaguoti',
    'BUTTON_TITLE_SORT_UP' => 'Perkelti aukščiau',
    'BUTTON_TITLE_SORT_DOWN' => 'Perkelti žemiau',
    'BUTTON_EDIT_CATEGORIES' => 'Redaguoti kategorijas',
    'LINK_GO_BACK_TO_CODE' => 'grįžti atgal į kodą',

    'HOME_HEADER' => 'Kodo fragmentų valdymas',
    'HOME_FEATURE_1' => 'Visas Jūsų kodas vienoje vietoje',
    'HOME_FEATURE_2' => 'Patogus <a href="https://ace.c9.io/" target="_blank" class="btn-link">Ace</a> kodo redaktorius',
    'HOME_FEATURE_3' => 'Greita paieška pagal raktažodžius, kategorijas',
    'HOME_FEATURE_4' => 'Dalinkis kodu su draugais ir kolegomis',

    'SIGNUP_SUCCESS_MESSAGE' => 'Registracija sėkminga. Galite prisijungti',

    'EMAIL_CONGRATULATION' => 'Sveiki, {name},',
    'EMAIL_LINK' => 'nuorodos',
    'EMAIL_REGARDS' => 'Pagarbiai,',

    'PASSWORD_RESET_SUBJECT' => 'Slaptažodžio atstatymas',
    'PASSWORD_RESET_START_TEXT' => 'slaptažodžio atstatymui spauskite ant ',

    'PASSWORD_CHANGE_SUCCESS_MESSAGE' => "Slaptažodis sėkmingai pakeistas",

    'MESSAGE_PLEASE_VERIFY_EMAIL' => 'Patvirtinkite savo el. pašto adresą',

    'VERIFY_EMAIL_SUBJECT' => 'El. pašto patvirtinimas',
    'VERIFY_EMAIL_START_TEXT' => "savo elektroninio pašto patvirtinimui spauskite ant ",

    'ERROR_MESSAGE_INVALID_TOKEN' => "Žetonas yra blogas",
    'ERROR_MESSAGE_EXPIRED_TOKEN' => "Žetonas galiojimo laikas yra pasibaigęs",

    'INVALID_REQUEST' => 'Neteisinga užklausa',
    'EMAIL_SEND_FAILED' => 'Laiško išsiųsti nepavyko',

    'TITLE_COPY_LINK_TO_CLIPBOARD' => 'Kopijuoti nuorodą į atmintį',

    'WEBSITE_IS_IN_TEST_MODE' => 'Iki birželio 24 dienos svetainė veikia bandymo režimu',

    'ACCOUNT_UPDATE_EMAIL_CHANGED' => 'Patvirtinkite pasikeitusį el. pašto adresą',

    'COOKIE_MESSAGE' => 'Naršymo kokybei pagerinti naudojame slapukus. Toliau naudodamiesi svetaine, Jūs sutinkate su slapukų naudojimu ir mūsų ',
    'PRIVACY_POLICY_LINK' => 'privatumo politika',

    'PRIVACY_POLICY_PARAGRAPH_1' => "This privacy policy sets out how “mycodebase.io” uses and protects any information that you give “mycodebase.io” when you use this website.<br>" .
      "“mycodebase.io” is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement.<br>" .
      "“mycodebase.io” may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes.<br>" .
      "<br><strong>What we collect</strong>&nbsp;<br>We may collect the following information:",

    'PRIVACY_POLICY_PARAGRAPH_2' => "<ul>" .
      "<li>name and job title</li>" .
      "<li>contact information including email address</li>" .
      "<li>demographic information such as postcode, preferences and interests</li>" .
      "<li>other information relevant to customer surveys and/or offers</li>" .
      "</ul>",

    'PRIVACY_POLICY_PARAGRAPH_3' => "<strong>What we do with the information we gather</strong><br>" .
      "We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:",

    'PRIVACY_POLICY_PARAGRAPH_4' => "<ul>" .
      "<li>Internal record keeping.</li>" .
      "<li>We may use the information to improve our products and services.</li>" .
      "<li>We may periodically send promotional email about new products, special offers or other information which we think you may find interesting using the email address which you have provided.&nbsp;</li>" .
      "<li>From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail.</li>" .
      "<li>We may use the information to customize the website according to your interests.</li>" .
      "<li>We may provide your information to our third party partners for marketing or promotional purposes.</li>" .
      "<li>We will never sell your information.</li>" .
      "</ul>",

    'PRIVACY_POLICY_PARAGRAPH_5' => "<strong>Security</strong>&nbsp;<br>" .
      "We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.",

    'PRIVACY_POLICY_PARAGRAPH_6' => "<strong>How we use cookies</strong>&nbsp;<br>" .
      "A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookie helps analyze web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.&nbsp;<br>" .
      "<br>We use traffic log cookies to identify which pages are being used. This helps us analyze data about web page traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.&nbsp;<br>" .
      "<br>Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.&nbsp;<br>" .
      "<br>You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website.<br>" .
      "<br><strong>Links to other websites</strong><br>" .
      "Our website may contain links to enable you to visit other websites of interest easily. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.",
];
