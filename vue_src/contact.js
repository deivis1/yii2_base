import Vue from 'vue';
import axios from 'axios';
import {cookieMixin} from './cookieMixin';
import {myMixin} from './myMixin';
import {init} from './init';
init();

new Vue({
  el: '#app',
  mixins: [cookieMixin, myMixin],
  data () {
    return {
      loading: false,
      email: '',
      name: '',
      subject: '',
      message: '',
      verifyCode: '',
      alertShow: false,
      alertColor: 'info',
      alertText: '',
      alertTimeout: null
    };
  },
  methods: {
    submitForm () {
      const postData = this.preparePostData();
      
      const config = {
        headers: { 
          'Content-type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        }
      };
            
      this.loading = true;
      const seconds = Math.floor(Date.now() / 1000);
      axios.post('/site/contact', postData, config)
          .then(response => {
            setTimeout(() => {
                this.loading = false;
            }, Math.floor(Date.now() / 1000) - seconds < 1 ? 1500 : 0);
            if (response.data.message === 'success') {
              this.resetForm();
              this.showAlert(this.tr('EMAIL_SEND_SUCCESS'), 'success');
            } else {
              this.showAlert(this.tr('EMAIL_SEND_FAILED'), 'danger');
            }
          });
    },
    validateForm (e) {
      this.validateFormAttributes();
      setTimeout(() => {
        if (this.getForm().find('.is-invalid').length === 0) {
          this.submitForm();
        }
      }, 300);
    },
    validateFormAttributes () {
      this.iterateFormAttributes(el => {
        el.status = 3;
      });
      this.getForm().yiiActiveForm("validate");  
    },
    getForm() {
      return $("#contact-form");
    },
    iterateFormAttributes (callback) {
      const data = this.getForm().data("yiiActiveForm");
      $.each(data.attributes, function() {
        callback(this);
      });
    },
    resetForm () {
      this.loading = false;
      this.getForm().yiiActiveForm('resetForm');
      this.getForm()[0].reset();
    },
    preparePostData () {
      let postData = '';
      
      this.iterateFormAttributes(el => {
        postData += encodeURI('ContactForm[' + el.name + ']=' + el.value + '&');
      });
      
      postData += encodeURI(yii.getCsrfParam() + '=' + yii.getCsrfToken());
      return postData;
    },
    showAlert (text, alertColor) {
      if (this.alertTimeout !== null) {
        clearTimeout(this.alertTimeout);
      }
      
      this.alertShow = true;
      this.alertColor = alertColor;
      this.alertText = text;
      
      const delay = 2000;
      this.alertTimeout = setTimeout(() => {
        this.alertShow = false;
      }, delay);
      
      this.alertTimeout = setTimeout(() => {
        this.alertColor = 'info';
        this.alertText = '';
      }, delay + 2000);
    },
    tr (placeholder) {
      let translation;
      if (this.placeholders.indexOf(placeholder) === -1) {
        translation = placeholder;
      } else {
        translation = this.messages[placeholder];
      }
      return translation;
    }
  },
  mounted () {
    document.querySelectorAll('.site-hide').forEach(element => {
      element.classList.remove('site-hide');
    });
      
    this.$nextTick(() => {
      const el = document.querySelector('[autofocus]');
      if (el !== null) {
        el.focus();
      }
    });
    
    $('.nav-link').click((e) => {
      this.getForm().yiiActiveForm('resetForm');
    });
  }
});
