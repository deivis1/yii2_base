import Vue from 'vue';
import axios from 'axios';
import {cookieMixin} from './cookieMixin';
import {myMixin} from './myMixin';
import {init} from './init';
init();

new Vue({
  el: '#app',
  mixins: [cookieMixin, myMixin],
  data () {
    return {
      loadingAccount: false,
      loadingPassword: false,
      defaultEmail: '',
      defaultName: '',
      defaultLanguage: 0,
      email: '',
      name: '',
      language: '',
      currentPassword: '',
      newPassword: '',
      newPasswordConfirm: '',
      alertShow: false,
      alertColor: 'info',
      alertText: '',
      alertTimeout: null,
      mountCallback: this.onMount,
      profileCancelSource: null,
      passwordCancelSource: null
    };
  },
  computed: {
    isDangerAlert () {
        return this.alertColor === 'danger';
    }  
  },
  methods: {
    updateAccount () {
      if (!this.name.length || !this.email.length) {
        this.showAlert(this.tr('ACCOUNT_UPDATE_FIELDS_MUST_BE_FILLED'), 'danger', true);
        return;
      }
        
      const postData = this.preparePostData(['name', 'email', 'language']);
      
      this.profileCancelSource = axios.CancelToken.source();
      
      const config = {
        headers: { 
          'Content-type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        },
        cancelToken: this.profileCancelSource.token
      };
            
      this.loadingAccount = true;
      const seconds = Math.floor(Date.now() / 1000);
      
      axios.post('/user/update-account', postData, config)
        .then(response => {
          setTimeout(() => {
              this.loadingAccount = false;
          }, Math.floor(Date.now() / 1000) - seconds < 1 ? 1500 : 0);
          if (response.data.message === 'success' && response.data.redirect !== null) {
              window.location.href = response.data.redirect;
          } else if (response.data.message === 'success') {
            this.defaultName = this.name;
            this.defaultEmail = this.email;
            this.defaultLanguage = this.language;
            this.showAlert(this.tr('ACCOUNT_UPDATE_SUCCESS'), 'success', false);
          } else {
            this.showAlert(response.data.errors, 'danger', true);
          }
        }).catch(error => {
          if (axios.isCancel(error)) {
            console.log('Request canceled', error.message);
          } else {
            console.log(error.message);
          }
        });
    },
    changePassword () {
      if (!this.currentPassword.length || !this.newPassword.length || !this.newPasswordConfirm.length) {
        this.showAlert(this.tr('ACCOUNT_UPDATE_FIELDS_MUST_BE_FILLED'), 'danger', true);
        return;
      }
      
      const postData = this.preparePostData(['currentPassword', 'password', 'passwordConfirm']);
      
      this.passwordCancelSource = axios.CancelToken.source();
      
      const config = {
        headers: { 
          'Content-type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        },
        cancelToken: this.passwordCancelSource.token
      };
            
      this.loadingPassword = true;
      const seconds = Math.floor(Date.now() / 1000);
      axios.post('/user/change-password', postData, config)
        .then(response => {
          setTimeout(() => {
              this.loadingPassword = false;
          }, Math.floor(Date.now() / 1000) - seconds < 1 ? 1500 : 0);
          if (response.data.message === 'success') {
            this.currentPassword = '';
            this.newPassword = '';
            this.newPasswordConfirm = '';
            this.showAlert(this.tr('PASSWORD_CHANGE_SUCCESS'), 'success', false);
          } else {
            this.showAlert(response.data.errors, 'danger', true);
          }
        }).catch(error => {
          if (axios.isCancel(error)) {
            console.log('Request canceled', error.message);
          } else {
            console.log(error.message);
            this.showAlert(this.tr('PASSWORD_CHANGE_FAILED'), 'danger', true);
          }
        });
    },
    preparePostData (attributes) {
      let postData = '';
      
      if (!attributes || !attributes.length) {
        return null;
      }
      
      attributes.forEach(name => {
        const modelAttribute = 'User[' + name + ']';
        const value = document.querySelector('[name="' + modelAttribute + '"]').value;
        postData += encodeURI(modelAttribute + '=' + value + '&');
      });
      
      postData += encodeURI(yii.getCsrfParam() + '=' + yii.getCsrfToken());
      return postData;
    },
    resetAccountForm() {
      if (this.profileCancelSource) {
        this.profileCancelSource.cancel('Operation canceled by the user.');
      }
      this.loadingAccount = false;
      this.name = this.defaultName;
      this.email = this.defaultEmail;
      this.language = this.defaultLanguage;
    },
    resetPasswordForm() {
      if (this.passwordCancelSource) {
        this.passwordCancelSource.cancel('Operation canceled by the user.');
      }
      this.loadingPassword = false;
      this.currentPassword = '';
      this.newPassword = '';
      this.newPasswordConfirm = '';
    },
    showAlert (text, alertColor, keep) {
      if (this.alertTimeout !== null) {
        clearTimeout(this.alertTimeout);
      }
      
      if (this.alertShow) {
        this.alertShow = false;  
        this.alertTimeout = setTimeout(() => {
          this.showAlert(text, alertColor, keep);
        }, 450);
        return;
      }
      
      this.alertShow = true;
      this.alertColor = alertColor;
      this.alertText = text;
      
      if (keep) {
        return;
      }
      this.hideAlert(null, 2000);
    },
    hideAlert (e, delay) {
      if (this.alertTimeout !== null) {
        clearTimeout(this.alertTimeout);
      }
      
      if (!this.alertShow) {
        return;
      }
      
      if (!delay) {
        delay = 0;
      }
      
      setTimeout(() => {
        this.alertShow = false;
      }, delay);
      
      this.alertTimeout = setTimeout(() => {
        this.alertColor = 'info';
        this.alertText = '';
      }, delay + 2000);
    },
    tr (placeholder) {
      let translation;
      if (this.placeholders.indexOf(placeholder) === -1) {
        translation = placeholder;
      } else {
        translation = this.messages[placeholder];
      }
      return translation;
    },
    onMount () {
      this.defaultName = window.myData.values.name;
      this.defaultEmail = window.myData.values.email;
      this.defaultLanguage = window.myData.values.language;
      this.resetAccountForm();
    }
  },
  mounted () {
    document.querySelectorAll('.site-hide').forEach(element => {
      element.classList.remove('site-hide');
    });
      
    this.$nextTick(() => {
      const el = document.querySelector('[autofocus]');
      if (el !== null) {
        el.focus();
      }
    });
  }
});
