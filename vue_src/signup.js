import Vue from 'vue';
import {cookieMixin} from './cookieMixin';
import {init} from './init';
init();

new Vue({
  el: '#app',
  mixins: [cookieMixin],
  data () {
    return {
      loading: false
    };
  },
  methods: {
    submitForm() {
      this.loading = true;
      this.getForm().yiiActiveForm('submitForm');
    },
    validateForm () {
      this.validateFormAttributes();
      setTimeout(() => {
        if (this.getForm().find('.is-invalid').length === 0) {
          this.submitForm();
        }
      }, 300);
    },
    validateFormAttributes () {
      this.iterateFormAttributes(el => {
        el.status = 3;
      });
      this.getForm().yiiActiveForm("validate");  
    },
    getForm() {
      return $("#signup-form");
    },
    iterateFormAttributes (callback) {
      const data = this.getForm().data("yiiActiveForm");
      $.each(data.attributes, function() {
        callback(this);
      });
    },
    resetForm () {
      this.loading = false;
      this.getForm().yiiActiveForm('resetForm');
      this.getForm()[0].reset();
    }
  },
  mounted () {
    document.querySelectorAll('.site-hide').forEach(element => {
      element.classList.remove('site-hide');
    });
      
    this.$nextTick(() => {
      const el = document.querySelector('[autofocus]');
      if (el !== null) {
        el.focus();
      }
    });
    
    $('.nav-link').click((e) => {
      this.getForm().yiiActiveForm('resetForm');
    });
  }
});
