export const myMixin = {
  data () {
    return {
      messages: {},
      placeholders: [],
      alertShow: false,
      alertColor: 'info',
      alertText: '',
      alertTimeout: null
    };
  },
  methods: {
    tr (placeholder) {
      let translation;
      if (this.placeholders.indexOf(placeholder) === -1) {
        translation = placeholder;
      } else {
        translation = this.messages[placeholder];
      }
      return translation;
    }
  },
  mounted () {
    if (this.hasOwnProperty('noMixinMount') && this.noMixinMount) {
      return;
    }
    this.messages = window.myData.messages;
    this.placeholders = Object.keys(this.messages);
    
    if (this.hasOwnProperty('mountCallback') && 
      typeof this.mountCallback === "function"
    ) {
      this.mountCallback();
    }
    
    delete window.myData;
  }
};
