export const cookieMixin = {
  data () {
    return {
      showCookieConsent: false
    };
  },
  methods: {
    cookieConsent() {
      this.setCookie('consent', 1, 365);
      this.showCookieConsent = false;
    },
    checkConsentCookie() {
      let consent = this.getCookie('consent');
      return consent === '1';
    },
    getCookie(cname) {
      let name = cname + "=";
      let decodedCookie = decodeURIComponent(document.cookie);
      let ca = decodedCookie.split(';');
      for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    },
    setCookie(cname, cvalue, exdays) {
      const date = new Date();
      date.setTime(date.getTime() + (exdays * 24 * 60 * 60 * 1000));
      let expires = "expires="+ date.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
  },
  mounted () {
    if (!this.checkConsentCookie()) {
      setTimeout(() => {
        this.showCookieConsent = true;
      }, 1000);
    }
  }
};
