import Vue from 'vue';
import {cookieMixin} from './cookieMixin';
import {init} from './init';
init();

new Vue({
  el: '#app',
  mixins: [cookieMixin],
  data () {
    return {
      editor: null,
      messages: {},
      placeholders: [],
      alertShow: false,
      alertColor: 'info',
      alertText: '',
      alertTimeout: null
    };
  },
  methods: {
    showAlert (text, alertColor) {
      this.alertColor = alertColor;
      this.alertText = text;
      this.alertShow = true;
      
      if (this.alertTimeout !== null) {
        clearTimeout(this.alertTimeout);
      }
      
      this.alertTimeout = setTimeout(() => {
        this.alertShow = false;    
      }, 2000);
    },
    copyCodeToClipboard () {
      if (this.code.length) {
        this.copyTextToClipboard(this.code);
        this.showAlert(this.tr('COPY_CODE_TO_CLIPBOARD_SUCCESS'), 'info');
      } else {
        this.showAlert(this.tr('COPY_CODE_TO_CLIPBOARD_FIELD_EMPTY'), 'danger');
      }
    },
    copyTextToClipboard (text) {
      const el = document.querySelector('.code-textarea');
      el.value = text;
      $(el).select();
      document.execCommand('copy');
      el.value = '';
    },
    tr (placeholder) {
      let translation;
      if (this.placeholders.indexOf(placeholder) === -1) {
        translation = placeholder;
      } else {
        translation = this.messages[placeholder];
      }
      return translation;
    },
    setupEditor () {
      this.editor = ace.edit('editor');
      this.editor.session.setTabSize(4);
      this.editor.setShowFoldWidgets(false);
      this.editor.setShowPrintMargin(false);
      this.editor.getSession().setUseWorker(false);
      this.editor.session.setMode('ace/mode/javascript');
      
      this.editor.setValue(this.code);
      this.editor.selection.moveCursorToPosition({row: 0, column: 0});
      this.editor.focus();
      
      this.editor.on('change', e => {
        this.code = this.editor.getValue();
      });
    }
  },
  mounted () {
    document.querySelectorAll('.site-hide').forEach(element => {
      element.classList.remove('site-hide');
    });
      
    this.messages = window.myData.messages;
    this.placeholders = Object.keys(this.messages);
    this.code = window.myData.code.replace(/\|n\|/g, '\n');
    
    delete window.myData;
    
    this.setupEditor();
  }
});
