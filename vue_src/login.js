import Vue from 'vue';
import {cookieMixin} from './cookieMixin';
import {init} from './init';
init();

new Vue({
  el: '#app',
    mixins: [cookieMixin],
  data () {
    return {
      show: false
    };
  },
  methods: {
    onSubmit (e) {
      if (!document.querySelector('[name="LoginForm[username]"]').value.length ||
        !document.querySelector('[name="LoginForm[password]"]').value.length
      ) {
        e.preventDefault();
      }
    },
    onForgotPassword (e) {
      const email = document.querySelector('[name="LoginForm[username]"]').value.toLowerCase();
      let url = e.target.href;

      if (email.match(/^[a-z0-9\.\-]{2,}\@[a-z0-9\-]{3,}\.[a-z0-9]{2,}(\.[a-z0-9]{2,})?$/)) {
        url += "/" + btoa(email);
      }
      window.location.href = url;
    }
  },
  mounted () {
    var el = document.querySelector('.site-alert-message');
    if (el !== null) {
      setTimeout(() => {
        this.show = true;
      }, 200);
    }
    
    document.querySelectorAll('.site-hide').forEach(element => {
      element.classList.remove('site-hide');
    });
  }
});