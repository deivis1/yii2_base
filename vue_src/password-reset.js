import Vue from 'vue';
import {cookieMixin} from './cookieMixin';
import {init} from './init';
init();

new Vue({
  el: '#app',
    mixins: [cookieMixin],
  data () {
    return {
    };
  },
  methods: {
  },
  mounted () {
    var el = document.querySelector('.site-alert-message');
    if (el !== null) {
      setTimeout(() => {
        this.show = true;
      }, 200);
    }
    
    document.querySelectorAll('.site-hide').forEach(element => {
      element.classList.remove('site-hide');
    });
  }
});
