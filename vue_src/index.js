import Vue from 'vue';
import {cookieMixin} from './cookieMixin';
import {init} from './init';
init();

new Vue({
  el: '#app',
  mixins: [cookieMixin],
  mounted () {
    document.querySelectorAll('.site-hide').forEach(element => {
      element.classList.remove('site-hide');
    });
  }
});
