import Vue from 'vue';

export function init() {
  Vue.config.productionTip = false;
  Vue.config.devtools = false;
}