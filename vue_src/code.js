import Vue from 'vue';
import axios from 'axios';
import {cookieMixin} from './cookieMixin';
import {init} from './init';
init();

const app = new Vue({
  el: '#app',
  mixins: [cookieMixin],
  data () {
    return {
      editor: null,
      messages: {},
      placeholders: [],
      search: '',
      searchCategories: [],
      selected: 0,
      codeDescription: '',
      codeKeywords: '',
      codeCategories: [],
      code: '',
      codeLanguageId: '',
      defaultLanguageId: '',
      hidePopoverTimeout: null,
      resultsAll: [],
      results: [],
      shareLink: '',
      itemShareLink: '',
      itemShareToken: '',
      itemShareData: '',
      showCopyLink: false,
      modalTitle: '',
      positiveModalAction: {},
      negativeModalAction: {},
      modalCancelButton: '',
      modalActionButton: '',
      modalText: '',
      alertShow: false,
      alertColor: 'info',
      alertText: '',
      alertTimeout: null
    };
  },
  computed: {
    changed () {
      if (this.selected === 0 && this.valid) {
        return true;
      }
      
      const item = this.getSelectedItem();
      
      if (!item || !this.valid) {
        return false;
      }
      
      return item.description !== this.codeDescription ||
        item.keywords !== this.codeKeywords ||
        item.code !== this.code ||
        item.languageId !== this.codeLanguageId ||
        !this.compareArrays(item.categories, this.codeCategories);
    },
    isSelected () {
       return this.selected !== 0;
    },
    isShared () {
      const item = this.getSelectedItem();
      
      if (!item || !item.shareToken) {
        return false;
      }
      
      return item.shareToken.length > 0;
    },
    valid () {
       return this.codeDescription.length > 0 && this.code.length > 0;
    }
  },
  filters: {
    timeFilter (value) {
      return new Date(value * 1000).toISOString().slice(0, -5).replace('T', ' ');
    }
  },
  methods: {
    displayCode (value) {
      if (!value) {
        return '';
      }
      value = value.toString();
      const pos = value.indexOf('\n');
      value = pos === -1 ? value : value.substring(0, pos);
      return value.substring(0, 30).replace('\n', '<br>');
    },
    displayKeywords (value) {
      return !value ? this.tr('FILTER_KEYWORDS_NONE'): value;
    },
    confirmSelect(id) {
      if (this.changed) {
        this.positiveModalAction = { 'type': 'selectResult', 'id': id };
        this.modalCancelButton = this.tr('MODAL_BUTTON_CANCEL');
        this.modalActionButton = this.tr('MODAL_BUTTON_CONTINUE');
        this.modalTitle = this.tr('MODAL_TITLE_UNSAVED_CHANGES');
        this.modalText = this.tr('MODAL_TEXT_CONFIRM_UNSAVED_CHANGES');
        $('#myModal').modal('show');
      } else {
        this.selectResult(id);
      }
    },
    selectResult(id) {
      if (this.selected !== id) {
        this.selected = id;
        const result = this.results.filter(el => el.id === id)[0];
        this.codeDescription = result.description;
        this.codeKeywords = result.keywords;
        this.setCodeCategories(result.categories);
        this.setCode(result.code);
        this.codeLanguageId = result.languageId;
        this.editor.session.setMode('ace/mode/' + this.codeLanguageId);
      }
      if (window.innerWidth > 480) {
        this.editor.focus();
      }
    },
    compareArrays(a1, a2) {
      if (a1.length === 0 && a2.length === 0) {
        return true;
      } else if (a1.length !== a2.length) {
        return false;
      }
      const filterLength = a1.filter((el, i) => el === a2[i]).length;
      return a1.length === filterLength && a2.length === filterLength;
    },
    setCode (value) {
      this.editor.setValue(value);
      this.editor.selection.moveCursorToPosition({row: 0, column: 0});
    },
    getSelectedItem() {
      if (this.selected === 0 || this.results.length === 0) {
        return null;
      }
      const items = this.results.filter(el => el.id === this.selected);
      if (items.length === 1) {
        return items[0];
      }
      return null;
    },
    setSearchCategories () {
      const list = $('#filter-categories').val();
      let result = [];
      list.forEach(value => result.push(Number(value)));
      this.searchCategories = result;
      this.onSearchCriteriaInput();
    },
    setSelectedCodeCategories () {
      const list = $('#code-categories').val();
      let result = [];
      list.forEach(value => result.push(Number(value)));
      this.codeCategories = result;
    },
    saveItem() {
        if (!this.changed) {
          return;
        }
        const id = this.selected;
        let newValues = {
          id: id,
          description: this.codeDescription,
          keywords: this.codeKeywords,
          categories: this.codeCategories,
          code: this.code,
          language_id: this.codeLanguageId
        };
        
        if (id > 0) {
          this.updateDbItem(newValues);
        } else {
          this.createDbItem(newValues);
        }
    },
    confirmRemove() {
      const item = this.getSelectedItem();
      if (item) {
        this.positiveModalAction = { type: 'removeItem' };
        this.modalTitle = this.tr('MODAL_TITLE_CONFIRM_DELETE');
        this.modalCancelButton = this.tr('MODAL_BUTTON_CANCEL');
        this.modalActionButton = this.tr('MODAL_BUTTON_DELETE');
        this.modalText = this.tr('MODAL_TEXT_CONFIRM_DELETE_CODE') + ' \'' + item.description + '\' ?';
        $('#myModal').modal('show');
      }
    },
    removeItem() {
      const id = this.selected;
      if (id > 0) {
        const item = this.results.filter(el => el.id === id)[0];
        
        const config = {
          headers: { 
            'Content-type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN': yii.getCsrfToken()
          }
        };
        
        axios.post('/code/remove-code/' + item.id, null, config)
          .then(response => {
            if (response.data.message === 'success') {
              this.resultsAll = this.resultsAll.filter(el => el.id !== id);  
              this.results = this.results.filter(el => el.id !== id);
              this.selected = 0;
              this.codeDescription = '';
              this.codeKeywords = '';
              this.setCodeCategories([]);
              this.setCode('');
              this.codeLanguageId = this.defaultLanguageId;
              this.showAlert(this.tr('NOTICE_DELETED'), 'info');
            } else {
              console.log(response);
            }
          });
      }
    },
    setCodeCategories(ids) {
      this.codeCategories = ids;
      let result = [];
      ids.forEach(id => result.push(id.toString()));
      $("#code-categories").val(result).change();
    },
    onPositiveModalAction () {
      $('#myModal').modal('hide');
      switch(this.positiveModalAction.type) {
        case 'selectResult':
          this.selectResult(this.positiveModalAction.id);
          break;
        case 'newItem':
          this.newItem();
          break;
        case 'removeItem':
          this.removeItem();
          break;
        case 'shareItem':
          this.shareItem();
          break;
      }
    },
    onNegativeModalAction () {
      if (!this.negativeModalAction.type) {
        this.negativeModalAction.type = '';
      }
      switch(this.negativeModalAction.type) {
        case 'stopSharingItem':
          this.stopSharingItem();
          break;
      }
    },
    onMouseoutCopyPopover () {
      this.removePopoverTimeout();
      this.hidePopoverTimeout = setTimeout(() => {
        $('.site-code-shared').popover('hide');
      }, 200);
    },
    onMouseoutCopyBadge () {
      this.removePopoverTimeout();
      this.hidePopoverTimeout = setTimeout(() => {
        $('.site-code-shared').popover('hide');
      }, 1000);
    },
    removePopoverTimeout () {
      if (this.hidePopoverTimeout) {
        clearTimeout(this.hidePopoverTimeout);
      }
    },
    showFiltered(values) {
      let categoryCondition = false;
      if (this.searchCategories.length === 0) {
        categoryCondition = true;
      } else {
        const list = values.categories.filter(
          val => this.searchCategories.includes(val));
        categoryCondition = list.length > 0 && values.categories.length > 0;
      }
      const str = this.search.toLowerCase();
      return (values.description.toLowerCase().indexOf(str) !== -1 ||
          values.keywords.toLowerCase().indexOf(str) !== -1 ||
          values.code.toLowerCase().indexOf(str) !== -1) && 
          categoryCondition;
    },
    encodeCodeText(code) {  
      return code
        .replace(/\n/g, '_n_')
        .replace(/&/g, '_amp_')
        .replace(/\+/g, '_plus_');
    },
    createDbItem(newValues) {
      newValues.code = this.encodeCodeText(newValues.code);
      const postData = this.prepareFormData(newValues);
      const config = {
        headers: { 'Content-type': 'application/x-www-form-urlencoded' }
      };
      axios.post('/code/create-code', postData, config)
        .then(response => {
          if (response.data.message === 'success') {
            
            this.showAlert(this.tr('NOTICE_CREATED'), 'info');
            const savedValues = this.formatItemFields(response.data.item);
            this.setInputValues(savedValues);
            this.resultsAll.push(savedValues);
            
            if (this.showFiltered(newValues)) {
              this.selected = savedValues.id;
              this.filterResults();
              
              this.$nextTick(() => {
                const selectedItem = document.querySelector('.selected.result-item');
                document.querySelector('.site-list-group').scrollTo(0, selectedItem.offsetTop);
              });
            } else {
              this.newItem();
              $('#search').popover('enable').popover('show');
              setTimeout(() => {
                $('#search').popover('hide').popover('disable');
              }, 4000);
            }
          } else {
            console.log(response);
          }
        });  
    },
    updateDbItem(newValues) {
      newValues.code = this.encodeCodeText(newValues.code);
      const postData = this.prepareFormData(newValues);
      const config = {
        headers: { 
          'Content-type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        }
      };
      axios.post('/code/update-code', postData, config)
        .then(response => {
          if (response.data.message === 'success') {
            this.showAlert(this.tr('NOTICE_UPDATED'), 'info');
    
            const resultItem = this.getSelectedItem();
            const savedValues = this.formatItemFields(response.data.item);
            this.setItemValues(resultItem, savedValues);
            
            const id = this.resultsAll.findIndex(el => el.id === resultItem.id);
            this.filterResults();
            
            if (resultItem.id === this.selected) {
              this.setInputValues(resultItem);
            } else {
              this.newItem();
              this.results = this.results.filter(el => el.id !== resultItem.id);
              $('#search').popover('enable').popover('show');
              setTimeout(() => {
                $('#search').popover('hide').popover('disable');
              }, 4000);
            }
          } else {
            this.showAlert(this.tr('NOTICE_UPDATE_FAILED'), 'danger');
            console.log(response);
          }
        });  
    },
    setItemValues (item, values) {
       Object.keys(values).forEach(key => {
        item[key] = values[key];
      });
    },
    setInputValues (item) {
      if (this.codeDescription === item.description &&
        this.codeKeywords === item.keywords &&
        this.code === item.code      
      ) {
          return;
      }
      this.codeDescription = item.description;
      this.codeKeywords = item.keywords;
      this.setCode(item.code);
    },
    prepareFormData(item) {
      let result = '';
      Object.keys(item).forEach(key => {
        result += encodeURI('Code[' + key + ']=' + item[key] + '&');
      });
      result += encodeURI('search=' + this.search + '&');
      result += encodeURI('categories=' + this.searchCategories + '&');
      result += encodeURI(yii.getCsrfParam() + '=' + yii.getCsrfToken());
      return result;
    },
    confirmNewItem() {
      if (this.changed) {
        this.positiveModalAction = { 'type': 'newItem' };
        this.modalCancelButton = this.tr('MODAL_BUTTON_CANCEL');
        this.modalActionButton = this.tr('MODAL_BUTTON_CONTINUE');
        this.modalTitle = this.tr('MODAL_TITLE_UNSAVED_CHANGES');
        this.modalText = this.tr('MODAL_TEXT_CONFIRM_UNSAVED_CHANGES');
        $('#myModal').modal('show');
      } else {
        this.newItem();
      }
    },
    openShareItemDialog() {
      const config = {
        headers: { 
          'Content-type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
          'X-CSRF-TOKEN': yii.getCsrfToken()
        }
      };

      axios.post('/code/get-share-link/' + this.selected, null, config)
        .then(response => {
          if (response.data.message === 'success') {
            this.itemShareLink = response.data.itemShareLink;
            this.itemShareToken = response.data.itemShareToken;
            this.itemShareData = response.data.itemShareData;
            this.showCopyLink = true;
              
            this.positiveModalAction = { 'type': 'shareItem' };
            this.negativeModalAction = { 'type': 'stopSharingItem' };
            this.modalCancelButton = this.tr('MODAL_BUTTON_STOP_SHARING');
            this.modalActionButton = this.tr('MODAL_BUTTON_SHARE');
            this.modalTitle = this.tr('MODAL_TITLE_SHARE_CODE');
            this.modalText = this.itemShareLink;
            $('#myModal').modal('show');
          } else {
            console.log(response);
          }
        });
    },
    shareItem() {
      let postData = encodeURI('Code[id]=' + this.selected + '&');
      postData += encodeURI('Code[share_token]=' + this.itemShareData + '&');
      postData += encodeURI(yii.getCsrfParam() + '=' + yii.getCsrfToken());
        
      const config = {
        headers: { 
          'Content-type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        }
      };

      axios.post('/code/share', postData, config)
        .then(response => {
          if (response.data.message === 'success') {
            this.getSelectedItem().shareToken = this.itemShareToken;
          } else {
            console.log(response);
          }
        });
    },
    stopSharingItem() {
      if (!this.showCopyLink) {
        this.copyTextToClipboard(this.tr('MESSAGE_CODE_WAS_NOT_SHARED'), 
          document.querySelector('.site-modal-copy-input'));
      }
      const config = {
        headers: { 
          'Content-type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
          'X-CSRF-TOKEN': yii.getCsrfToken()
        }
      };

      axios.post('/code/stop-sharing/' + this.selected, null, config)
        .then(response => {
          if (response.data.message === 'success') {
            this.itemShareLink = '';
            this.itemShareToken = '';
            this.itemShareData = '';
            this.getSelectedItem().shareToken = null; 
          } else {
            console.log(response);
          }
        });
    },
    newItem() {
        this.selected = 0;
        this.codeDescription = '';
        this.codeKeywords = '';
        this.setCodeCategories([]);
        this.setCode('');
        this.codeLanguageId = this.defaultLanguageId;
    },
    showAlert (text, alertColor) {
      this.alertColor = alertColor;
      this.alertText = text;
      this.alertShow = true;
      
      if (this.alertTimeout !== null) {
        clearTimeout(this.alertTimeout);
      }
      
      this.alertTimeout = setTimeout(() => {
        this.alertShow = false;    
      }, 2000);
    },
    copyCodeToClipboard () {
      if (!this.code.length) {
        return;
      }
      this.copyTextToClipboard(this.code);
      this.showAlert(this.tr('COPY_CODE_TO_CLIPBOARD_SUCCESS'), 'info');
    },
    copyTextToClipboard (text, el) {
      navigator.clipboard.writeText(text);
    },
    copyShareLinkToClipboard () {
      const link = this.shareLink + this.getSelectedItem().shareToken;
      this.copyTextToClipboard(link);
      $('.site-code-shared').popover('hide');
      this.showAlert(this.tr('COPY_SHARE_LINK_TO_CLIPBOARD'), 'info');
    },
    onModalCopyLink () {
      this.showCopyLink = false;
      this.copyTextToClipboard(this.itemShareLink, document.querySelector('.site-modal-copy-input'));
    },
    decodeCodeText(code) {
      return code
        .replace(/_n_/g, '\n')
        .replace(/_amp_/g, '&')
        .replace(/_plus_/g, '+');
    },
    formatItemFields (item) {
      const offset = new Date().getTimezoneOffset() * -60;
      return {
        id: parseInt(item.id),
        description: item.description,
        keywords: item.keywords,
        categories: item.categories,
        code: this.decodeCodeText(item.code),
        languageId: item.language_id,
        shareToken: item.share_token,
        created: parseInt(item.created_at) + offset,
        updated: parseInt(item.updated_at) + offset
      };
    },
    onSearchCriteriaInput1() {
      let values = {
        description: this.search,
        category_list: this.searchCategories
      };
      const postData = this.prepareFormData(values);
      const config = {
        headers: { 
          'Content-type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        }
      };
      axios.post('/code/search-code', postData, config)
        .then(response => {
          const data = response.data;
          this.results = [];
          data.forEach(item => {
            this.results.push(this.formatItemFields(item));
          });
          if (this.getSelectedItem() === null) {
            this.newItem();
          }
      });
    },
    onSearchCriteriaInput () {
      this.filterResults();
    },
    clearSearch () {
      this.search = '';
      this.onSearchCriteriaInput();
    },
    matchedCategories(categoryList) {
      const list = categoryList.filter(val => this.searchCategories.includes(val));
      return list.length > 0 && categoryList.length > 0;
    },
    filterResults () {
        const str = this.search.toLowerCase();
        if (!this.search.length && !this.searchCategories.length) {
            this.results = this.resultsAll;
        } else if (!this.searchCategories.length) {
            this.results = this.resultsAll.filter(el => {
                return (el.description.toLowerCase().indexOf(str) !== -1 ||
                    el.keywords.toLowerCase().indexOf(str) !== -1 ||
                    el.code.toLowerCase().indexOf(str) !== -1)
                });
        } else {
            this.results = this.resultsAll.filter(el => {
                return (el.description.toLowerCase().indexOf(str) !== -1 ||
                    el.keywords.toLowerCase().indexOf(str) !== -1 ||
                    el.code.toLowerCase().indexOf(str) !== -1) &&
                    this.matchedCategories(el.categories)
                });
        }
        if (this.results.filter(el => el.id === this.selected).length === 0) {
           this.newItem();
        }
    },
    tr (placeholder) {
      let translation;
      if (this.placeholders.indexOf(placeholder) === -1) {
        translation = placeholder;
      } else {
        translation = this.messages[placeholder];
      }
      return translation;
    },
    setupEditor () {
      this.editor = ace.edit('editor');
      // editor.setTheme('ace/theme/monokai');
      this.editor.session.setTabSize(2);
      this.editor.setShowFoldWidgets(false);
      this.editor.setShowPrintMargin(false);
      this.editor.getSession().setUseWorker(false);
      const val = $('#code-language').val() === 'cpp' ? 
        'c++' : $('#code-language').val();

      this.editor.on('change', e => {
        this.code = this.editor.getValue();
      });
    },
    onChangeLanguage () {
      this.editor.session.setMode('ace/mode/' + $('#code-language').val());
    }
  },
  mounted () {
    window.myData.data.forEach(item => {
      this.resultsAll.push(this.formatItemFields(item));
    });
    this.results = this.resultsAll;
    
    this.messages = window.myData.messages;
    this.placeholders = Object.keys(this.messages);
    
    this.shareLink = window.myData.shareLink;
    this.defaultLanguageId = window.myData.defaultLanguageId;
        
    delete window.myData;
    
    this.codeLanguageId = this.defaultLanguageId;
    
    document.querySelectorAll('.site-hide').forEach(element => {
      element.classList.remove('site-hide');
    });
    
    $('.site-code-shared').popover({
        content: $('.site-popover-body')
    });
    
    $('#myModal').on('hidden.bs.modal', () => {
      this.showCopyLink = false;
    });
    
    onFilterCategoryChange = this.setSearchCategories;
    onItemCategoryChange = this.setSelectedCodeCategories;
    
    this.setupEditor();
        
    if (this.results.length) {
      this.selectResult(this.results[0].id);
    }
  }
});
