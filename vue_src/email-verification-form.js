import Vue from 'vue';
import axios from 'axios';
import {cookieMixin} from './cookieMixin';
import {myMixin} from './myMixin';
import {init} from './init';
init();

new Vue({
  el: '#app',
  mixins: [cookieMixin, myMixin],
  data () {
    return {
      noMixinMount: true,
      loading: false,
      email: ''
    };
  },
  methods: {
    send () {
      if (!this.email.length) {
        return ;
      }
        
      let postData = encodeURI('User[email]=' + this.email + '&');
      postData += encodeURI(yii.getCsrfParam() + '=' + yii.getCsrfToken());
      
      const config = {
        headers: { 
          'Content-type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        }
      };
            
      this.loading = true;
      const seconds = Math.floor(Date.now() / 1000);
      axios.post('/user/send-verification-email', postData, config)
          .then(response => {
            setTimeout(() => {
                this.loading = false;
            }, Math.floor(Date.now() / 1000) - seconds < 1 ? 1500 : 0);
            if (response.data.message === 'success') {
              this.showAlert(this.tr('VERIFICATION_EMAIL_SEND_SUCCESS'), 'success');
            } else {
              this.showAlert(response.data.error, 'danger');
            }
          });
    },
    emailInput () {
      if (!this.alertShow) {
        return;
      }
      if (this.alertTimeout !== null) {
        clearTimeout(this.alertTimeout);
      }
      this.alertShow = false;
    },
    showAlert (text, alertColor) {
      if (this.alertTimeout !== null) {
        clearTimeout(this.alertTimeout);
      }
      
      if (this.alertShow) {
        this.alertShow = false;
        this.alertTimeout = setTimeout(() => {
          this.showAlert(text, alertColor);
        }, 500);
        return;
      }
      
      this.alertColor = alertColor;
      this.alertText = text;
      this.alertShow = true;
    },
    tr (placeholder) {
      let translation;
      if (this.placeholders.indexOf(placeholder) === -1) {
        translation = placeholder;
      } else {
        translation = this.messages[placeholder];
      }
      return translation;
    }
  },
  mounted () {
    this.messages = window.myData.messages;
    this.placeholders = Object.keys(this.messages); 
    this.email = window.myData.email;
    delete window.myData;
    
    document.querySelectorAll('.site-hide').forEach(element => {
      element.classList.remove('site-hide');
    });
  }
});
