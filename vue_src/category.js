import Vue from 'vue';
import axios from 'axios';
import {cookieMixin} from './cookieMixin';
import {init} from './init';
init();

const app = new Vue({
  el: '#app',
  mixins: [cookieMixin],
  data () {
    return {
      messages: {},
      placeholders: [],
      search: '',
      selected: 0,
      hidePopoverTimeout: null,
      resultsAll: [],
      results: [],
      modalTitle: '',
      positiveModalAction: {},
      negativeModalAction: {},
      modalCancelButton: '',
      modalActionButton: '',
      modalText: '',
      modalValue: '',
      modalId: '',
      alertShow: false,
      alertColor: 'info',
      alertText: '',
      alertTimeout: null
    };
  },
  computed: {
    isSelected () {
      return this.selected !== 0;
    },
    canDelete() {
      const item = this.getSelectedItem();
      
      if (item === null) {
        return false;
      }
      return item.canDelete;
    }
  },
  filters: {
    timeFilter (value) {
      return new Date(value * 1000).toISOString().slice(0, -5).replace('T', ' ');
    }
  },
  methods: {
    selectResult(id) {
      if (this.selected !== id) {
        this.selected = id;
        const result = this.results.filter(el => el.id === id)[0];
        this.name = result.name;
      }
    },
    getSelectedItem() {
      if (this.selected === 0 || this.results.length === 0) {
        return null;
      }
      const items = this.results.filter(el => el.id === this.selected);
      if (items.length === 1) {
        return items[0];
      }
      return null;
    },
    createNewItem() {
      this.positiveModalAction = { 'type': 'newItem' };
      this.modalCancelButton = this.tr('MODAL_BUTTON_CANCEL');
      this.modalActionButton = this.tr('MODAL_BUTTON_OK');
      this.modalTitle = this.tr('MODAL_TITLE_CREATE_CATEGORY');
      this.modalText = '';
      this.modalValue = '';
      this.resetName();
      this.modalId = 0;
      $('#editModal').modal('show');  
    },
    editItem() {
      if (this.isSelected) {
        this.positiveModalAction = { 'type': 'editItem' };
        this.modalCancelButton = this.tr('MODAL_BUTTON_CANCEL');
        this.modalActionButton = this.tr('MODAL_BUTTON_OK');
        this.modalTitle = this.tr('MODAL_TITLE_EDIT_CATEGORY');
        this.modalText = '';
        this.resetName();
        const item = this.getSelectedItem();
        this.modalValue = item.name;
        this.modalId = item.id;
        $('#editModal').modal('show');
      }
    },
    resetName() {
      $('#category-name').removeClass('is-invalid');
      $('#category-name').removeClass('is-valid');
      $('#category-name').siblings()[1].innerHTML = '';
    },
    onSortUp() {
      let item = this.getSelectedItem();
      
      this.resultsAll.sort((a,b) => a.order - b.order);
      
      if (this.resultsAll.length < 2) {
        return;
      }
      
      let i = 0;
      while (this.resultsAll[i].id !== item.id) {
        i++;
      }
      
      if (i === 0) {
        return;
      }
      
      item.order = this.resultsAll[i - 1].order;
      let order = item.order;
      
      let postData = this.prepareFormData(item);
      const config = {
        headers: { 
          'Content-type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        }
      };
      
      axios.post('/category/update', postData, config)
        .then(response => {
          if (response.data.message === 'success') {
            const id = this.resultsAll.findIndex(el => el.id === item.id);
            this.resultsAll[id] = this.formatItemFields(response.data.item);
            this.filterResults();
          } else {
            console.log(response);
          }
        });
            
      let movedItem = this.resultsAll[i - 1];
      movedItem.order = order + 1;
      postData = this.prepareFormData(movedItem);
      
      axios.post('/category/update', postData, config)
        .then(response => {
          if (response.data.message === 'success') {
            const id = this.resultsAll.findIndex(el => el.id === movedItem.id);
            this.resultsAll[id] = this.formatItemFields(response.data.item);
            this.filterResults();
          } else {
            console.log(response);
          }
        });
    },
    onSortDown() {
      let item = this.getSelectedItem();
      
      this.resultsAll.sort((a,b) => a.order - b.order);
      
      if (this.resultsAll.length < 2) {
        return;
      }
      
      let i = 0;
      while (this.resultsAll[i].id !== item.id) {
        i++;
      }
      
      if (i === this.resultsAll.length - 1) {
        return;
      }
      
      item.order = this.resultsAll[i + 1].order;
      let order = item.order;
      
      let postData = this.prepareFormData(item);
      const config = {
        headers: { 
          'Content-type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        }
      };
      
      axios.post('/category/update', postData, config)
        .then(response => {
          if (response.data.message === 'success') {
            const id = this.resultsAll.findIndex(el => el.id === item.id);
            this.resultsAll[id] = this.formatItemFields(response.data.item);
            this.filterResults();
          } else {
            console.log(response);
          }
        });
      
      let movedItem = this.resultsAll[i + 1]; 
      movedItem.order = order - 1;
      postData = this.prepareFormData(movedItem);
      
      axios.post('/category/update', postData, config)
        .then(response => {
          if (response.data.message === 'success') {
            const id = this.resultsAll.findIndex(el => el.id === movedItem.id);
            this.resultsAll[id] = this.formatItemFields(response.data.item);
            this.filterResults();
          } else {
            console.log(response);
          }
        });
    },
    confirmRemove() {
      const item = this.getSelectedItem();

      if (item) {
        this.positiveModalAction = { type: 'removeItem' };
        this.modalTitle = this.tr('MODAL_TITLE_CONFIRM_DELETE');
        this.modalCancelButton = this.tr('MODAL_BUTTON_CANCEL');
        this.modalActionButton = this.tr('MODAL_BUTTON_DELETE');
        this.modalText = this.tr('MODAL_TEXT_CONFIRM_DELETE_CATEGORY') + ' \'' + item.name + '\' ?';
        
        if (item.isAssigned) {
            this.modalText += '<br><span class="modal-warning">' + this.tr('MODAL_CATEGORY_IS_USED') + '</span>';
        }
        
        $('#myModal').modal('show');
      }
    },
    removeItem() {
      const id = this.selected;
      if (id > 0) {
        const item = this.results.filter(el => el.id === id)[0];
        
        const config = {
          headers: { 
            'Content-type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN': yii.getCsrfToken()
          }
        };
        
        axios.post('/category/remove/' + item.id, null, config)
          .then(response => {
            if (response.data.message === 'success') {
              this.resultsAll = this.resultsAll.filter(el => el.id !== id);  
              this.results = this.results.filter(el => el.id !== id);
              this.selected = 0;
              this.showAlert(this.tr('NOTICE_DELETED'), 'info');
            } else {
              console.log(response);
            }
          });
      }
    },
    onPositiveModalAction () {
      $('#myModal').modal('hide');
      switch(this.positiveModalAction.type) {
        case 'selectResult':
          this.selectResult(this.positiveModalAction.id);
          break;
        case 'newItem':
          this.validateForm(this.createDbItem);
          break;
        case 'editItem':
          this.validateForm(this.updateDbItem);
          break;
        case 'removeItem':
          this.removeItem();
          break;
      }
    },
    onNegativeModalAction () {
      if (!this.negativeModalAction.type) {
        this.negativeModalAction.type = '';
      }
      switch(this.negativeModalAction.type) {
        case 'stopSharingItem':
          this.stopSharingItem();
          break;
      }
    },
    showFiltered(values) {
      return values.name.toLowerCase().indexOf(this.search.toLowerCase()) !== -1;
    },
    createDbItem() {
      const item = this.getSelectedItem();
      const val = item === null ? 1 : item.order;
      
      const values = {
        name: this.modalValue,
        order: val
      };
      
      const postData = this.prepareFormData(values);
      const config = {
        headers: { 
          'Content-type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        }
      };
      axios.post('/category/create', postData, config)
        .then(response => {
          if (response.data.message === 'success') {
            
            this.showAlert(this.tr('NOTICE_CREATED'), 'info');
            
            this.resultsAll = [];
            response.data.items.forEach(item => {
              this.resultsAll.push(this.formatItemFields(item));
            });
            
            const item = this.resultsAll.filter(el => el.id === response.data.id)[0];
            
            if (this.showFiltered(item)) {
              this.selected = item.id;
              this.filterResults();
              
              this.$nextTick(() => {
                const selectedItem = document.querySelector('.selected.result-item');
                document.querySelector('.category-site-list-group').scrollTo(0, selectedItem.offsetTop);
              });
            } else {
              $('#search').popover('enable').popover('show');
              setTimeout(() => {
                $('#search').popover('hide').popover('disable');
              }, 4000);
            }
          } else {
            console.log(response);
          }
          $('#editModal').modal('hide');
        });  
    },
    updateDbItem() {
      let item = this.getSelectedItem();  
      item.name = this.modalValue;
      const postData = this.prepareFormData(item);
      const config = {
        headers: { 
          'Content-type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        }
      };
      axios.post('/category/update', postData, config)
        .then(response => {
          if (response.data.message === 'success') {
            this.showAlert(this.tr('NOTICE_UPDATED'), 'info');
    
            const savedItem = this.formatItemFields(response.data.item);
            const id = this.resultsAll.findIndex(el => el.id === savedItem.id);
            this.resultsAll[id] = savedItem;
           
            this.filterResults();
            
            if (!this.showFiltered(this.resultsAll[id])) {
              $('#search').popover('enable').popover('show');
              setTimeout(() => {
                $('#search').popover('hide').popover('disable');
              }, 4000);
            }
          } else {
            this.showAlert(this.tr('NOTICE_UPDATE_FAILED'), 'danger');
            console.log(response);
          }
          $('#editModal').modal('hide');
        });  
    },
    setItemValues (item, values) {
       Object.keys(values).forEach(key => {
        item[key] = values[key];
      });
    },
    setInputValues (item) {
      if (this.codeDescription === item.description &&
        this.codeKeywords === item.keywords &&
        this.code === item.code      
      ) {
          return;
      }
      this.codeDescription = item.description;
      this.codeKeywords = item.keywords;
      this.setCode(item.code);
    },
    prepareFormData(item) {
      let result = '';
      Object.keys(item).forEach(key => {
        result += encodeURI('Category[' + key + ']=' + item[key] + '&');
      });
      result += encodeURI(yii.getCsrfParam() + '=' + yii.getCsrfToken());
      return result;
    },
    showAlert (text, alertColor) {
      this.alertColor = alertColor;
      this.alertText = text;
      this.alertShow = true;
      
      if (this.alertTimeout !== null) {
        clearTimeout(this.alertTimeout);
      }
      
      this.alertTimeout = setTimeout(() => {
        this.alertShow = false;    
      }, 2000);
    },
    setTime(time) {
      const offset = new Date().getTimezoneOffset() * -60;
      return parseInt(time) + offset;
    },
    formatItemFields (item) {
      return {
        id: parseInt(item.id),
        name: item.name,
        user_id: parseInt(item.user_id),
        order: parseInt(item.order),
        isAssigned: item.isAssigned,
        created: this.setTime(item.created_at),
        updated: this.setTime(item.updated_at)
      };
    },
    onSearchCriteriaInput () {
      this.filterResults();
    },
    clearSearch () {
      this.search = '';
      this.onSearchCriteriaInput();
    },
    filterResults () {
        const str = this.search.toLowerCase();
        if (!this.search.length) {
            this.results = this.resultsAll;
        } else {
            this.results = this.resultsAll.filter(el => {
                return (el.name.toLowerCase().indexOf(str) !== -1);
            });
        }
        
        this.results.sort((a,b) => a.order - b.order);
        
        if (this.results.length === 0) {
            this.selected = 0;
        }
    },
    tr (placeholder) {
      let translation;
      if (this.placeholders.indexOf(placeholder) === -1) {
        translation = placeholder;
      } else {
        translation = this.messages[placeholder];
      }
      return translation;
    },
    validateForm (callback) {
      this.validateFormAttributes();
      setTimeout(() => {
        if (this.getForm().find('.is-invalid').length === 0) {
          callback(this);
        }
      }, 300);
    },
    validateFormAttributes () {
      this.iterateFormAttributes(el => {
        el.status = 3;
      });
      this.getForm().yiiActiveForm("validate");  
    },
    getForm() {
      return $("#edit-name-form");
    },
    iterateFormAttributes (callback) {
      const data = this.getForm().data("yiiActiveForm");
      $.each(data.attributes, function() {
        callback(this);
      });
    }
  },
  mounted () {
    window.myData.data.forEach(item => {
      this.resultsAll.push(this.formatItemFields(item));
    });
    
    this.results = this.resultsAll;
    
    this.messages = window.myData.messages;
    this.placeholders = Object.keys(this.messages);
            
    delete window.myData;
    
    $('#myModal').on('hidden.bs.modal', () => {
      this.showCopyLink = false;
    });
        
    if (this.results.length) {
      this.selectResult(this.results[0].id);
    }
    
    $('#edit-name-form').on('beforeSubmit', function (e) {
        return false;
    });
  }
});
