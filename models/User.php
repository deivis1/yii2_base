<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;
use Yii;
use yii\helpers\Url;
use app\components\Util;
use app\models\Category;
use app\models\Code;
use app\models\UserLog;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property string $auth_key
 * @property string $password_hash
 * @property string $email_verification_token
 * @property string $password_reset_token
 * @property integer $role
 * @property integer $locked
 * @property integer $language
 * @property integer $created_at
 * @property integer $updated_at
 * @property UserLog[] $userLogs
 * @property Category[] $categories
 * @property Code[] $codes
 */
class User extends ActiveRecord implements IdentityInterface
{
    /** @const SCENARIO_SIGNUP Scenario type */
    const SCENARIO_SIGNUP = 'signup';
    
    /** @const SCENARIO_CREATE Scenario type */
    const SCENARIO_CREATE = 'create';
    
    /** @const SCENARIO_VALIDATE_EMAIL Scenario type */
    const SCENARIO_VALIDATE_EMAIL = 'validate_email';
    
    /** @const SCENARIO_PASSWORD_RESET_REQUEST Scenario type */
    const SCENARIO_PASSWORD_RESET_REQUEST = 'password reset request';
    
    /** @const SCENARIO_PASSWORD_RESET Scenario type */
    const SCENARIO_PASSWORD_RESET = 'password reset';
    
    /** @const SCENARIO_PASSWORD_CHANGE Scenario type */
    const SCENARIO_CHANGE_PASSWORD = 'change password';
    
    /** @const SCENARIO_RESEND_EMAIL_VERIFICATION Scenario type */
    const SCENARIO_RESEND_EMAIL_VERIFICATION = 'resend email verification';
    
     /** @const SCENARIO_EDIT Scenario type */
    const SCENARIO_UPDATE = 'update';
    
    /** @const SCENARIO_SEARCH Scenario type */
    const SCENARIO_SEARCH = 'search';
    
     /** @const SCENARIO_SECURITY Scenario type */
    const SCENARIO_SECURITY = 'security';
    
     /** @const User role */
    const ROLE_ADMIN = 1;
    
     /** @const User role */
    const ROLE_USER = 2;
    
     /** @const User lock state */
    const LOCKED_NO = 0;
    
     /** @const User lock state */
    const LOCKED_YES = 1;
    
     /** @const User language id */
    const LANGUAGE_EN = 1;
    
     /** @const User language id */
    const LANGUAGE_LT = 2;
    
     /** @const User language name */
    const LANGUAGE_NAME_EN = 'en';
    
     /** @const User language name */
    const LANGUAGE_NAME_LT = 'lt';
   
    public $currentPassword;
    public $password;
    public $passwordConfirm;
    public $verifyCode;
  
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_SIGNUP => [
                'email',
                'name',
                'password',
                'passwordConfirm',
                'language',
                'verifyCode',
            ],
            static::SCENARIO_VALIDATE_EMAIL => [
                'email',
            ],
            static::SCENARIO_PASSWORD_RESET_REQUEST => [
                'email',
            ],
            static::SCENARIO_PASSWORD_RESET => [
                'password',
                'passwordConfirm',
            ],
            static::SCENARIO_CHANGE_PASSWORD => [
                'currentPassword',
                'password',
                'passwordConfirm',
            ],
            static::SCENARIO_RESEND_EMAIL_VERIFICATION => [
                'email',
            ],
            self::SCENARIO_CREATE => [
                'id',
                'email',
                'name',
                'role',
                'locked',
                'language',
                'created_at',
                'updated_at',
            ],
            self::SCENARIO_UPDATE => [
                'email',
                'name',
                'language',
                'created_at',
                'updated_at',
            ],
            self::SCENARIO_SEARCH => [
                'id',
                'email',
                'name',
                'role',
                'locked',
                'language',
                'created_at',
                'updated_at',
            ],
            self::SCENARIO_SECURITY => [
                'id',
                'auth_key',
                'password_hash',
                'email_verification_token',
                'password_reset_token',
                'created_at',
                'updated_at',
            ],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // id
            ['id', 'integer', 'min' => 0],
            ['id', 'unique', 'on' => static::SCENARIO_CREATE],
            ['id', 'required', 'on' => static::SCENARIO_UPDATE],
            
            // email
            ['email', 'required'],
            ['email', 'unique', 'on' => [
                static::SCENARIO_SIGNUP,
                static::SCENARIO_CREATE,
                static::SCENARIO_UPDATE,
            ]],
            ['email', 'exist', 
                'targetClass' => static::className(), 
                'targetAttribute' => 'email',
                'on' => static::SCENARIO_PASSWORD_RESET_REQUEST,
                'message' => Yii::t('app', 'MODEL_INVALID_EMAIL'),
            ],
            ['email', 'email'],
            ['email', 'validateResendVerification', 
                'on' => static::SCENARIO_RESEND_EMAIL_VERIFICATION
            ],
            
            // name
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'max' => 255],
            ['name', 'match', 'pattern' => '/^[a-zą-žа-яà-ỳあ-ん\s-]*$/ui',
                'message' => Yii::t('app', 'USER_NAME_INVALID_PATTERN'),
                'except' => self::SCENARIO_SEARCH
            ],
            
            // auth_key
            ['auth_key', 'required', 
                'on' => self::SCENARIO_SECURITY
            ],
            ['auth_key', 'string', 'min' => 32, 'max' => 32],
            
            // password_hash
            ['password_hash', 'required', 
                'on' => self::SCENARIO_SECURITY
            ],
            ['password_hash', 'string', 'min' => 50, 'max' => 100,
                'on' => self::SCENARIO_SECURITY
            ],

            // email_verification_token
            ['email_verification_token', 'string', 'max' => 100],
            
            // password_reset_token
            ['password_reset_token', 'string', 'max' => 100],
            
            // currentPassword
            ['currentPassword', 'required', 'on' => static::SCENARIO_CHANGE_PASSWORD],
            ['currentPassword', 'string', 'min' => 8, 'max' => 20, 'on' => static::SCENARIO_CHANGE_PASSWORD],
            ['currentPassword', 'validateCurrentPassword', 'on' => static::SCENARIO_CHANGE_PASSWORD],
            
            // password
            ['password', 'required'],
            ['password', 'string', 'min' => 8, 'max' => 20],
            
            // passwordConfirm
            ['passwordConfirm', 'required'],
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password',
                'message' => Yii::t('app', $this->scenario !== static::SCENARIO_CHANGE_PASSWORD ?
                  'PASSWORD_CONFIRMATION_DOES_NOT_MATCH' :
                  'NEW_PASSWORD_CONFIRMATION_DOES_NOT_MATCH'),
            ],
            
            // role
            ['role', 'required'],
            ['role', 'integer'],
            ['role', 'in', 'range' => [
                static::ROLE_ADMIN,
                static::ROLE_USER,
            ]],
            
            // locked
            ['locked', 'required'],
            ['locked', 'integer'],
            ['locked', 'in', 'range' => [
                static::LOCKED_NO,
                static::LOCKED_YES,
            ]],
            
            // language
            ['language', 'required'],
            ['language', 'integer'],
            ['language', 'in', 'range' => [
                static::LANGUAGE_EN,
                static::LANGUAGE_LT,
            ]],
            
            // created_at
            ['created_at', 'integer'],

            // updated_at
            ['updated_at', 'integer'],
            
            // verifyCode
            ['verifyCode', 'captcha'],
        ];
    }
    
    /**
     * Validates if email either is logged user's email or 
     * 
     * brand new email not belonging to another user
     *
     * @param string $attribute the attribute currently being validated
     * @param mixed $params the value of the "params" given in the rule
     */
    public function validateResendVerification($attribute, $params)
    {
        $email = $this->$attribute;
        $currentUser = Yii::$app->user->isGuest ? null : 
            Yii::$app->user->identity;
        
        if (!empty($email) && $email == $currentUser->email) {
            return;
        }
        
        $user = static::findByUsername($email);
        
        if (!is_null($user)) {
            $this->addError($attribute, Yii::t('app', 'MODEL_INVALID_EMAIL'));
        }
    }
    
    /**
     * Validates the current user password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateCurrentPassword($attribute, $params)
    {
        if (!$this->hasErrors()) {

            if (!$this->validatePassword($this->$attribute)) {
                $this->addError(
                    $attribute, 
                    Yii::t('app', 'USER_INVALID_CURRENT_PASSWORD'
                ));
            }
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'USER_LABEL_ID'),
            'email' => Yii::t('app', 'USER_LABEL_EMAIL'),
            'name' => Yii::t('app', 'USER_LABEL_NAME'),
            'currentPassword' => Yii::t('app', 'USER_LABEL_CURRENT_PASSWORD'),
            'password' => Yii::t('app', 'USER_LABEL_PASSWORD'),
            'passwordConfirm' => Yii::t('app', 'USER_LABEL_PASSWORD_CONFIRM'),
            'verifyCode' => Yii::t('app', 'USER_LABEL_VERIFY_CODE'),
            'auth_key' => Yii::t('app', 'USER_LABEL_AUTH_KEY'),
            'password_hash' => Yii::t('app', 'USER_LABEL_PASSWORD_HASH'),
            'email_verification_token' => Yii::t('app', 'USER_LABEL_VERIFY_EMAIL_TOKEN'),
            'password_reset_token' => Yii::t('app', 'USER_LABEL_PASSWORD_RESET_TOKEN'),
            'role' => Yii::t('app', 'USER_LABEL_ROLE'),
            'locked' => Yii::t('app', 'USER_LABEL_LOCKED'),
            'language' => Yii::t('app', 'USER_LABEL_LANGUAGE'),
            'created_at' => Yii::t('app', 'USER_LABEL_CREATED_AT'),
            'updated_at' => Yii::t('app', 'USER_LABEL_UPDATED_AT'),
        ];
    }
    
    /**
     * Loads model
     * 
     * @return Item model 
     * @throws NotFoundHttpException
     */
    public static function loadModel($id)
    {
        $model = static::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException(Yii::t('app', 'MODEL_NOT_FOUND', 
                ['class' => basename(str_replace('\\', '/', static::class))])
            );
        }
        return $model;
    }
    
    /**
     * @return ActiveQuery
     */
    public function getUserLogs()
    {
        return $this->hasMany(UserLog::className(), ['user_id' => 'id']);
    }
    
    /**
     * @return ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['user_id' => 'id']);
    }
    
    /**
     * @return ActiveQuery
     */
    public function getCodes()
    {
        return $this->hasMany(Code::className(), ['user_id' => 'id']);
    }
    
    /**
     * Model used for search
     *
     * @param array $params Attributes list.
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->distinct()
            ->where(['!=', 'id',  1])
            ->andWhere(['locked' => static::LOCKED_NO]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'email',
                'name',
                'email_verification_token',
                'auth_key',
                'password_hash',
                'password_reset_token',
                'role',
                'locked',
                'language',
                'created_at',
                'updated_at',
            ],
            'defaultOrder' => ['id' => SORT_ASC],
        ]);

        $this->setScenario(self::SCENARIO_SEARCH);
        if (!($this->load($params))) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['language' => $this->language]);

        return $dataProvider;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'locked' => self::LOCKED_NO]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $users = static::find()
            ->where([
                'auth_key' => $token, 
                'locked' => self::LOCKED_NO
            ])->andWhere(['IS NOT', 'auth_key', null])
            ->all();
        
        if (count($users) !== 1) {
            return null;
        }
        
        return $users[0];
    }

    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByUsername($email)
    {
        if (empty($email)) {
            return null;  
        }
        
        $users = static::find()
            ->where([
                'email' => $email, 
                'locked' => self::LOCKED_NO
            ])->andWhere(['IS NOT', 'email', null])
            ->all();
        
        if (count($users) !== 1) {
            return null;
        }
        
        return $users[0];
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if (empty($password)) {
            return false;
        }
        
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }
    
    /**
     * Checks whether user's email is verified
     * 
     * @return boolean
     */
    public function isEmailVerified()
    {
        return is_null($this->email_verification_token);
    }
    
    /**
     * Returns mapped available user language array
     *
     * @return array
     */
    public static function getMappedLanguages()
    {
        return [
            static::LANGUAGE_EN => Yii::t('app', 'LANGUAGE_EN'),
            static::LANGUAGE_LT => Yii::t('app', 'LANGUAGE_LT'),
        ];
    }
    
    /**
     * Returns mapped available user languages id => name
     *
     * @return array
     */
    public static function getMappedLanguageNames()
    {
        return [
            static::LANGUAGE_EN => static::LANGUAGE_NAME_EN,
            static::LANGUAGE_LT => static::LANGUAGE_NAME_LT,
        ];
    }
    
    /**
     * Returns language id by specified language name
     *
     * @param string 
     * @return integer|null Null if language id by name was not found
     */
    public static function getLanguageId($language)
    {
        $languageNames = static::getMappedLanguageNames();
        foreach($languageNames as $key => $value) {
            if ($value === $language) {
                return $key;
            }
        }
        return null;
    }
    
    /**
     * Returns user language name string
     *
     * @return string
     */
    public function getLanguageName()
    {
        return static::getMappedLanguageNames()[$this->language];
    }
    
    /**
     * Returns language name string by specified id
     *
     * @param integer
     * @return string
     */
    public static function getLanguageNameById($languageId)
    {
        return static::getMappedLanguageNames()[$languageId];
    }
    
    /**
     * Creates new user
     *
     * @return string
     */
    public function create()
    {
        $this->setScenario(static::SCENARIO_CREATE);
        $this->auth_key = Yii::$app->security->generateRandomString();
        $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
        $this->password_reset_token = null;
        $this->role = static::ROLE_USER;
        $this->locked = static::LOCKED_NO;
        $this->save();
        
        $this->sendVerificationEmail();
    }
    
    /**
     * Send password reset email to user
     * 
     * @return boolean whether message was successfully sent
     */
    public function sendRecoveryEmail()
    {
        $model = static::findByUsername($this->email);
        $model->setScenario(static::SCENARIO_SECURITY);
        $model->generatePasswordResetToken();
        $model->save();
        
        $token = Yii::$app->security->maskToken($model->password_reset_token);
        $url = Url::to(['/user/password-reset-form', 'token' => $token], true);
        
        $subject = Yii::t('app', 'PASSWORD_RESET_SUBJECT');
        $params = [
            'name' => $model->name,
            'startText' => Yii::t('app', 'PASSWORD_RESET_START_TEXT'),
            'actionText' => Yii::t('app', 'EMAIL_LINK'),
            'endText' => '',
            'actionUrl' => $url,
            'sender' => Yii::$app->name,
        ];
        
        return Util::sendEmail('user-security', $params, $model->email, $subject);
    }
    
    /**
     * Generates email verification token for new user and sends email with link
     * 
     * @return boolean whether message was successfully sent
     */
    public function sendVerificationEmail()
    {
        $model = static::findByUsername($this->email);
        $model->setScenario(static::SCENARIO_SECURITY);
        $model->email_verification_token = Yii::$app->security->generateRandomString();
        $model->save();
        
        $token = Yii::$app->security->maskToken($model->email_verification_token);
        $url = Url::to(['/user/verify-email', 'token' => $token], true);
        
        $subject = Yii::t('app', 'VERIFY_EMAIL_SUBJECT');
        
        $params = [
            'name' => $model->name,
            'startText' => Yii::t('app', 'VERIFY_EMAIL_START_TEXT'),
            'actionText' => Yii::t('app', 'EMAIL_LINK'),
            'endText' => '',
            'actionUrl' => $url,
            'sender' => Yii::$app->name,
        ];
        
        return Util::sendEmail('user-security', $params, $model->email, $subject);
    }
    
    /**
     * Generates password reset token
     */
    public function generatePasswordResetToken()
    {
        $time = time() + Yii::$app->params['passwordResetTokenExpires'];
        $this->password_reset_token = Yii::$app->security
            ->generateRandomString() . $time;
    }
    
    /**
     * Returns User model by specified token or null if user not found
     * 
     * @param string $tokenField
     * @param string $token
     * @param boolean $unmask
     * @return User|null
     */
    public static function findByToken($tokenField, $token , $unmask = false)
    {
        if (empty($token)) {
            return null;
        }
        
        if ($unmask) {
            $token = Yii::$app->security->unmaskToken($token);
        }
        
        if (empty($token)) {
            return null;
        }
      
        $users = static::find()
            ->where([
                $tokenField => $token,
                'locked' => static::LOCKED_NO
            ])->andWhere(['IS NOT', $tokenField, null])
            ->all();
        
        if (count($users) !== 1) {
            return null;
        }
        
        return $users[0];
    }
    
    /**
     * Checks if user token in specified field is expired
     * 
     * @param string $tokenField
     * @return boolean|null If token is invalid
     */
    public function isTokenExpired($tokenField)
    {
        $token = $this->$tokenField;
        
        if (empty($token) || strlen($token) !== 42) {
            return null;
        }
        
        $time = substr($this->$tokenField, -10);

        if (!is_numeric($time)) {
            return null;
        }
        
        return $time < time();
    }
    
    /**
     * Changes user password
     * 
     * @return User|null model
     */
    public function setPassword()
    {
        $this->setScenario(static::SCENARIO_SECURITY);
        
        if (empty($this->password)) {
             return null;
        }
        
        $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
        return $this->save() ? $this : null;
    }
    
    /**
     * Verifies if model id and logged in user id match
     * 
     * @return boolean
     */
    public function verifyIdentity()
    {
        if (empty($this->id)) {
            return false;
        }
        return $this->id === Yii::$app->user->identity->id;
    }
    
    /**
     * Set session language variable by user language
     * 
     * @return boolean
     */
    public function setAccountLanguage()
    {
        Yii::$app->session->set('lang', $this->languageName);
    }
    
    /**
     * Returns mapped user's code category list
     * 
     * @return array
     */
    public function getCategoryList()
    {
        $categories = Category::find()
            ->select(['id', 'name'])
            ->where(['user_id' => $this->id])
            ->orderBy(['order' => SORT_ASC])
            ->asArray()
            ->all();
        
        return ArrayHelper::map($categories, 'id', 'name');
    }
}
