<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;
use app\models\Category;
use app\models\Code;

/**
 * This is the model class for table "category_code".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $code_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property Category[] $categories
 * @property Code[] $codes
 */
class CategoryCode extends ActiveRecord
{
     /** @const SCENARIO_CREATE Scenario type */
    const SCENARIO_CREATE = 'create';
    
     /** @const SCENARIO_EDIT Scenario type */
    const SCENARIO_UPDATE = 'update';
  
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_code';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_CREATE => [
                'category_id',
                'code_id',
                'created_at',
                'updated_at',
            ],
            self::SCENARIO_UPDATE => [
                'id',
                'category_id',
                'code_id',
                'created_at',
                'updated_at',
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // id
            ['id', 'integer', 'min' => 0],
            ['id', 'unique', 'on' => static::SCENARIO_CREATE],
            ['id', 'required', 'on' => static::SCENARIO_UPDATE],
            
            // category_id
            ['category_id', 'required'],
            ['category_id', 'integer', 'min' => 0],
            ['category_id', 'exist', 
                'targetClass' => Category::className(), 
                'targetAttribute' => 'id',
            ],
            
            // code_id
            ['code_id', 'required'],
            ['code_id', 'integer', 'min' => 0],
            ['code_id', 'exist', 
                'targetClass' => Code::className(), 
                'targetAttribute' => 'id',
            ],
            
            // created_at
            ['created_at', 'integer'],

            // updated_at
            ['updated_at', 'integer'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'CATEGORY_CODE_LABEL_ID'),
            'category_id' => Yii::t('app', 'CATEGORY_CODE_LABEL_CATEGORY_ID'),
            'code_id' => Yii::t('app', 'CATEGORY_CODE_LABEL_CODE_ID'),
            'created_at' => Yii::t('app', 'CATEGORY_CODE_LABEL_CREATED_AT'),
            'updated_at' => Yii::t('app', 'CATEGORY_CODE_LABEL_UPDATED_AT'),
        ];
    }
    
    /**
     * Loads model
     * 
     * @return CategoryCode model 
     * @throws NotFoundHttpException
     */
    public static function loadModel($id)
    {
        $model = static::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException(Yii::t('app', 'MODEL_NOT_FOUND', 
                ['class' => basename(str_replace('\\', '/', static::class))])
            );
        }
        return $model;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(User::className(), ['id' => 'category_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodes()
    {
        return $this->hasMany(User::className(), ['id' => 'code_id']);
    }
}
