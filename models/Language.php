<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $name
 * @property string $language_id
 * @property integer $order
 * @property integer $created_at
 * @property integer $updated_at
 */
class Language extends ActiveRecord
{
     /** @const SCENARIO_CREATE Scenario type */
    const SCENARIO_CREATE = 'create';

     /** @const SCENARIO_EDIT Scenario type */
    const SCENARIO_UPDATE = 'update';

    /** @const DEFAULT_ID Default language id */
    const DEFAULT_ID = 'javascript';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_CREATE => [
                'name',
                'language_id',
                'order',
                'created_at',
                'updated_at',
            ],
            self::SCENARIO_UPDATE => [
                'id',
                'name',
                'language_id',
                'order',
                'created_at',
                'updated_at',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // id
            ['id', 'integer', 'min' => 0],
            ['id', 'unique', 'on' => static::SCENARIO_CREATE],
            ['id', 'required', 'on' => static::SCENARIO_UPDATE],

            // name
            ['name', 'required'],
            ['name', 'string', 'max' => 255],

            // language_id
            ['language_id', 'required'],
            ['language_id', 'string', 'max' => 255],

            // order
            ['order', 'required'],
            ['order', 'integer', 'min' => 1,'max' => 99],

            // created_at
            ['created_at', 'integer'],

            // updated_at
            ['updated_at', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'LANGUAGE_LABEL_ID'),
            'name' => Yii::t('app', 'LANGUAGE_LABEL_NAME'),
            'language_id' => Yii::t('app', 'LANGUAGE_LABEL_LANGUAGE_ID'),
            'order' => Yii::t('app', 'LANGUAGE_LABEL_ORDER'),
            'created_at' => Yii::t('app', 'LANGUAGE_LABEL_CREATED_AT'),
            'updated_at' => Yii::t('app', 'LANGUAGE_LABEL_UPDATED_AT'),
        ];
    }

    /**
     * Loads model
     *
     * @return Category model
     * @throws NotFoundHttpException
     */
    public static function loadModel($id)
    {
        $model = static::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException(Yii::t('app', 'MODEL_NOT_FOUND',
                ['class' => basename(str_replace('\\', '/', static::class))])
            );
        }
        return $model;
    }

    /**
     * Returns mapped language list
     *
     * @return array
     */
    public static function getMappedList()
    {
        $languages = static::find()
            ->select(['language_id', 'name'])
            ->orderBy(['order' => SORT_ASC])
            ->asArray()
            ->all();

        return ArrayHelper::map($languages, 'language_id', 'name');
    }
}
