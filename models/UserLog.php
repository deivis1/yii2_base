<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;
use app\models\User;
use app\models\UserLogAction;

/**
 * This is the model class for table "user_log".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property User $user
 * @property UserLogAction $userLogAction
 */
class UserLog extends ActiveRecord
{
     /** @const SCENARIO_CREATE Scenario type */
    const SCENARIO_CREATE = 'create';
    
     /** @const SCENARIO_EDIT Scenario type */
    const SCENARIO_UPDATE = 'update';
  
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_log';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_CREATE => [
                'id',
                'user_id',
                'ip',
                'action_id',
                'created_at',
                'updated_at',
            ],
            self::SCENARIO_UPDATE => [
                'id',
                'user_id',
                'ip',
                'action_id',
                'created_at',
                'updated_at',
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // id
            ['id', 'integer', 'min' => 0],
            ['id', 'unique', 'on' => static::SCENARIO_CREATE],
            ['id', 'required', 'on' => static::SCENARIO_UPDATE],
            
            // user_id
            ['user_id', 'integer', 'min' => 0],
            ['user_id', 'required'],
            
            // ip
            ['ip', 'filter', 'filter' => 'trim'],
            ['ip', 'required'],
            ['ip', 'string', 'min' => 7, 'max' => 15],
            
            // action_id
            ['action_id', 'integer', 'min' => 0],
            ['action_id', 'required'],
            ['action_id', 'exist', 
                'targetClass' => UserLogAction::className(), 
                'targetAttribute' => 'id',
                'on' => static::SCENARIO_UPDATE,
            ],

            // created_at
            ['created_at', 'integer'],

            // updated_at
            ['updated_at', 'integer'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'USER_LOG_LABEL_ID'),
            'user_id' => Yii::t('app', 'USER_LOG_LABEL_USER_ID'),
            'ip' => Yii::t('app', 'USER_LOG_LABEL_IP'),
            'action_id' => Yii::t('app', 'USER_LOG_LABEL_ACTION_ID'),
            'created_at' => Yii::t('app', 'USER_LOG_LABEL_CREATED_AT'),
            'updated_at' => Yii::t('app', 'USER_LOG_LABEL_UPDATED_AT'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogUserAction()
    {
        return $this->hasOne(LogUserAction::className(), ['id' => 'action_id']);
    }
    
    /**
     * Loads model
     * 
     * @return Item model 
     * @throws NotFoundHttpException
     */
    public static function loadModel($id)
    {
        $model = static::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException(Yii::t('app', 'MODEL_NOT_FOUND', 
                ['class' => basename(str_replace('\\', '/', static::class))])
            );
        }
        return $model;
    }
    
    /**
     * Creates user log
     * 
     * @param integer $actionId
     * @param integer|null $userId
     * @return UserLog Created model
     */
    public static function create($actionId, $userId = null)
    {
        $model = new static;
        $model->setScenario(static::SCENARIO_CREATE);
        
        if (is_null($userId)) {
            $userId = Yii::$app->user->isGuest ? 0 : Yii::$app->user->identity->id;
        }
        
        $model->user_id = $userId;
        $model->ip = Yii::$app->getRequest()->getUserIP();
        $model->action_id = $actionId;
        return $model->save() ? $model : null;
    }
}
