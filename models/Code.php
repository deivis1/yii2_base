<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\CategoryCode;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;
use app\models\User;
use app\models\Language;
use app\components\Util;
use yii\db\Expression;

/**
 * This is the model class for table "code".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $description
 * @property string $keywords
 * @property string $code
 * @property string $language_id
 * @property string $share_token
 * @property integer $created_at
 * @property integer $updated_at
 * @property User $user
 * @property Category[] $categories
 * @property Language $language
 */
class Code extends ActiveRecord
{
     /** @const SCENARIO_CREATE Scenario type */
    const SCENARIO_CREATE = 'create';
    
     /** @const SCENARIO_EDIT Scenario type */
    const SCENARIO_UPDATE = 'update';

    /** @const SCENARIO_SEARCH Scenario type */
    const SCENARIO_SEARCH = 'serach';
  
    public $category_list;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'code';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_CREATE => [
                'id',
                'user_id',
                'description',
                'keywords',
                'code',
                'language_id',
                'share_token',
                'created_at',
                'updated_at',
            ],
            self::SCENARIO_UPDATE => [
                'id',
                'user_id',
                'description',
                'keywords',
                'code',
                'language_id',
                'share_token',
                'created_at',
                'updated_at',
            ],
            self::SCENARIO_SEARCH => [
                'description',
                'category_list',
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // id
            ['id', 'integer', 'min' => 0],
            ['id', 'unique', 'on' => static::SCENARIO_CREATE],
            ['id', 'validateNewId', 'on' => static::SCENARIO_CREATE],
            ['id', 'required', 'on' => static::SCENARIO_UPDATE],
            ['id', 'exist', 
                'targetClass' => static::className(), 
                'targetAttribute' => 'id',
                'on' => static::SCENARIO_UPDATE,
            ],
            
            // user_id
            ['user_id', 'default', 
                'value' => Util::currentUserId(), 
                'on' => static::SCENARIO_CREATE
            ],
            ['user_id', 'required'],
            ['user_id', 'compare', 
                'compareValue' => Util::currentUserId(0), 
                'operator' => '==',
                'on' => static::SCENARIO_UPDATE,
                'message' => Yii::t('app', 'MODEL_INVALID_USER_ID'),
            ],
            ['user_id', 'integer', 'min' => 0],
            ['user_id', 'exist', 
                'targetClass' => User::className(), 
                'targetAttribute' => 'id',
                'on' => static::SCENARIO_UPDATE,
            ],
            
            // description
            ['description', 'filter', 'filter' => 'trim'],
            ['description', 'required'],
            ['description', 'string', 'max' => 255],
            
            // keywords
            ['keywords', 'filter', 'filter' => 'trim'],
            ['keywords', 'string', 'max' => 255],
            
            // code
            ['code', 'required'],
            ['code', 'filter', 'filter' => 'trim'],
            ['code', 'string', 'max' => 2000],
            
            // language_id
            ['language_id', 'required'],
            ['language_id', 'string', 'max' => 20],
            ['language_id', 'exist', 
                'targetClass' => Language::className(), 
                'targetAttribute' => 'language_id',
            ],
            
            // share_token
            ['share_token', 'string', 'max' => 20],
            ['share_token', 'unique'],

            // created_at
            ['created_at', 'integer'],

            // updated_at
            ['updated_at', 'integer'],
            
            // category_list
            ['category_list', 'string', 'max' => 255, 
                'on' => static::SCENARIO_SEARCH
            ],
        ];
    }
    
    /**
     * Validates new record id
     *
     * @param string $attribute the attribute currently being validated
     * @param mixed $params the value of the "params" given in the rule
     */
    public function validateNewId($attribute, $params)
    {
        $id = $this->$attribute;
        $model = self::findOne($id);
        
        if ($id != 0 || $model !== null) {
            $this->addError($attribute, Yii::t('app', 'MODEL_INVALID_ID'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'CODE_LABEL_ID'),
            'user_id' => Yii::t('app', 'CODE_LABEL_USER_ID'),
            'description' => Yii::t('app', 'CODE_LABEL_DESCRIPTION'),
            'keywords' => Yii::t('app', 'CODE_LABEL_KEYWORDS'),
            'code' => Yii::t('app', 'CODE_LABEL_CODE'),
            'language_id' => Yii::t('app', 'CODE_LABEL_LANGUAGE_ID'),
            'share_token' => Yii::t('app', 'CODE_LABEL_SHARE_TOKEN'),
            'created_at' => Yii::t('app', 'CODE_LABEL_CREATED_AT'),
            'updated_at' => Yii::t('app', 'CODE_LABEL_UPDATED_AT'),
        ];
    }
    
    /**
     * Loads model
     * 
     * @return Code model 
     * @throws NotFoundHttpException
     */
    public static function loadModel($id)
    {
        $model = static::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException(Yii::t('app', 'MODEL_NOT_FOUND', 
                ['class' => basename(str_replace('\\', '/', static::class))])
            );
        }
        return $model;
    }
    
    /**
     * Finds code model by specified share token
     * 
     * @param string $token
     * @param boolean $raiseError
     * @throws NotFoundHttpException
     * @return Code|null
     */
    public static function findByShareToken($token, $raiseError = true)
    {
        if (empty($token)) {
            throw new NotFoundHttpException(Yii::t('app', 'INVALID_CODE_SHARE_LINK'));
        }
        
        $model = static::find()->where(['share_token' => $token])->one();
        
        if (is_null($model) && $raiseError) {
            throw new NotFoundHttpException(Yii::t('app', 'INVALID_CODE_SHARE_LINK'));
        }
        
        return $model;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(
            Category::className(), ['id' => 'category_id']
        )->viaTable(CategoryCode::tableName(), ['code_id' => 'id'])
        ->orderBy(['order' => SORT_ASC]);
    }

    
    /**
     * Return code category ids
     * 
     * @return array
     */
    public function getCategoryIds()
    {
        $ids = $this->getCategories()->select('id')->column();
        $result = [];
        foreach ($ids as $id) {
            $i = intval($id);
            if ($i != "") {
                array_push($result, $i);
            }
        }
        return $result;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
    
    /**
     * Returns array of models by search string
     * 
     * @return array
     */
    public function search()
    {
        $query = self::find()->with('categories');
        $searchString = $this->description;
        $categoryIds = explode(',', $this->category_list);
        $words = explode(' ', $searchString);
        
        foreach($words as $word) {
            $query->andWhere([
                'or',
                ['like', 'description', $word],
                ['like', 'keywords', $word],
                ['like', 'code', $word],
            ]);
        }
        
//        if (!empty($categoryIds)) {
//            $query->innerJoin('category_code', 'category_code.code_id = code.id')
//                ->andWhere(['category_code.category_id' => $categoryIds]);
//        }
        
        $query->andWhere(['user_id' => Util::currentUserId()]);
        
        $list = $query->asArray()->all();
        return $this->setListCategories($list);
    }
    
    /**
     * Check if created or updated model is not filtered out by search string
     * 
     * and selected filter categories
     * 
     * @param string $search
     * @param string $categories
     * @return boolean
     */
    public function getSearchResults($search, $categories)
    {
        $query = self::find()->with('categories');
        $categoryIds = explode(',', $categories);
        $words = explode(' ', $search);
        
        foreach($words as $word) {
            $query->andWhere([
                'or',
                ['like', 'description', $word],
                ['like', 'keywords', $word],
                ['like', 'code', $word],
            ]);
        }
        
        if (!empty($categoryIds)) {
            $query->innerJoin('category_code', 'category_code.code_id = code.id')
                ->andWhere(['category_code.category_id' => $categoryIds]);
        }
        
        $query->andWhere(['user_id' => Util::currentUserId()]);
        
        $list = $query->asArray()->all();
        return $this->setListCategories($list);
    }
    
    /**
     * Check if created or updated model is not filtered out by search string
     * 
     * @param string $search
     * @param string $categories
     * @return boolean
     */
    public function checkFiltered($search, $categories)
    {
        if (empty($search) && empty($categories)) {
            return true;
        }
        
        $models = $this->getSearchResults($search, $categories);
        
        foreach($models as $model) {
            if ($this->id == $model['id']) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Verifies if logged user owns this code
     * 
     * @return boolean
     */
    public function checkOwner()
    {
        return $this->user_id === Util::currentUserId();
    }
    
    /**
     * Verifies if code has share link
     * 
     * @return boolean
     */
    public function isShared()
    {
        return !is_null($this->share_token);
    }
    
    /**
     * Returns new share token string
     * 
     * @return string
     */
    public function getShareTokenString()
    {
        $token = Yii::$app->security->generateRandomString();
        $symbols = ['-', '_'];
        
        foreach($symbols as $symbol) {
            $token = str_replace($symbol, '', $token);
        }
        
        $token = substr($token, 0, 16);
        if (static::findByShareToken($token, false) !== null) {
            $token = $this->getShareTokenString();
        }
        return $token;
    }
    
    /**
     * Returns array containing share link string and boolean whether code is currently shared
     * 
     * @return array
     */
    public function getShareToken()
    {
        return $this->isShared() ? $this->share_token : $this->getShareTokenString();
    }
    
    /**
     * Saves code categories
     * 
     * @param array $ids Category ids
     */
    public function saveCategories($ids)
    {
        CategoryCode::deleteAll(['code_id' => $this->id]);
        
        if (empty($ids)) {
            return;
        }
        
        $now = new Expression('UNIX_TIMESTAMP(NOW())');
        
        $rows = [];
        foreach ($ids as $categoryId) {
            $rows[] = [$categoryId, $this->id, $now, $now];
        }
        
        Yii::$app->db->createCommand()
          ->batchInsert('category_code', [
              'category_id', 'code_id', 'created_at', 'updated_at',
          ], $rows)->execute();
    }
    
    /**
     * Sets code list categories
     * 
     * @param array
     * @return array With reformatted categories field containing only ids
     */
    public function setListCategories($list)
    {
        foreach ($list as $key => $code) {
            $categories = [];
            foreach ($code['categories'] as $category) {
                array_push($categories, intval($category['id']));
            }
            $list[$key]['categories'] = $categories;
        }
        return $list;
    }
    
    /**
     * Deletes model and associated categories
     * 
     * @return boolean
     */
    public function deleteModel()
    {
        CategoryCode::deleteAll(['code_id' => $this->id]);
        return $this->delete();
    }
    
    /**
     * Returns demo code list
     * 
     * @return array
     */
    public function getDemoList()
    {
        $demoUserId = 7;
        
        $query = self::find()->with('categories')
            ->where(['user_id' => $demoUserId]);
        
        $list = $query->asArray()->all();
        return $this->setListCategories($list);
    }
    
    /**
     * Returns code demo model
     * 
     * @return Code
     */
    public static function getDemoModel()
    {
        $model = new static;
        $model->description = "foreach loop with break";
        $model->keywords = "php";
        $model->updated_at = 1561127475;
        return $model;
    }
}
