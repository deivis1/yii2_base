<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\components\Util;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $message;
    public $verifyCode;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email
            ['email', 'default', 
                'value' => Yii::$app->user->isGuest ? null : Yii::$app->user->identity->email,
                'when' => function ($model) {
                    return !Yii::$app->user->isGuest;
                }
            ],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            
            // name
            ['name', 'default', 
                'value' => Yii::$app->user->isGuest ? null : Yii::$app->user->identity->name,
                'when' => function ($model) {
                    return !Yii::$app->user->isGuest;
                }
            ],
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'max' => 255],
            
            // subject
            ['subject', 'filter', 'filter' => 'trim'],
            ['subject', 'required'],
            ['subject', 'string', 'max' => 255],
            
            // message
            ['message', 'filter', 'filter' => 'trim'],
            ['message', 'required'],
            ['message', 'string', 'max' => 255],
            
            // verifyCode
            ['verifyCode', 'captcha', 'when' => function ($model) {
                return Yii::$app->user->isGuest;
            }],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'CONTACT_FORM_LABEL_EMAIL'),
            'name' => Yii::t('app', 'CONTACT_FORM_LABEL_NAME'),
            'subject' => Yii::t('app', 'CONTACT_FORM_LABEL_SUBJECT'),
            'message' => Yii::t('app', 'CONTACT_FORM_LABEL_MESSAGE'),
            'verifyCode' => Yii::t('app', 'CONTACT_FORM_LABEL_VERIFY_CODE'),
        ];
    }
    
    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            $subjectPrefix = Yii::$app->user->isGuest ?
                Yii::t('app', 'CONTACT_SUBJECT_GUEST_VISITOR_PREFIX') :
                Yii::t('app', 'CONTACT_SUBJECT_LOGGED_IN_VISITOR_PREFIX');
            
            Util::sendMail([
                'email' => $email,
                'name' => '',
                'replyEmail' => $this->email,
                'replyName' => $this->name,
                'subject' => $subjectPrefix . $this->subject,
                'message' => $this->message,
            ]);
            
//            Yii::$app->mailer->compose()
//                ->setTo($email)
//                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
//                ->setReplyTo([$this->email => $this->name])
//                ->setSubject($subjectPrefix . $this->subject)
//                ->setTextBody($this->message)
//                ->send();

            return true;
        }
        return false;
    }
}
