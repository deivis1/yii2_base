<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;
use app\models\UserLog;

/**
 * This is the model class for table "user_log_action".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 * @property UserLog[] $userLogs
 */
class UserLogAction extends ActiveRecord
{
     /** @const SCENARIO_CREATE Scenario type */
    const SCENARIO_CREATE = 'create';
    
     /** @const SCENARIO_EDIT Scenario type */
    const SCENARIO_UPDATE = 'update';

    /** @const User log action ids */
    const ACTION_LOGIN = 1;
    const ACTION_LOGIN_FAIL_EMAIL = 2;
    const ACTION_LOGIN_FAIL_PASSWORD = 3;
    const ACTION_LOGOUT = 4;
    const ACTION_UPDATE_ACCOUNT = 5;
    const ACTION_CHANGE_LANGUAGE = 6;
    const ACTION_PASSWORD_RESET_REQUEST = 7;
    const ACTION_PASSWORD_RESET = 8;
    const ACTION_PASSWORD_CHANGE = 9;
    const ACTION_SIGNUP = 10;
    const ACTION_SIGNUP_VALIDATE_EMAIL = 11;
    const ACTION_CODE_CREATE = 12;
    const ACTION_CODE_UPDATE = 13;
    const ACTION_CODE_DELETE = 14;
    const ACTION_CODE_SHARE = 15;
    const ACTION_CODE_VIEW = 16;
    const ACTION_CODE_VIEW_SHARED = 17;
    const ACTION_VIEW_INDEX = 18;
    const ACTION_CATEGORY_CREATE = 19;
    const ACTION_CATEGORY_UPDATE = 20;
    const ACTION_CATEGORY_DELETE = 21;
    
    /** @const User log action names */
    const ACTION_NAME_LOGIN = 'login';
    const ACTION_NAME_LOGIN_FAIL_EMAIL = 'login fail email';
    const ACTION_NAME_LOGIN_FAIL_PASSWORD = 'login fail password';
    const ACTION_NAME_LOGOUT = 'logout';
    const ACTION_NAME_UPDATE_ACCOUNT = 'update account';
    const ACTION_NAME_CHANGE_LANGUAGE = 'change language';
    const ACTION_NAME_PASSWORD_RESET_REQUEST = 'password reset request';
    const ACTION_NAME_PASSWORD_RESET = 'password reset';
    const ACTION_NAME_PASSWORD_CHANGE = 'password change';
    const ACTION_NAME_SIGNUP = 'signup';
    const ACTION_NAME_SIGNUP_VALIDATE_EMAIL = 'signup validate email';
    const ACTION_NAME_CODE_CREATE = 'code create';
    const ACTION_NAME_CODE_UPDATE = 'code update';
    const ACTION_NAME_CODE_DELETE = 'code delete';
    const ACTION_NAME_CODE_SHARE = 'code share';
    const ACTION_NAME_CODE_VIEW = 'code view';
    const ACTION_NAME_CODE_VIEW_SHARED = 'code view shared';
    const ACTION_NAME_VIEW_INDEX = 'view index';
    const ACTION_NAME_CATEGORY_CREATE = 'category create';
    const ACTION_NAME_CATEGORY_UPDATE = 'category update';
    const ACTION_NAME_CATEGORY_DELETE = 'category delete';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_log';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_CREATE => [
                'id',
                'name',
                'created_at',
                'updated_at',
            ],
            self::SCENARIO_UPDATE => [
                'id',
                'name',
                'created_at',
                'updated_at',
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // id
            ['id', 'integer', 'min' => 0],
            ['id', 'unique', 'on' => static::SCENARIO_CREATE],
            ['id', 'validateNewId', 'on' => static::SCENARIO_CREATE],
            ['id', 'required', 'on' => static::SCENARIO_UPDATE],
            
            // name
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 3, 'max' => 30],

            // created_at
            ['created_at', 'integer'],

            // updated_at
            ['updated_at', 'integer'],
        ];
    }
    
    /**
     * Validates new record id
     *
     * @param string $attribute the attribute currently being validated
     * @param mixed $params the value of the "params" given in the rule
     */
    public function validateNewId($attribute, $params)
    {
        $id = $this->$attribute;
        $model = self::findOne($id);
        
        if ($id != 0 || $model !== null) {
            $this->addError($attribute, Yii::t('app', 'MODEL_INVALID_ID'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'USER_LOG_ACTION_LABEL_ID'),
            'name' => Yii::t('app', 'USER_LOG_ACTION_LABEL_NAME'),
            'created_at' => Yii::t('app', 'USER_LOG_ACTION_LABEL_CREATED_AT'),
            'updated_at' => Yii::t('app', 'USER_LOG_ACTION_LABEL_UPDATED_AT'),
        ];
    }
    
    /**
     * Loads model
     * 
     * @return Item model 
     * @throws NotFoundHttpException
     */
    public static function loadModel($id)
    {
        $model = static::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException(Yii::t('app', 'MODEL_NOT_FOUND', 
                ['class' => basename(str_replace('\\', '/', static::class))])
            );
        }
        return $model;
    }
    
    /**
     * @return ActiveQuery
     */
    public function getUserLogs()
    {
        return $this->hasMany(UserLog::className(), ['action_id' => 'id']);
    }
    
    /**
     * Returns mapped log action list
     * 
     * @return array
     */
    public static function getMappedList()
    {
        return [
            static::ACTION_LOGIN => static::ACTION_NAME_LOGIN,
            static::ACTION_LOGIN_FAIL_EMAIL => static::ACTION_NAME_LOGIN_FAIL_EMAIL,
            static::ACTION_LOGIN_FAIL_PASSWORD => static::ACTION_NAME_LOGIN_FAIL_PASSWORD,
            static::ACTION_LOGOUT => static::ACTION_NAME_LOGOUT,
            static::ACTION_UPDATE_ACCOUNT => static::ACTION_NAME_UPDATE_ACCOUNT,
            static::ACTION_CHANGE_LANGUAGE => static::ACTION_NAME_CHANGE_LANGUAGE,
            static::ACTION_PASSWORD_RESET_REQUEST => static::ACTION_NAME_PASSWORD_RESET_REQUEST,
            static::ACTION_PASSWORD_RESET => static::ACTION_NAME_PASSWORD_RESET,
            static::ACTION_PASSWORD_CHANGE => static::ACTION_NAME_PASSWORD_CHANGE,
            static::ACTION_SIGNUP => static::ACTION_NAME_SIGNUP,
            static::ACTION_SIGNUP_VALIDATE_EMAIL => static::ACTION_NAME_SIGNUP_VALIDATE_EMAIL,
            static::ACTION_CODE_CREATE => static::ACTION_NAME_CODE_CREATE,
            static::ACTION_CODE_UPDATE => static::ACTION_NAME_CODE_UPDATE,
            static::ACTION_CODE_DELETE => static::ACTION_NAME_CODE_DELETE,
            static::ACTION_CODE_SHARE => static::ACTION_NAME_CODE_SHARE,
            static::ACTION_CODE_VIEW => static::ACTION_NAME_CODE_VIEW,
            static::ACTION_CODE_VIEW_SHARED => static::ACTION_NAME_CODE_VIEW_SHARED,
            static::ACTION_VIEW_INDEX => static::ACTION_NAME_VIEW_INDEX,
            static::ACTION_CATEGORY_CREATE => static::ACTION_NAME_CATEGORY_CREATE,
            static::ACTION_CATEGORY_UPDATE => static::ACTION_NAME_CATEGORY_UPDATE,
            static::ACTION_CATEGORY_DELETE => static::ACTION_NAME_CATEGORY_DELETE,
        ];
    }
}
