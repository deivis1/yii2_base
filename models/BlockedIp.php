<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "blocked_ip".
 *
 * @property integer $id
 * @property string $ip
 * @property integer $level
 * @property integer $created_at
 * @property integer $updated_at
 */
class BlockedIp extends ActiveRecord
{
     /** @const SCENARIO_CREATE Scenario type */
    const SCENARIO_CREATE = 'create';
    
     /** @const SCENARIO_EDIT Scenario type */
    const SCENARIO_UPDATE = 'update';
    
    /** @const LEVEL_LOCKED Ip level */
    const LEVEL_LOCKED = 3;
  
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blocked_ip';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_CREATE => [
                'ip',
                'level',
                'created_at',
                'updated_at',
            ],
            self::SCENARIO_UPDATE => [
                'id',
                'ip',
                'level',
                'created_at',
                'updated_at',
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // id
            ['id', 'integer', 'min' => 0],
            ['id', 'unique', 'on' => static::SCENARIO_CREATE],
            ['id', 'required', 'on' => static::SCENARIO_UPDATE],
            ['id', 'exist', 
                'targetClass' => static::className(), 
                'targetAttribute' => 'id',
                'on' => static::SCENARIO_UPDATE,
            ],
            
            // ip
            ['ip', 'required'],
            ['ip', 'string', 'min' => 7, 'max' => 15],
            
            // level
            ['level', 'required'],
            ['level', 'integer', 'min' => 0, 'max' => static::LEVEL_LOCKED],
            
            // created_at
            ['created_at', 'integer'],

            // updated_at
            ['updated_at', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'BLOCKED_IP_LABEL_ID'),
            'ip' => Yii::t('app', 'BLOCKED_IP_LABEL_USER_IP'),
            'level' => Yii::t('app', 'BLOCKED_IP_LABEL_LEVEL'),
            'created_at' => Yii::t('app', 'BLOCKED_IP_LABEL_CREATED_AT'),
            'updated_at' => Yii::t('app', 'BLOCKED_IP_LABEL_UPDATED_AT'),
        ];
    }
    
    /**
     * Loads model
     * 
     * @return Item model 
     * @throws NotFoundHttpException
     */
    public static function loadModel($id)
    {
        $model = static::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException(Yii::t('app', 'MODEL_NOT_FOUND', 
                ['class' => basename(str_replace('\\', '/', static::class))])
            );
        }
        return $model;
    }
    
    /**
     * Checks if user's ip in the table
     * 
     * @return boolean
     */
    public static function check()
    {
        $ip = Yii::$app->getRequest()->getUserIP();
        $model = static::find()->where([
            'ip' => $ip,
            'level' => static::LEVEL_LOCKED,
        ])->one();
        return !is_null($model);
    }
    
    /**
     * Checks if model is locked
     * 
     * @return boolean
     */
    public function isLocked()
    {
        return $this->level === static::LEVEL_LOCKED;
    }
    
    /**
     * Logs user's ip in blocked ip addresses table
     * 
     * @param integer|null $ip
     * @return boolean
     */
    public static function log($ip = null)
    {
        if ($ip === null) {
            $ip = Yii::$app->getRequest()->getUserIP();
        }
        $model = static::find()->where(['ip' => $ip])->one();
        
        if (is_null($model)) {
            $model = new static(['scenario' => static::SCENARIO_CREATE]);
            $model->ip = $ip;
            $model->level = 0;
        } elseif ($model->isLocked()) {
            return;
        } else {
            $model->setScenario(static::SCENARIO_UPDATE);
            $model->level++;
        }
        
        $model->save();
    }
    
    /**
     * Removes user's ip from blocked ips table
     * 
     * @param integer|null $ip
     * @return integer Rows deleted
     */
    public static function reset($ip = null)
    {
        if ($ip === null) {
            $ip = Yii::$app->getRequest()->getUserIP();
        }
        return static::deleteAll(['ip' => $ip]);
    }
}
