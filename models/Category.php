<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;
use app\models\User;
use yii\helpers\ArrayHelper;
use app\components\Util;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property integer $order
 * @property integer $created_at
 * @property integer $updated_at
 * @property User $user
 * @property Code $codes
 */
class Category extends ActiveRecord
{
     /** @const SCENARIO_CREATE Scenario type */
    const SCENARIO_CREATE = 'create';
    
     /** @const SCENARIO_EDIT Scenario type */
    const SCENARIO_UPDATE = 'update';
  
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_CREATE => [
                'user_id',
                'name',
                'order',
                'created_at',
                'updated_at',
            ],
            self::SCENARIO_UPDATE => [
                'id',
                'user_id',
                'name',
                'order',
                'created_at',
                'updated_at',
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // id
            ['id', 'integer', 'min' => 0],
            ['id', 'unique', 'on' => static::SCENARIO_CREATE],
            ['id', 'required', 'on' => static::SCENARIO_UPDATE],
            
            // user_id
            ['user_id', 'default', 
                'value' => Util::currentUserId(), 
                'on' => static::SCENARIO_CREATE
            ],
            ['user_id', 'required'],
            ['user_id', 'compare', 
                'compareValue' => Util::currentUserId(0), 
                'operator' => '==',
                'on' => static::SCENARIO_UPDATE,
                'message' => Yii::t('app', 'MODEL_INVALID_USER_ID'),
            ],
            ['user_id', 'integer', 'min' => 0],
            ['user_id', 'exist', 
                'targetClass' => User::className(), 
                'targetAttribute' => 'id',
                'on' => static::SCENARIO_UPDATE,
            ],
            
            // name
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'uniqueForUser', 'params' => ['id' => Util::currentUserId()]],
            ['name', 'string', 'max' => 255],
            
            // keywords
            ['keywords', 'filter', 'filter' => 'trim'],
            ['keywords', 'string', 'max' => 255],
            
            // order
            ['order', 'required'],
            ['order', 'integer', 'min' => 1,'max' => 99],

            // created_at
            ['created_at', 'integer'],

            // updated_at
            ['updated_at', 'integer'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function uniqueForUser($attribute, $params)
    {
        $categories = self::find()
            ->select(['id', 'name'])
            ->where(['user_id' => $params['id']])
            ->orderBy(['order' => SORT_ASC])
            ->asArray()
            ->all();
        
        foreach($categories as $category) {
            if ($this->{$attribute} == $category['name'] &&
                $this->id !== $category['id']
            ) {
                $this->addError($attribute, 
                    Yii::t('app', 'CATEGORY_NAME_EXISTS'));
                return;
            }
        }
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'CATEGORY_LABEL_ID'),
            'user_id' => Yii::t('app', 'CATEGORY_LABEL_USER_ID'),
            'name' => Yii::t('app', 'CATEGORY_LABEL_NAME'),
            'order' => Yii::t('app', 'CATEGORY_LABEL_ORDER'),
            'created_at' => Yii::t('app', 'CATEGORY_LABEL_CREATED_AT'),
            'updated_at' => Yii::t('app', 'CATEGORY_LABEL_UPDATED_AT'),
        ];
    }
    
    /**
     * Loads model
     * 
     * @return Category model 
     * @throws NotFoundHttpException
     */
    public static function loadModel($id)
    {
        $model = static::findOne($id);
        if (is_null($model)) {
            throw new NotFoundHttpException(Yii::t('app', 'MODEL_NOT_FOUND', 
                ['class' => basename(str_replace('\\', '/', static::class))])
            );
        }
        return $model;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
   /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodes()
    {
        return $this->hasMany(
            Code::className(), ['id' => 'code_id']
        )->viaTable(CategoryCode::tableName(), ['category_id' => 'id']);
    }
    
    /**
     * Verifies if logged user owns this code
     * 
     * @return boolean
     */
    public function checkOwner()
    {
        return $this->user_id === Util::currentUserId();
    }
    
    /**
     * Returns demo category list
     * 
     * @return array
     */
    public static function getDemoList()
    {
        $categories = self::find()
            ->select(['id', 'name'])
            ->where(['user_id' => 1])
            ->orderBy(['order' => SORT_ASC])
            ->asArray()
            ->all();
        
        return ArrayHelper::map($categories, 'id', 'name');
    }
    
    /**
     * Change categories order values if there are duplicates
     */
    public static function reorder()
    {
        $categories = self::find()
            ->select(['id', 'order'])
            ->where(['user_id' => Util::currentUserId()])
            ->orderBy(['order' => SORT_ASC])
            ->asArray()
            ->all();
        
        if (count($categories) < 2) {
            return;
        }
        
        $gaps = false;
        for ($i = 0; $i < count($categories) - 1; $i++) {
            if ($categories[$i + 1]['order'] - $categories[$i]['order'] > 1) {
                $gaps = true;
                break;
            }
        }
        
        $list = ArrayHelper::map($categories, 'id', 'order');
        
        if (!$gaps && count($list) === count(array_unique($list))) {
            return;
        }
        
        $used = [];
        $order = 1;
        
        for ($i = 0; $i < count($categories); $i++) {        
            while (in_array($order, $used)) {
                $order++;
            }
        
            array_push($used, $order);
            
            $model = self::loadModel($categories[$i]['id']);
            $model->setScenario(self::SCENARIO_UPDATE);
            $model->order = $order;
            $model->save();
        }
    }
    
    /**
     * Change categories order values to insert new not duplicated order value
     */
    public function reorderForNew()
    {
        $categories = self::find()
            ->where(['user_id' => Util::currentUserId()])
            ->orderBy(['order' => SORT_ASC])
            ->asArray()
            ->all();
        
        if (count($categories) === 0) {
            return;
        }
        
        $i = 0;
        while ($i < count($categories) && 
            $categories[$i]['order'] !== $this->order
        ) {
            $i++;
        }
        
        if ($i === count($categories)) {
            $i = 0;
        }
        
        while ($i < count($categories)) {
            $model = self::loadModel($categories[$i]['id']);
            $model->setScenario(self::SCENARIO_UPDATE);
            $model->order = $model->order + 1;
            $model->save();
            $i++;
        }
    }
    
    /**
     * Removes category from codes and then deletes it
     * 
     * @return boolean if model was deleted
     */
    public function deleteModel()
    {
        CategoryCode::deleteAll(['category_id' => $this->id]);
        return $this->delete();
    }
    
    /**
     * Returns user category array with isAssigned field
     * 
     * @return array
     */
    public static function getUserCategories()
    {
        $data = Category::find()
            ->where(['user_id' => Yii::$app->user->identity->id])
            ->orderBy(['order' => SORT_ASC])
            ->asArray()->all();
       
        foreach($data as $key => $item) {
            $category = Category::loadModel($item['id']);
            $data[$key]['isAssigned'] = count($category->codes) !== 0;
        }
        
        return $data;
    }
}
