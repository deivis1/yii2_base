<?php

use yii\db\Migration;
use yii\db\Expression;
use app\models\UserLogAction;

/**
 * Handles the creation of table `user_log_action`.
 */
class m190602_114225_create_log_action_table extends Migration
{
    private $tableName = 'user_log_action';
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        
        $now = new Expression('UNIX_TIMESTAMP(NOW())');
        $names = array_values(UserLogAction::getMappedList());
        
        $values = [];
        foreach($names as $name) {
            $values[] = [$name, $now, $now];
        }
        
        $this->batchInsert($this->tableName, 
            ['name', 'created_at', 'updated_at'],
            $values
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
