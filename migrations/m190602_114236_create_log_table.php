<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_log`.
 */
class m190602_114236_create_log_table extends Migration
{
    private $tableName = 'user_log';
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'ip' => $this->string(15)->notNull(),
            'action_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        
        $this->addForeignKey(
            'fk-user_log-action_id',
            $this->tableName,
            'action_id',
            'user_log_action',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-user_log-action_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
