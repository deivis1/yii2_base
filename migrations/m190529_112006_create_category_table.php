<?php

use yii\db\Migration;
use yii\db\Expression;
use app\models\User;

/**
 * Handles the creation of table `{{%category}}`.
 */
class m190529_112006_create_category_table extends Migration
{
    private $tableName = '{{%category}}';
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
      
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'order' => $this->smallInteger(6)->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        
        $this->addForeignKey(
            'fk-category-user_id',
            $this->tableName,
            'user_id',
            'user',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
        
        $now = new Expression('UNIX_TIMESTAMP(NOW())');
        $id = User::find()->where(['role' => User::ROLE_ADMIN])->one()->id;
        
        $this->batchInsert($this->tableName, [
            'name', 'user_id', 'order', 'created_at', 'updated_at',
        ], [
            ['php', $id, 1, $now, $now],
            ['yii2', $id, 2, $now, $now],
            ['laravel', $id, 3, $now, $now],
            ['vuejs', $id, 4, $now, $now],
            ['javascript', $id, 5, $now, $now],
            ['JQuery', $id, 6, $now, $now],
            ['bash', $id, 7, $now, $now],
            ['mysql', $id, 8, $now, $now],
            ['c++', $id, 9, $now, $now],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
