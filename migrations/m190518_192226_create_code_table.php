<?php

use yii\db\Migration;
use app\models\Language;

/**
 * Handles the creation of table `code`.
 */
class m190518_192226_create_code_table extends Migration
{
    private $tableName = 'code';
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'description' => $this->string(255)->null(),
            'keywords' => $this->string(255)->null(),
            'language_id' => $this->string()->notNull()->defaultValue(Language::DEFAULT_ID),
            'code' => $this->text()->null(),
            'share_token' => $this->string()->unique()->null(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        
        $this->addForeignKey(
            'fk-code-user_id',
            $this->tableName,
            'user_id',
            'user',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
        
        $this->addForeignKey(
            'fk-code-language_id',
            $this->tableName,
            'language_id',
            'language',
            'language_id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-code-language_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
