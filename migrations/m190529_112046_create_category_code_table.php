<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category_code`.
 */
class m190529_112046_create_category_code_table extends Migration
{
    private $tableName = 'category_code';
  
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
      
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'code_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        
        $this->addForeignKey(
            'fk-category_code-category_id',
            $this->tableName,
            'category_id',
            'category',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
        
        $this->addForeignKey(
            'fk-category_code-code_id',
            $this->tableName,
            'code_id',
            'code',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-category_code-category_id', $this->tableName);
        $this->dropForeignKey('fk-category_code-code_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
