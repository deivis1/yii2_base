<?php

use yii\db\Migration;
use app\models\User;
use yii\db\Expression;

/**
 * Handles the creation of table `user`.
 */
class m190517_173647_create_user_table extends Migration
{
    private $tableName = 'user';
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'email' => $this->string()->unique()->notNull(),
            'name' => $this->string()->notNull(),
            'auth_key' => $this->string()->notNull(),
            'password_hash' => $this->string()->notNull(),
            'email_verification_token' => $this->string()->null(),
            'password_reset_token' => $this->string()->null(),
            'role' => $this->smallInteger(6)->notNull()->defaultValue(User::ROLE_USER),
            'locked' => $this->smallInteger(6)->notNull()->defaultValue(User::LOCKED_NO),
            'language' => $this->integer()->notNull()->defaultValue(User::LANGUAGE_EN),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        
        $now = new Expression('UNIX_TIMESTAMP(NOW())');
        
        $this->insert($this->tableName, [
            'id' => 1,
            'email' => 'deividas.deka@gmail.com',
            'name' => 'Deividas',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('mypass5'),
            'email_verification_token' => null,
            'password_reset_token' => null,
            'role' => User::ROLE_ADMIN,
            'locked' => User::LOCKED_NO,
            'language' => User::LANGUAGE_EN,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
