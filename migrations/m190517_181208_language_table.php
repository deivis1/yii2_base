<?php

use yii\db\Migration;
use yii\db\Expression;

/**
 * Handles the creation of table `language`.
 */
class m190517_181208_language_table extends Migration
{
    private $tableName = 'language';
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
      
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'language_id' => $this->string()->unique()->notNull(),
            'order' => $this->smallInteger(6)->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        
        $now = new Expression('UNIX_TIMESTAMP(NOW())');
        
        $this->batchInsert($this->tableName, [
            'name', 'language_id', 'order', 'created_at', 'updated_at',
        ], [
            ['PHP', 'php', 1, $now, $now],
            ['Javascript', 'javascript', 2, $now, $now],
            ['C++', 'cpp', 3, $now, $now],
            ['bash', 'bash', 4, $now, $now],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
