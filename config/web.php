<?php

use app\components\Util;
use app\models\BlockedIp;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'My Code Base',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'en',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'app' => 'app.php'
                    ],
                ],
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'frontend' => 'frontend.php'
                    ],
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['user/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.google.com',
                'username' => 'sender@gmail.com',
                'password' => 'pass',
                'port' => '587',
                'encryption' => 'ssl',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'app\components\EmailTarget',
                    'levels' => ['error', 'warning'],
                    'message' => [
                        'from' => ['admin@mycodebase.io' => 'MyCodeBase Error'],
                        'to' => ['admin@mycodebase.io'],
                        'subject' => 'Error log',
                    ],
               ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,
            'rules' => [
                'user/password-reset/<token:\w+>' => 'user/password-reset',
                'user/password-reset-request/<email:[a-zA-z0-9\_\-\=]+>' => 'user/password-reset-request',
                'code/remove-code/<id:\d+>' => 'code/remove-code',
                'code/shared/<token:[a-zA-z0-9]+>' => 'code/shared',
                'code/shared-demo/<token:[a-zA-z0-9]+>' => 'code/shared-demo',
                'code/get-share-link/<id:\d+>' => 'code/get-share-link',
                'code/stop-sharing/<id:\d+>' => 'code/stop-sharing',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<lang:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'js' => [ '/js/jquery.js' ],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => null,
                    'css' => [ '/css/bootstrap.min.css' ],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => null,
                    'js' => [ '/js/bootstrap.min.js' ],
                ],
            ],
        ],
    ],
    'params' => $params,
    'on beforeAction' => function ($event) {
        if (BlockedIp::check()) {
            exit;
        }
        
        if (Yii::$app->getErrorHandler()->exception === null) {
            BlockedIp::reset();
        }
        
        Util::setLanguage();
        $routes = [
            'site/index',
            'site/contact',
            'user/email-verification-form',
            'user/send-verification-email',
            'user/verify-email',
            'user/logout',
        ];
        if (Util::redirectVerifyEmail() && !in_array(Yii::$app->request->pathInfo, $routes)) {
            Yii::$app->response->redirect(['/user/email-verification-form'])->send();
            $event->handled = true;
        }
    }
];

if (YII_ENV_DEV && false) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
