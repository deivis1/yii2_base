<?php

return [
    'adminEmail' => 'admin@gmail.com',
    'senderEmail' => 'sender@gmail.com',
    'senderName' => 'MyCodeBase website',
    'senderPassword' => 'pass',
    'passwordResetTokenExpires' => 2 * 3600,
    'verifyEmailTokenExpires' => 2 * 24 * 3600,
    'icon-framework' => 'fa',
    'bsVersion' => '4.x',
];
