<?php

return [  
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=my_code_base;port=3306',
    'username' => 'db_user',
    'password' => 'db_pass',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
