<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $type integer */

use yii\helpers\Html;
use app\controllers\SiteController;
$this->params['vueFile'] = 'index';
?>
<div class="site-message">

    <div class="alert alert-<?php echo $type === SiteController::MESSAGE_SUCCESS ? 'success' : 'danger'; ?>">
        <?= nl2br(Html::encode($message)) ?>
    </div>

</div>
