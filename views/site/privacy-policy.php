<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('app', 'TITLE_PRIVACY_POLICY');
$this->params['fullHeight'] = false;
$this->params['breadcrumbs'] = false;
$this->params['vueFile'] = 'index';
?>
<div class="site-privacy-policy col-md-10 col-lg-8">
    
  <div class="card">
    <div class="card-body pt-3 px-4 pb-2">

      <h4 class="site-card-header card-title text-center mb-0 mb-sm-3 mt-0 mt-sm-1"><?php echo Html::encode($this->title); ?></h4>
      <hr class="mb-3 mb-sm-4">

        <p><?php echo Yii::t('app', 'PRIVACY_POLICY_PARAGRAPH_1'); ?></p>
        <?php echo Yii::t('app', 'PRIVACY_POLICY_PARAGRAPH_2'); ?>
        <p><?php echo Yii::t('app', 'PRIVACY_POLICY_PARAGRAPH_3'); ?></p>
        <?php echo Yii::t('app', 'PRIVACY_POLICY_PARAGRAPH_4'); ?>
        <p><?php echo Yii::t('app', 'PRIVACY_POLICY_PARAGRAPH_5'); ?></p>
        <?php echo Yii::t('app', 'PRIVACY_POLICY_PARAGRAPH_6'); ?>

      </div>
  </div>
    
</div>