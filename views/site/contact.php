<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */
/* @var $myData string */

use app\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\captcha\Captcha;
use yii\web\View;

$this->title = Yii::t('app', 'TITLE_CONTACT');
$this->params['fullHeight'] = true;
$this->params['breadcrumbs'] = false;
?>
<div id="vue-component" class="site-contact<?php echo Yii::$app->user->isGuest ? '' : '-logged'; ?> col-md-9 col-lg-7">
    
  <div class="site-hide">
    <div :class="'site-contact-alert alert alert-' + alertColor + (alertShow ? ' show' : '')"
      role="alert">{{ alertText }}</div>
  </div>
    
  <div class="card">
    <div class="card-body pt-3 px-4 pb-2 pb-sm-3">

      <h4 class="site-card-header card-title text-center mb-0 mb-sm-3 mt-0 mt-sm-1"><?php echo Html::encode($this->title); ?></h4>
      <hr class="mb-3 mb-sm-4">

        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

          <?php if (Yii::$app->user->isGuest) { ?>
      
            <div class="form-row">
              <div class="col-md-6 px-2">
                <?= $form->field($model, 'email')->textInput([
                    'v-bind' => 'email',
                    'autofocus' => '',
                ]) ?>
              </div>

              <div class="col-md-6 px-2">
                <?= $form->field($model, 'name')->textInput(['v-bind' => 'name']) ?>
              </div>
            </div>
      
          <?php } ?>

          <?php $inputOptions = [
              'v-bind' => 'subject',
          ];
          
          if (!Yii::$app->user->isGuest) {
            $inputOptions['autofocus'] = true;
          }
          
          echo $form->field($model, 'subject')->textInput($inputOptions) ?>

          <?= $form->field($model, 'message', [
              'options' => [
                  'class' => 'form-group mb-2 mb-sm-3',
              ]
          ])->textarea([
              'rows' => 6,
              'class' => 'form-control site-no-resize',
              'v-bind' => 'message',
          ]) ?>

          <?php if (Yii::$app->user->isGuest) { ?>
            <div class="form-row d-flex justify-content-center align-items-center mt-0 mt-sm-4">
              <div class="col-sm-12 col-md-6">
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row justify-content-center align-items-center"><div class="site-contact-verify col-lg-6 px-3 px-sm-0 mb-2 mb-sm-0">{image}</div><div class="col-lg-5 px-3 px-sm-0">{input}</div></div>',
                    'options' => [
                        'v-bind' => 'verifyCode',
                    ],
                ])->label(false) ?>
              </div>
            </div>
          <?php } ?>

          <div class="form-group text-center mt-2 mt-sm-<?php echo Yii::$app->user->isGuest ? 2 : 4; ?> mb-0">
              <?= Html::button(Html::tag(
                    'span', null, [
                      'class' => 'spinner-grow spinner-grow-sm site-hide mr-1',
                      'v-show' => 'loading',
                  ]) . ' ' . Yii::t('app', 'BUTTON_SUBMIT'), [
                  'class' => 'btn btn-primary button-width mx-2',
                  'v-on:click' => 'validateForm($event)',
              ]) . Html::button(Yii::t('app', 'BUTTON_RESET'), [
                  'class' => 'btn btn-secondary button-width mx-2',
                  'v-on:click' => 'resetForm',
              ]) ?>
          </div>

        <?php ActiveForm::end(); ?>

      </div>
  </div>
    
</div>

<?php
$this->registerJs("
    window.myData = JSON.parse('" . $myData . "');"
, View::POS_BEGIN);
$this->registerJsFile('@web/js/vue-contact.js', ['depends' => [AppAsset::className()]]);