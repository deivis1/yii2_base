<?php

/* @var $this yii\web\View */

use app\components\Util;
use yii\bootstrap4\Html;

$this->title = Yii::$app->name;
$this->params['vueFile'] = 'index';
?>
<div class="site-index col-lg-6 card-deck text-center">
  <div class="card mb-4 shadow-sm">
    <div class="card-header">
      <h4 class="site-card-header my-0 font-weight-normal"><?php echo Yii::t('app', 'HOME_HEADER'); ?></h4>
    </div>
    <div class="card-body text-center pb-1 pt-3 pt-sm-4">
      <ul class="site-feature-list list-unstyled mb-3 mb-sm-4 text-left">
        <li><?php echo Util::icon('check') . Yii::t('app', 'HOME_FEATURE_1'); ?></li>
        <li><?php echo Util::icon('check') . Yii::t('app', 'HOME_FEATURE_2'); ?></li>
        <li><?php echo Util::icon('check') . Yii::t('app', 'HOME_FEATURE_3'); ?></li>
        <li><?php echo Util::icon('check') . Yii::t('app', 'HOME_FEATURE_4'); ?></li>
      </ul>
      <div>
        <?php echo Html::a(
           Yii::t('app', 'BUTTON_TRY_DEMO'),
           '/code/demo', [
             'class' => 'btn btn-lg btn-success button-width mx-2',
            ]
        ) . Html::a(
           Yii::t('app', 'BUTTON_GET_STARTED'),
           Yii::$app->user->isGuest ? '/user/signup' : '/code/list', [
             'class' => 'btn btn-lg btn-primary button-width mx-2',
            ]
        ); ?>
      </div>
    </div>
  </div>
</div>
