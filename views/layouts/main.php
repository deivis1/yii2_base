<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use kartik\icons\Icon;
use app\components\Util;

AppAsset::register($this);
Icon::map($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo Html::csrfMetaTags(); ?>
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ; ?>
    
<div id="app" class="<?php echo Util::isFullHeight($this) ? 'layout-flex-column' : ''; ?>">

<nav class="site-header navbar navbar-expand-lg navbar-light bg-light border-bottom px-sm-3">
  <div class="container">
    <a class="site-navbar-brand navbar-brand" href="<?php echo Yii::$app->homeUrl; ?>"
       >My <span class="site-navbar-brand-code">{Code}</span> Base</a>
    <button class="navbar-toggler" 
        type="button" data-toggle="collapse" 
        data-target="#navbarSupportedContent" 
        aria-controls="navbarSupportedContent" 
        aria-expanded="false" 
        aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="site-nav navbar-nav mr-auto">
        <li class="site-nav-item nav-item<?php echo Util::getMenuActive($this, 'site/index'); ?>">
          <a class="nav-link" href="<?php echo Yii::$app->homeUrl; ?>"><?php echo Yii::t('app', 'MENU_HOME'); ?></a>
        </li>
        <li class="site-nav-item nav-item<?php echo Util::getMenuActive($this, 'site/contact'); ?>">
            <?php echo Html::a(
                Yii::t('app', 'MENU_CONTACT'), 
                Url::to(['site/contact']),
                [
                    'class' => 'nav-link',
                ]
            ); ?>
        </li>
        
        <?php if (!Yii::$app->user->isGuest) { ?>
            <li class="site-nav-item nav-item<?php echo Util::getMenuActive($this, 'code/list'); ?>">
                <?php echo Html::a(
                    Yii::t('app', 'MENU_CODE'), 
                    Url::to(['code/list']),
                    [
                        'class' => 'nav-link',
                    ]
                ); ?>
            </li>
        <?php } ?>
        
      </ul>
        <ul class="site-nav site-nav-right navbar-nav">
          <?php if (Yii::$app->user->isGuest) { ?>
          <li class="site-nav-item nav-item">
            <a class="signup-link<?php echo Util::getMenuActive($this, 'user/signup'); ?>" 
               href="<?php echo Url::to(['user/signup']); ?>"
               ><?php echo Yii::t('app', 'MENU_SIGNUP'); ?></a>
          </li>
          <li class="site-nav-item nav-item<?php echo Util::getMenuActive($this, 'user/login'); ?>">
            <a class="nav-link" 
               href="<?php echo Url::to(['user/login']); ?>"
               ><?php echo Yii::t('app', 'MENU_LOGIN'); ?></a>
          </li>
          <?php } else { ?>
          
              <li class="site-nav-item nav-item<?php echo Util::getMenuActive($this, 'user/account'); ?>">
                <a class="nav-link" 
                   href="<?php echo Url::to(['user/account']); ?>"
                   ><?php echo Yii::t('app', 'MENU_MY_ACCOUNT'); ?></a>
              </li>
          
              <li class="site-nav-item nav-item">
                  <?php echo Html::beginForm(['/user/logout'], 'post')
                      . Html::submitButton(
                          Yii::t('app', 'MENU_LOGOUT'),
                          ['class' => 'logout-link']
                      )
                      . Html::endForm(); ?>
              </li>
              
          <?php } ?>
          
          <li class="nav-item dropdown text-center">
              <a class="nav-link dropdown-toggle" 
                  href="#" 
                  id="navbarDropdown" 
                  role="button" 
                  data-toggle="dropdown" 
                  aria-haspopup="true" 
                  aria-expanded="false">
                  <?php echo Yii::t('app', 'MENU_LANGUAGE'); ?>
              </a>
              <div class="site-dropdown-menu dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item<?php echo Yii::$app->language === 'en' ? ' active' : '';  ?>"
                      href="/user/language/en"
                      ><?php echo Yii::t('app', 'MENU_LANGUAGE_EN'); ?></a>
                  <a class="dropdown-item<?php echo Yii::$app->language === 'lt' ? ' active' : '';  ?>"
                      href="/user/language/lt"
                      ><?php echo Yii::t('app', 'MENU_LANGUAGE_LT'); ?></a>
              </div>
          </li>
        </ul>
    </div>
  </div>
</nav>

<div class="container flex-grow-1 d-flex justify-content-center px-0 px-md-1 <?php 
    echo Util::isFullHeight($this) ? 'align-items-center' : 'align-items-start'; ?>">
    <?php
    if (!empty($this->params['breadcrumbs'])) {
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
    }
    echo Alert::widget();
    echo $content;
    ?>
</div>

<?php if (!Yii::$app->session->has('cookieConsent')) { ?>
  
  <div class="site-hide">
    <div :class="'site-cookie-info d-flex justify-content-center align-items-center px-2 px-md-0' + (showCookieConsent ? ' show' : '')">
      <span class="site-cookie-text flex-grow-1 py-sm-2 py-md-0"><?php echo Yii::t('app', 'COOKIE_MESSAGE'); ?><a 
        href="/site/privacy-policy" 
        class="btn-link" 
        target="_blank"><?php echo Yii::t('app', 'PRIVACY_POLICY_LINK'); ?></a></span>
      <div class="site-cookie-close btn btn-primary mb-2 mb-md-0 mt-1 mt-md-0 mx-3" v-on:click="cookieConsent"><?php echo Yii::t('app', 'BUTTON_OK'); ?></div>
    </div>
  </div>
    
<?php } ?>

</div>

<?php if (isset($this->params['vueFile'])) {
    $this->registerJsFile("@web/js/vue-{$this->params['vueFile']}.js", ['depends' => [AppAsset::className()]]); 
}
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
