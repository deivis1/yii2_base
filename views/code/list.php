<?php

/* @var yii\web\View $this  */
/* @var app\models\Code $model  */
/* @var string $myData */
/* @var array $categoryList */
/* @var array $languageList */

use yii\helpers\Html;
use yii\web\View;
use kartik\select2\Select2;

$this->title = Yii::t('app', 'TITLE_CODE');
$this->params['breadcrumbs'] = null;
$this->params['fullHeight'] = false;
$this->params['vueFile'] = 'code';
?>
<div id="vue-component" class="site-code" v-cloak>
    <textarea class="code-textarea"></textarea>
    <div :class="[alertShow ? 'show' : '', 'site-alert-container']">
      <div :class="'site-alert alert alert-' + alertColor" role="alert">{{ alertText }}</div>
    </div>
    <div class="row justify-content-center">
        <!--Inputs row-->
        <div class="row w-100 justify-content-center">

            <!--Search inputs-->
            <div class="search-container-sm col-lg-3 mb-4 px-0 px-lg-4 d-flex flex-column justify-content-end">
                <div class="d-flex justify-content-center align-items-end mb-3 mb-lg-2">
                    <label
                        for="search-code"
                        class="col-form-label col-form-label-sm mr-3"
                    ><?php echo Yii::t('app', 'LABEL_SEARCH'); ?></label>
                    <div class="search-container">
                      <input type="text"
                          id="search-code"
                          class="form-control form-control-sm"
                          @input="onSearchCriteriaInput"
                          v-model="search"
                          data-container="body"
                          data-toggle="popover"
                          data-placement="top"
                          data-content="<?php echo Yii::t('app', 'SEARCH_CRITERIA_INFO'); ?>"
                          />

                      <div :class="'search-clear' + (search.length ? ' show' : '')"
                        @click="clearSearch"
                        >&times;</div>
                    </div>
                </div>
                <div class="d-flex justify-content-start align-items-start">
                    <div class="d-flex flex-column align-items-start">
                    <label
                        for="filter-categories"
                        class="filter-categories-label col-form-label col-form-label-sm mr-3 mb-0"
                    ><?php echo Yii::t('app', 'LABEL_FILTER_CATEGORIES'); ?></label>
                    <a href="/category/list"
                        title="<?php echo Yii::t('app', 'BUTTON_EDIT_CATEGORIES'); ?>"
                        class="btn btn-primary category-edit-padding mx-2 mt-2 mb-0"
                    ><i class="fa fa-edit" aria-hidden="true"></i></a>
                    </div>
                    <div class="filter-categories-code-width">
                        <?php echo Select2::widget([
                            'id' => 'filter-categories',
                            'name' => 'filter-categories',
                            'data' => $categoryList,
                            'size' => Select2::SMALL,
                            'hideSearch' => true,
                            'showToggleAll' => false,
                            'pluginLoading' => false,
                            'options' => [
                                'placeholder' => Yii::t('app', 'SELECT_PLACEHOLDER'),
                                'multiple' => true,
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            'pluginEvents' => [
                                'change' => 'onFilterCategoryChange',
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>

            <!--Code editor inputs-->
            <div class="col-lg-8">

                <!--Code editor inputs container-->
                <div class="row w-100 mb-3 ml-1 pl-3">

                    <div class="col-lg-6">
                        <div class="form-group row mb-2">
                            <label
                                for="<?php echo Html::getInputId($model, 'description'); ?>"
                                class="col-sm-3 col-form-label col-form-label-sm"
                            ><?php echo Yii::t('app', 'LABEL_INPUT_DESCRIPTION'); ?></label>

                            <div class="col-sm-9">
                                <input type="text"
                                    id="<?php echo Html::getInputId($model, 'description'); ?>"
                                    name="<?php echo Html::getInputName($model, 'description'); ?>"
                                    class="form-control form-control-sm"
                                    v-model="codeDescription" />
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label
                                for="<?php echo Html::getInputId($model, 'keywords'); ?>"
                                class="col-sm-3 col-form-label col-form-label-sm"
                            ><?php echo Yii::t('app', 'LABEL_INPUT_KEYWORDS'); ?></label>

                            <div class="col-sm-9">
                                <input type="text"
                                    id="<?php echo Html::getInputId($model, 'keywords'); ?>"
                                    name="<?php echo Html::getInputName($model, 'keywords'); ?>"
                                    class="form-control form-control-sm"
                                    v-model="codeKeywords" />
                            </div>
                        </div>
                        <div class="code-categories-container form-group row mb-3 mg-lg-0">
                            <label
                                for="item-categories"
                                class="col-sm-3 col-form-label col-form-label-sm"
                            ><?php echo Yii::t('app', 'LABEL_CATEGORIES'); ?></label>

                            <div class="col-sm-9">
                                <?php echo Select2::widget([
                                    'id' => 'code-categories',
                                    'name' => 'Code[categories]',
                                    'data' => $categoryList,
                                    'size' => Select2::SMALL,
                                    'hideSearch' => true,
                                    'showToggleAll' => false,
                                    'pluginLoading' => false,
                                    'options' => [
                                        'placeholder' => Yii::t('app', 'SELECT_PLACEHOLDER'),
                                        'multiple' => true,
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                    'pluginEvents' => [
                                        'change' => 'onItemCategoryChange',
                                    ],
                                ]); ?>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-6 d-flex flex-column justify-content-end align-items-start">
                        <div class="d-flex h-100 align-items-center mb-3 mb-lg-0">
                            <div>
                                <button type="button"
                                    title="<?php echo Yii::t('app', 'BUTTON_TITLE_SAVE'); ?>"
                                    :class="[ changed ? '' : 'disabled', 'btn btn-primary code-button mx-2 mb-0']"
                                    :aria-disabled="changed"
                                    @click="saveItem"
                                ><i class="fa fa-save" aria-hidden="true"></i></button>
                                <button type="button"
                                    title="<?php echo Yii::t('app', 'BUTTON_TITLE_CREATE_NEW'); ?>"
                                    class="btn btn-primary code-button mx-2 mb-0"
                                    @click="confirmNewItem"
                                ><i class="fa fa-plus" aria-hidden="true"></i></button>
                                <button type="button"
                                    title="<?php echo Yii::t('app', 'BUTTON_TITLE_REMOVE'); ?>"
                                    :class="[ isSelected ? '' : 'disabled', 'btn btn-primary code-button mx-2 mb-0']"
                                    :aria-disabled="isSelected"
                                    @click="confirmRemove"
                                ><i class="fa fa-times" aria-hidden="true"></i></button>
                                <button type="button"
                                    title="<?php echo Yii::t('app', 'BUTTON_TITLE_COPY_TO_CLIPBOARD'); ?>"
                                    :class="[ code.length > 0 ? '' : 'disabled', 'btn btn-primary code-button mx-2 mb-0']"
                                    :aria-disabled="code.length == 0"
                                    @click="copyCodeToClipboard"
                                ><i class="fa fa-copy" aria-hidden="true"></i></button>
                                <button type="button"
                                    title="<?php echo Yii::t('app', 'BUTTON_TITLE_SHARE'); ?>"
                                    :class="[ isSelected ? '' : 'disabled', 'btn btn-primary code-button mx-2 mb-0']"
                                    :aria-disabled="isSelected"
                                    @click="openShareItemDialog"
                                ><i class="fa fa-share-alt" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="ml-2 mb-lg-3">
                            <?php echo Html::dropDownList('language_id', null, $languageList, [
                                'id' => 'code-language',
                                'class' => 'site-code-language',
                                'v-model' => 'codeLanguageId',
                                'v-on:change' => 'onChangeLanguage',
                            ]); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!--Content row-->
        <div class="row w-100 justify-content-center">
            <!--Result list-->
            <div class="col-lg-3">
                <ul class="list-group site-list-group mx-5 mx-lg-0 mb-5 mb-lg-0">
                    <li
                        :class="[item.id === selected ? 'selected' : '', 'list-group-item result-item no-text-select']"
                        v-for="item in results"
                        @click="confirmSelect(item.id)"
                    >
                        <div class="site-item-data site-item-description">{{ item.description }}</div>
                        <div class="site-item-data site-item-keywords"><?php
                            echo Yii::t('app', 'LABEL_KEYWORDS'); ?>: {{ displayKeywords(item.keywords) }}</div>
                        <div class="site-item-data site-item-code" v-html="displayCode(item.code)"></div>
                        <div class="site-item-data site-item-updated d-flex align-items-center justify-content-end"
                            ><?php echo Yii::t('app', 'LABEL_UPDATED'); ?>: {{ item.updated | timeFilter }}</div>
                    </li>
                </ul>
            </div>

            <!--Code editor-->
            <div class="col-10 col-lg-8">
                <div class="site-code-shared badge badge-info no-text-select"
                    data-id="copyPopover"
                    data-container="body"
                    data-toggle="popover"
                    data-placement="top"
                    data-html="true"
                    v-if="isShared"
                    v-on:mouseout="onMouseoutCopyBadge"
                    v-on:click="removePopoverTimeout"
                ><?php echo Yii::t('app', 'LABEL_SHARED'); ?></div>
                <div class="site-hide-always">
                  <div class="site-popover-body"
                    @mouseover="removePopoverTimeout"
                    @mouseout="onMouseoutCopyPopover"
                    ><div class="site-shared-code-copy-link btn-link"
                    @click="copyShareLinkToClipboard"><?php
                    echo Yii::t('app', 'LABEL_COPY_LINK_TO_CLIPBOARD'); ?></div>
                  </div>
                </div>
                <pre id="editor" class="code-input"></pre>
            </div>
        </div>
    </div>
    <div class="modal fade"
      id="myModal"
      tabindex="-1"
      role="dialog"
      aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">{{ modalTitle }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <input type="text" class="site-modal-copy-input">
          <div class="modal-body">{{ modalText }}<div class="site-modal-copy"
            v-if="showCopyLink"
            @click="onModalCopyLink"
            title="<?php echo Yii::t('app', 'TITLE_COPY_LINK_TO_CLIPBOARD'); ?>"><i class="fa fa-copy" aria-hidden="true"></i></div></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal"
              @click="onNegativeModalAction">{{ modalCancelButton }}</button>
            <button type="button" class="btn btn-secondary" @click="onPositiveModalAction">{{ modalActionButton }}</button>
          </div>
        </div>
      </div>
    </div>
</div>
<?php
$this->registerJs("
    window.myData = JSON.parse(\"" . $myData . "\");" .
    "let onFilterCategoryChange = () => { }; " .
    "let onItemCategoryChange = () => { }; "
, View::POS_BEGIN);
$this->registerJsFile("@web/js/ace.js", ['charset' => 'utf-8'], View::POS_END);
