<?php

/* @var $this yii\web\View */
/* @var Code $model */
/* @var string $userName */
/* @var string $myData */

use yii\web\View;

$this->title = Yii::t('app', 'TITLE_SHARED_CODE');
$this->params['fullHeight'] = false;
$this->params['breadcrumbs'] = false;
$this->params['vueFile'] = 'shared';
?>
<div class="site-shared-code d-flex justify-content-center" v-cloak>
    
  <textarea class="code-textarea"></textarea>
  <div class="site-hide">
    <div :class="'site-shared-code-alert alert alert-' + alertColor + (alertShow ? ' show' : '')"
      role="alert">{{ alertText }}</div>
  </div>
  
  <div class="col-md-10 col-lg-8">
      
    <div class="row justify-content-center">
      <div class="col-sm-12 col-md-8 col-lg-6">

        <div class="card mb-3 mb-sm-4">
          <div class="card-body py-2 px-3">

            <div class="row">
              <div class="col-sm-4 site-shared-code-label"><?php echo Yii::t('app', 'LABEL_SHARED_CODE_AUTHOR') . ':'; ?></div>
              <div class="col-sm-8 site-shared-code-value"><?php echo $userName; ?></div>
            </div>

            <div class="row">
              <div class="col-sm-4 site-shared-code-label"><?php echo $model->attributeLabels()['description'] . ':'; ?></div>
              <div class="col-sm-8 site-shared-code-value"><?php echo $model->description; ?></div>
            </div>

            <div class="row">
              <div class="col-sm-4 site-shared-code-label"><?php echo $model->attributeLabels()['updated_at'] . ':'; ?></div>
              <div class="col-sm-8 site-shared-code-value"><?php echo date('Y-m-d H:i:s', $model->updated_at); ?></div>
            </div>

          </div>
        </div>
        
        <div class="shared-code-button-container">
          <button type="button"
              title="<?php echo Yii::t('app', 'BUTTON_TITLE_COPY_TO_CLIPBOARD'); ?>"
              class="btn btn-primary shared-code-button mb-0"
              @click="copyCodeToClipboard"
          ><i class="fa fa-copy" aria-hidden="true"></i></button>
        </div>

      </div>
    </div>
    
      
    <pre id="editor" class="code-input"></pre>
    
  </div>
  
</div>

<?php
$this->registerJs("
    window.myData = JSON.parse(\"" . $myData . "\");"
, View::POS_BEGIN);
$this->registerJsFile("@web/js/ace.js", ['charset' => 'utf-8'], View::POS_END);