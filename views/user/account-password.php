<?php

/* @var yii\web\View $this  */
/* @var app\models\User $model  */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

    $form = ActiveForm::begin([
      'id' => 'password-change-form',
      'layout' => ActiveForm::LAYOUT_HORIZONTAL,
      'enableClientScript' => false,
      'fieldConfig' => [
          'horizontalCssClasses' => [
              'label' => 'col-sm-4 pt-1 px-0',
              'offset' => 'offset-sm-3',
              'wrapper' => 'col-sm-7',
              'error' => '',
              'hint' => '',
          ],
      ],
    ]); ?>

      <?= $form->field($model, 'currentPassword', [
          'options' => [
              'class' => 'form-group row justify-content-center',
          ],
      ])->passwordInput([
          'v-model' => 'currentPassword',
          'autofocus' => true,
      ]) ?>

      <?= $form->field($model, 'password', [
          'options' => [
              'class' => 'form-group row justify-content-center',
          ],
      ])->passwordInput([
          'v-model' => 'newPassword',
      ])->label(Yii::t('app', 'USER_LABEL_NEW_PASSWORD')) ?>

      <?= $form->field($model, 'passwordConfirm', [
          'options' => [
              'class' => 'form-group row justify-content-center',
          ],
      ])->passwordInput([
          'v-model' => 'newPasswordConfirm',
      ])->label(Yii::t('app', 'USER_LABEL_CONFIRM_NEW_PASSWORD')) ?>

      <div class="form-group mt-45 mb-1 text-center">
        <?php echo Html::button(Html::tag(
            'span', null, [
            'class' => 'spinner-grow spinner-grow-sm site-hide mr-1',
            'v-show' => 'loadingPassword',
        ]) . Yii::t('app', 'BUTTON_CHANGE'), [
            'v-bind:class' => "'btn btn-primary button-width mx-2' + (loadingPassword ? ' disabled' : '')", 
            'name' => 'password-change-button',
            'v-bind:disabled' => 'loadingPassword',
            'v-on:click' => 'changePassword'
        ]) . Html::button(Yii::t('app', 'BUTTON_RESET'), [
            'class' => 'btn btn-secondary button-width mx-2',
            'v-on:click' => 'resetPasswordForm',
        ]) ?>
      </div>

    <?php ActiveForm::end(); ?>