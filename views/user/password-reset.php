<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\User */
/* @var $key string */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app', 'TITLE_PASSWORD_RESET');
$this->params['breadcrumbs'] = false;
$this->params['fullHeight'] = true;
$this->params['vueFile'] = 'password-reset';
?>
<div id="vue-component" class="site-password-reset col-sm-10 col-md-8 col-lg-5">
    
    <div class="card">
        <div class="card-body pt-3 px-4">
    
            <h4 class="card-title text-center mb-0 mb-sm-3"><?php echo Html::encode($this->title); ?></h4>
            <hr class="mb-4">

            <?php $form = ActiveForm::begin([
                'id' => 'password-reset-form',
                'action' => Url::to(['/user/password-reset-post']),
                'layout' => ActiveForm::LAYOUT_DEFAULT,
            ]); ?>

                <?php echo $form->field($model, 'password')->passwordInput(); ?>
            
                <?php echo $form->field($model, 'passwordConfirm')->passwordInput(); ?>
            
                <?php echo Html::hiddenInput('tag', $key); ?>
            
                <div class="form-group mt-4 text-center">
                    
                    <?php echo Html::submitButton(Yii::t('app', 'BUTTON_CHANGE'), [
                        'class' => 'btn btn-primary button-width mx-2',
                        'name' => 'change-password-button'
                    ]); ?>
                    
                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
        
</div>