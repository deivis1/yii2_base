<?php

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $myData string */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\web\View;

$this->title = Yii::t('app', 'TITLE_EMAIL_VERIFICATION');
$this->params['breadcrumbs'] = false;
$this->params['fullHeight'] = true;
$this->params['vueFile'] = 'email-verification-form';
?>
<div id="vue-component" class="site-password-reset col-sm-10 col-md-8 col-lg-5">
    
    <div class="card">
        <div class="card-body pt-3 px-4">
    
            <h5 class="card-title text-center mb-0 mb-sm-3"><?php echo Yii::t('app', 'MESSAGE_PLEASE_VERIFY_EMAIL'); ?></h5>
            <hr class="mb-4">

            <?php $form = ActiveForm::begin([
                'id' => 'email-verification-send-form',
                'enableClientScript' => false,
            ]); ?>

                <?php echo $form->field($model, 'email', [
                    'options' => [
                        'class' => 'form-group mx-lg-5',
                    ],
                ])->textInput([
                    'v-model' => 'email',
                    'v-on:input' => 'emailInput',
                ])->label(false); ?>
            
                <div class="form-group mt-4 text-center">

                    <?php echo Html::button(Html::tag(
                            'span', null, [
                                'class' => 'spinner-grow spinner-grow-sm site-hide',
                                'v-show' => 'loading',
                            ]) . ' ' . Yii::t('app', 'BUTTON_RESEND'), [
                        'v-bind:class' => "'btn btn-primary button-width mx-2' + (loading ? ' disabled' : '')",
                        'name' => 'send-button',
                        'v-bind:disabled' => 'loading',
                        'v-on:click' => 'send',
                    ]); ?>

                </div>
            
            <?php ActiveForm::end(); ?>

        </div>
    </div>
        
    <div class="site-hide">
      <div :class="(alertShow ? 'show ' : '') + 
          'site-alert-message alert alert-' + alertColor + ' position-absolute mt-4 mx-3 text-center absolute-margins'"
      >{{ alertText }}</div>
    </div>
        
</div>

<?php
$this->registerJs("
    window.myData = JSON.parse('" . $myData . "');"
, View::POS_BEGIN);