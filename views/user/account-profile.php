<?php

/* @var yii\web\View $this  */
/* @var app\models\User $model  */
/* @var array $languageList  */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

    $form = ActiveForm::begin([
      'id' => 'profile-form',
      'layout' => ActiveForm::LAYOUT_HORIZONTAL,
      'enableClientScript' => false,
      'fieldConfig' => [
          'horizontalCssClasses' => [
              'label' => 'col-sm-3 pt-1',
              'offset' => 'offset-sm-3',
              'wrapper' => 'col-sm-7',
              'error' => '',
              'hint' => '',
          ],
      ],
    ]); ?>

      <?= $form->field($model, 'email', [
          'options' => [
              'class' => 'form-group row justify-content-center',
          ],
      ])->textInput([
          'v-model' => 'email',
          'autofocus' => true,
      ]) ?>

      <?= $form->field($model, 'name', [
          'options' => [
              'class' => 'form-group row justify-content-center',
          ],
      ])->textInput([
          'v-model' => 'name',
      ]) ?>

      <?php echo $form->field($model, 'language', [
          'options' => [
              'class' => 'form-group row justify-content-center',
          ],
      ])->dropDownList($languageList, [
          'v-model' => 'language',
      ]); ?>

      <div class="form-group mt-45 mb-1 text-center">
        <?php echo Html::button(Html::tag(
            'span', null, [
            'class' => 'spinner-grow spinner-grow-sm site-hide mr-1',
            'v-show' => 'loadingAccount',
        ]) . Yii::t('app', 'BUTTON_UPDATE'), [
            'v-bind:class' => "'btn btn-primary button-width mx-2' + (loadingAccount ? ' disabled' : '')", 
            'name' => 'profile-button',
            'v-bind:disabled' => 'loadingAccount',
            'v-on:click' => 'updateAccount',
        ]) . Html::button(Yii::t('app', 'BUTTON_RESET'), [
            'class' => 'btn btn-secondary button-width mx-2',
            'v-on:click' => 'resetAccountForm',
        ]) ?>
      </div>

    <?php ActiveForm::end(); ?>