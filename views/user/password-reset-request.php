<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\User */
/* @var $myData string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use yii\web\View;

$this->title = Yii::t('app', 'TITLE_PASSWORD_RESET_REQUEST');
$this->params['breadcrumbs'] = false;
$this->params['fullHeight'] = true;
$this->params['vueFile'] = 'password-reset-request';
?>
<div id="vue-component" class="site-password-reset-request col-sm-10 col-md-8 col-lg-5 position-relative">
    
    <div class="card">
        <div class="card-body pt-3 px-4">
    
            <h4 class="site-card-header card-title text-center mb-0 mb-sm-3 mt-0 mt-sm-1"><?php 
                echo Html::encode($this->title); ?></h4>
            <hr class="mb-3 mb-sm-4">
            <p class="text-center"><?php echo Yii::t('app', 'PASSSWORD_RESET_REQUEST_TEXT'); ?></p>

            <?php $form = ActiveForm::begin([
                'id' => 'password-reset-request-form',
                'enableClientScript' => false,
            ]); ?>

                <?php echo $form->field($model, 'email', [
                    'options' => [
                        'class' => 'form-group mx-lg-5',
                    ],
                ])->textInput([
                    'v-model' => 'email',
                    'v-on:input' => 'emailInput',
                ])->label(false); ?>
            
                <div class="form-group mt-4 text-center" v-cloak>
                    
                    <?php echo Html::button(Html::tag(
                            'span', null, [
                                'class' => 'spinner-grow spinner-grow-sm site-hide',
                                'v-show' => 'loading',
                            ]) . ' ' . Yii::t('app', 'BUTTON_SEND'), [
                        'v-bind:class' => "'btn btn-primary button-width mx-2' + (loading ? ' disabled' : '')",
                        'name' => 'send-button',
                        'v-bind:disabled' => 'loading',
                        'v-on:click' => 'submit',
                    ]); ?>
                    
                    <?php echo Html::a(
                        Yii::t('app', 'BUTTON_BACK'), 
                        Url::to(['user/login']),
                        [
                            'class' => 'btn btn-secondary button-width mx-2', 
                        ]
                    ); ?>
                    
                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
    
    <div class="site-hide">
      <div :class="(alertShow ? 'show ' : '') + 
          'site-alert-message alert alert-' + alertColor + ' position-absolute mt-4 mx-3 text-center absolute-margins'"
      >{{ alertText }}</div>
    </div>
        
</div>

<?php
$this->registerJs("
    window.myData = JSON.parse('" . $myData . "');"
, View::POS_BEGIN);