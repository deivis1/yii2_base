<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use app\components\Util;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = Yii::t('app', 'TITLE_LOGIN');
$this->params['breadcrumbs'] = null;
$this->params['fullHeight'] = true;
$this->params['vueFile'] = 'login';
?>
<div id="vue-component" class="site-login col-sm-10 col-md-8 col-lg-5 position-relative">
    <div class="card">
        <div class="card-body px-4">
            <h4 class="site-card-header card-title text-center mb-0 mb-sm-3 mt-0 mt-sm-1"
                ><?php echo Yii::t('app', 'TEXT_SIGNIN'); ?></h4>
            <hr class="mb-4">
            <form method="post" action="<?php echo Url::to([$this->context->route]); ?>">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><?php echo Util::icon('user'); ?></span>
                     </div>
                    <input 
                        id="<?php echo Html::getInputId($model, 'username'); ?>" 
                        name="<?php echo Html::getInputName($model, 'username'); ?>" 
                        class="form-control<?php echo array_key_exists('username', $model->errors) ? ' is-invalid' : ''; ?>" 
                        value="<?php echo $model->username; ?>"
                        placeholder="<?php echo Yii::t('app', 'LOGIN_EMAIL_PLACEHOLDER'); ?>" 
                        type="text">
                    <?php if (isset($model->firstErrors['username']) && 0) { ?>
                    <div class="invalid-tooltip invalid-feedback" 
                        ><?php echo $model->firstErrors['username']; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><?php echo Util::icon('lock'); ?></span>
                    </div>
                    <input 
                        id="<?php echo Html::getInputId($model, 'password'); ?>" 
                        name="<?php echo Html::getInputName($model, 'password'); ?>" 
                        class="form-control<?php echo array_key_exists('password', $model->errors) ? ' is-invalid' : ''; ?>" 
                        placeholder="******" 
                        type="password">
                    <?php if (isset($model->firstErrors['password']) && 0) { ?>
                    <div class="invalid-tooltip invalid-feedback" 
                        ><?php echo $model->firstErrors['password']; ?></div>
                    <?php } ?>
                </div>
            </div>
            <?php echo Util::getCSRFField(); ?>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" @click="onSubmit($event)"
                    ><?php echo Yii::t('app', 'BUTTON_LOGIN'); ?></button>
            </div>
                <p class="text-center my-0 mt-sm-4 mb-sm-2"><a href="<?php echo Url::to(['/user/password-reset-request']) ; ?>" 
                    class="btn-link forgot-password-link"
                    @click.prevent="onForgotPassword"
                    ><?php echo Yii::t('app', 'LINK_FORGOT_PASSWORD'); ?></a></p>
            </form>
        </div>
    </div>
    
    <?php if (!empty($model->errors)) { ?>
    <div :class="'site-alert-message alert alert-danger position-absolute mt-4 mx-3 absolute-margins' + (show ? ' show' : '')"  v-cloak>
        <ul class="login-error-list mb-0">
            <?php foreach($model->firstErrors as $error) {
                echo Html::tag('li', $error);
            } ?>
        </ul>
    </div>
    <?php } ?>
    
    <?php if (Yii::$app->session->hasFlash('messageSuccess')) { ?>
    <div :class="'site-alert-message alert alert-success position-absolute mt-4 mx-3 text-center absolute-margins' + (show ? ' show' : '')">
        <?php echo Yii::$app->session->getFlash('messageSuccess'); ?>
    </div>
    <?php } ?>
</div>