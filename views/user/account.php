<?php

/* @var yii\web\View $this  */
/* @var app\models\User $model  */
/* @var string $myData */
/* @var array $languageList */

use yii\web\View;

$this->title = Yii::t('app', 'TITLE_ACCOUNT');
$this->params['breadcrumbs'] = null;
$this->params['fullHeight'] = true;
$this->params['vueFile'] = 'account';
?>
<div id="vue-component" class="site-account" v-cloak>
    
    <div :class="[alertShow ? 'show' : '', 'site-alert-container site-account-alert-container']">
      <div :class="'site-alert alert alert-' + alertColor + (isDangerAlert ? ' alert-dismissible' : '')" 
        role="alert"><div v-html="alertText"></div><button 
          type="button" 
          class="close" 
          v-if="isDangerAlert"
          v-on:click="hideAlert" 
          aria-label="Close"
          ><span aria-hidden="true">&times;</span></button></div>
    </div>
    
    <div class="row w-100 justify-content-center">
        
        <div class="col-sm-12 col-md-10 col-lg-7">
          
          <div class="card">
            <div class="card-body pt-4 px-4">
            
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                  
                <li class="nav-item">
                  <a class="site-tab-link nav-link active" 
                    id="home-tab" 
                    @click="hideAlert"
                    data-toggle="tab" 
                    href="#profile" 
                    role="tab" 
                    aria-controls="profile" 
                    aria-selected="true"><?php echo Yii::t('app', 'ACCOUNT_TAB_PROFILE'); ?></a>
                </li>
                <li class="nav-item">
                  <a class="site-tab-link nav-link" 
                    id="profile-tab" 
                    @click="hideAlert"
                    data-toggle="tab" 
                    href="#change-password" 
                    role="tab" 
                    aria-controls="profile" 
                    aria-selected="false"><?php echo Yii::t('app', 'ACCOUNT_TAB_CHANGE_PASSWORD'); ?></a>
                </li>
              </ul>
                
              <div class="tab-content row justify-content-center" id="myTabContent">
                  
                <div class="col-sm-12 col-md-9 tab-pane fade pt-5 pb-0 px-4 show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                  
                    <?php echo Yii::$app->controller->renderPartial(
                        'account-profile', [
                            'model' => $model,
                            'languageList' => $languageList,
                        ]
                    ); ?>
                    
                </div>
                  
                <div class="col-sm-12 col-md-9 tab-pane fade pt-5 pb-0 px-4" id="change-password" role="tabpanel" aria-labelledby="change-password-tab">
                  
                    <?php echo Yii::$app->controller->renderPartial(
                        'account-password', [
                            'model' => $model,
                        ]
                    ); ?>
                    
                </div>
                  
              </div>
            
            </div>
          </div>
              
        </div>
    </div>
</div>
<?php
$this->registerJs("
    window.myData = JSON.parse(\"" . $myData . "\");"
, View::POS_BEGIN);