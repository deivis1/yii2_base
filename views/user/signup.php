<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\User */
/* @var $languageList array */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = Yii::t('app', 'TITLE_NEW_ACCOUNT');
$this->params['breadcrumbs'] = false;
$this->params['fullHeight'] = true;
$this->params['vueFile'] = 'signup';
?>
<div id="vue-component" class="site-new-account col-md-12 col-lg-8">
    
    <div class="card">
        <div class="card-body pt-3 px-4">
    
            <h4 class="site-card-header card-title text-center mb-0 mb-sm-3 mt-0 mt-sm-1"><?php 
              echo Html::encode($this->title); ?></h4>
            <hr class="mb-3 mb-sm-4">

            <?php $form = ActiveForm::begin([
                'id' => 'signup-form',
                'enableAjaxValidation' => false,
                'validationUrl' => Url::to(['/user/validate-email']),
            ]); ?>

                <div class="form-row">
                    
                    <div class="col-md-6 px-1 px-sm-3">
                        <?php echo $form->field($model, 'email', [
                            'enableAjaxValidation' => true,
                            'options' => [
                                'class' => 'form-group',
                            ],
                            'inputOptions' => [
                                'autofocus' => true,
                            ],
                        ]); ?>
                    </div>
                    
                    <div class="col-md-6 px-1 px-sm-3">
                        <?php echo $form->field($model, 'name'); ?>
                    </div>
                    
                </div>

                <div class="form-row">
                    
                    <div class="col-md-6 px-1 px-sm-3">
                        <?php echo $form->field($model, 'language')->dropDownList($languageList); ?>
                    </div>
                    
                    <div class="col-md-6 px-1 px-sm-3">
                    
                    </div>
                    
                </div>
            
                <div class="form-row">
                    
                    <div class="col-md-6 px-1 px-sm-3">
                        <?php echo $form->field($model, 'password')->passwordInput(); ?>
                    </div>
                    
                    <div class="col-md-6 px-1 px-sm-3">
                        <?php echo $form->field($model, 'passwordConfirm')->passwordInput(); ?>
                    </div>
                    
                </div>
            
                <div class="form-row">
                    
                    <div class="col-md-6 px-1 px-sm-3">
                        <?php echo $form->field($model, 'verifyCode')
                            ->widget(Captcha::className(), [
                                'template' => '<div class="row align-items-center justify-content-around"><div class="col-lg-4">{image}</div><div class="col-lg-7">{input}</div></div>',
                        ]); ?>
                    </div>
                    
                    <div class="col-md-6 px-1 px-sm-3">
                        
                    </div>
                    
                </div>
            
                <div class="form-group mt-3 mt-sm-4 mb-0 mb-sm-3 text-center">
                    <?php echo Html::button(Html::tag(
                        'span', null, [
                        'v-bind:class' => "'spinner-grow spinner-grow-sm site-hide mr-1' + (loading ? ' disabled' : '')",
                        'v-show' => 'loading',
                    ]) . Yii::t('app', 'BUTTON_SIGNUP'), [
                        'class' => 'btn btn-primary button-width mx-2', 
                        'name' => 'register-button',
                        'v-bind:disabled' => 'loading',
                        'v-on:click' => 'validateForm',
                    ]) . Html::button(Yii::t('app', 'BUTTON_RESET'), [
                        'class' => 'btn btn-secondary button-width mx-2',
                        'v-on:click' => 'resetForm',
                    ]) ?>
                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
        
</div>