<?php

/* @var yii\web\View $this */
/* @var string $myData */

use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('app', 'TITLE_CATEGORY');
$this->params['breadcrumbs'] = null;
$this->params['fullHeight'] = false;
$this->params['vueFile'] = 'category';
?>
<div id="vue-component" class="site-category">
    <div :class="[alertShow ? 'show' : '', 'site-alert-container']">
      <div :class="'site-alert alert alert-' + alertColor" role="alert">{{ alertText }}</div>
    </div>
    <div class="row justify-content-center">

        <!--Inputs row-->
        <div class="row w-100 justify-content-center">


            <!--Search inputs-->
            <div class="col-lg-3 mb-4 px-4 d-flex flex-column justify-content-end">

            </div>

            <!--Code editor inputs-->
            <div class="col-lg-8">

                <!--Code editor inputs container-->
                <div class="row w-100 mb-4 ml-1 pl-3">

                    <div class="col-lg-6">
                        <div class="text-center mt-4 mt-lg-5 mb-3">
                            <a href="/code/list"><i class="fa fa-reply mr-2" aria-hidden="true"
                                ></i><?php echo Yii::t('app', 'LINK_GO_BACK_TO_CODE'); ?></a>
                        </div>

                        <div class="form-group row mb-3 d-flex justify-content-center">

                            <label
                                for="search"
                                class="col-sm-2 col-form-label col-form-label-sm"
                            ><?php echo Yii::t('app', 'LABEL_SEARCH'); ?></label>
                            <div class="col-sm-7 category-search-container">
                              <input type="text"
                                  id="search"
                                  class="form-control form-control-sm"
                                  @input="onSearchCriteriaInput"
                                  v-model="search"
                                  data-container="body"
                                  data-toggle="popover"
                                  data-placement="top"
                                  data-content="<?php echo Yii::t('app', 'SEARCH_CRITERIA_INFO'); ?>"
                                  />

                              <div :class="'category-search-clear' + (search.length ? ' show' : '')"
                                @click="clearSearch"
                                >&times;</div>
                            </div>

                        </div>

                    </div>
                    <div class="col-lg-6 d-flex flex-column align-items-start mb-lg-2">
                        <div class="d-flex h-100 align-items-end mb-1">
                            <div>
                                <button type="button"
                                    title="<?php echo Yii::t('app', 'BUTTON_TITLE_CREATE_NEW'); ?>"
                                    class="btn btn-primary code-button mx-1 mb-0"
                                    @click="createNewItem"
                                ><i class="fa fa-plus" aria-hidden="true"></i></button>
                                <button type="button"
                                    title="<?php echo Yii::t('app', 'BUTTON_TITLE_EDIT'); ?>"
                                    :class="[ isSelected ? '' : 'disabled', 'btn btn-primary code-button mx-1 mb-0 category-edit-padding']"
                                    @click="editItem"
                                ><i class="fa fa-edit" aria-hidden="true"></i></button>
                                <button type="button"
                                    title="<?php echo Yii::t('app', 'BUTTON_TITLE_REMOVE'); ?>"
                                    :class="[ selected ? '' : 'disabled', 'btn btn-primary code-button mx-1 mb-0']"
                                    :aria-disabled="isSelected"
                                    @click="confirmRemove"
                                ><i class="fa fa-times" aria-hidden="true"></i></button>
                                <button type="button"
                                    title="<?php echo Yii::t('app', 'BUTTON_TITLE_SORT_DOWN'); ?>"
                                    :class="[ isSelected ? '' : 'disabled', 'btn btn-primary code-button mx-1 mb-0']"
                                    :aria-disabled="isSelected"
                                    @click="onSortDown"
                                ><i class="fa fa-chevron-circle-down" aria-hidden="true"></i></button>
                                <button type="button"
                                    title="<?php echo Yii::t('app', 'BUTTON_TITLE_SORT_UP'); ?>"
                                    :class="[ isSelected ? '' : 'disabled', 'btn btn-primary code-button mx-1 mb-0']"
                                    :aria-disabled="isSelected"
                                    @click="onSortUp"
                                ><i class="fa fa-chevron-circle-up" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!--Content row-->
        <div class="row w-100 justify-content-center">
            <!--Result list-->
            <div class="result-list col-lg-3">
                <ul class="list-group category-site-list-group">
                    <li
                        :class="[item.id === selected ? 'selected' : '', 'list-group-item result-item no-text-select']"
                        v-for="item in results"
                        @click="selectResult(item.id)"
                    >
                        <div class="site-item-data site-item-name">{{ item.name }}</div>
                        <div class="site-item-data site-item-updated d-flex align-items-center justify-content-end"
                            ><?php echo Yii::t('app', 'LABEL_UPDATED'); ?>: {{ item.updated | timeFilter }}</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="modal fade"
      id="myModal"
      tabindex="-1"
      role="dialog"
      aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">{{ modalTitle }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <input type="text" class="site-modal-copy-input">
          <div class="modal-body" v-html="modalText"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal"
              @click="onNegativeModalAction">{{ modalCancelButton }}</button>
            <button type="button" class="btn btn-secondary" @click="onPositiveModalAction">{{ modalActionButton }}</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade"
      id="editModal"
      tabindex="-1"
      role="dialog"
      aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">{{ modalTitle }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <?php $form = ActiveForm::begin([
              'id' => 'edit-name-form',
              'enableAjaxValidation' => true,
              'validationUrl' => Url::to(['/category/validate']),
          ]); ?>

              <div class="d-flex justify-content-center mt-2">

                <?php echo $form->field($model, 'name', [
                    'enableAjaxValidation' => true,
                    'options' => [
                        'class' => 'form-group form-inline mb-0 custom-width',
                    ],
                    'inputOptions' => [
                        'v-model' => 'modalValue',
                        'v-on:keyup.enter' => 'onPositiveModalAction',
                        'autofocus' => true,
                    ],
                ])->label($model->attributeLabels()['name'],
                    ['class' => 'mr-3']); ?>

                <?php echo $form->field($model, 'id', [
                    'options' => [
                        'class' => 'form-group mb-0'
                    ],
                    'inputOptions' => [
                        'v-model' => 'modalId',
                    ],
                ])->hiddenInput()->label(false); ?>

              </div>


          <div class="modal-body"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal"
              @click="onNegativeModalAction">{{ modalCancelButton }}</button>
            <button type="button" class="btn btn-secondary" @click="onPositiveModalAction">{{ modalActionButton }}</button>
          </div>

          <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>
</div>
<?php
$this->registerJs("
    window.myData = JSON.parse(\"" . $myData . "\");"
, View::POS_BEGIN);