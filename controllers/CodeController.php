<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\Code;
use app\models\Category;
use app\models\Language;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use app\components\PhpMessageSource;
use app\models\UserLog;
use app\models\UserLogAction;
use app\components\Util;
use yii\helpers\Url;

class CodeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'list',
                            'create-code',
                            'update-code',
                            'remove-code',
                            'search-code',
                            'get-share-link',
                            'share',
                            'stop-sharing',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [
                            'demo',
                            'shared',
                            'shared-demo',
                            'error',
                        ],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays code snippets page.
     *
     * @return Response|string
     */
    public function actionList()
    {
        $model = new Code;

        $list = Code::find()
            ->with('categories')
            ->where(['user_id' => Util::currentUserId()])
            ->asArray()->all();

        $list = $model->setListCategories($list);

        $myData = [
            'data' => $list,
            'messages' => PhpMessageSource::getMessages('frontend'),
            'shareLink' => Url::to('/code/shared', true) . '/',
            'defaultLanguageId' => Language::DEFAULT_ID,
        ];

        $myData = Util::stringifyCode($myData);

        $categoryList = Yii::$app->user->identity->getCategoryList();
        $languageList = Language::getMappedList();

        return $this->render('list', [
            'model' => $model,
            'myData' => $myData,
            'languageList' => $languageList,
            'categoryList' => $categoryList,
        ]);
    }

    /**
     * Updates code record
     *
     * @throws NotAcceptableHttpException
     * @return string json
     */
    public function actionCreateCode()
    {
        $post = Yii::$app->request->post();

        if (!Yii::$app->request->isPost || !isset($post['Code'])) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }

        $model = new Code(['scenario' => Code::SCENARIO_CREATE]);
        $model->load($post);
        $result = $model->save();
        $show = $result && $model->checkFiltered($post['search'], $post['categories']);

        if ($result) {
            UserLog::create(UserLogAction::ACTION_CODE_CREATE);
        }

        $response = [
            'message' => $result ? 'success' : 'error',
            'show' => $show,
        ];

        if ($result) {
            $attributes = $model->attributes;
            $attributes['categories'] = $model->categoryIds;

            $response['item'] = $attributes;
        }

        return json_encode($response);
    }

    /**
     * Updates code record
     *
     * @throws NotAcceptableHttpException
     * @return string json
     */
    public function actionUpdateCode()
    {
        $post = Yii::$app->request->post();

        if (!Yii::$app->request->isPost ||
            !Yii::$app->request->isAjax ||
            !isset($post['Code'])
        ) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }

        $model = Code::loadModel($post['Code']['id']);

        if (!$model->checkOwner()) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'ERROR_WRONG_OWNER'));
        }

        $model->setScenario(Code::SCENARIO_UPDATE);
        $model->load($post);
        $result = $model->save();

        if ($result) {
            $categoryIds = !empty($post['Code']['categories']) ?
                explode(',', $post['Code']['categories']) : [];

            $model->saveCategories($categoryIds);
        }

        $show = $result && $model->checkFiltered($post['search'], $post['categories']);

        if ($result) {
            UserLog::create(UserLogAction::ACTION_CODE_UPDATE);
        }

        $response = [
            'message' => $result ? 'success' : 'error',
            'show' => $show,
        ];

        if ($result) {
            $attributes = $model->attributes;
            $attributes['categories'] = $model->categoryIds;

            $response['item'] = $attributes;
        }

        return json_encode($response);
    }

    /**
     * Removes code record
     *
     * @param integer $id Model id
     * @throws NotAcceptableHttpException
     * @return string json
     */
    public function actionRemoveCode($id)
    {
        if (!Yii::$app->request->isPost ||
            !Yii::$app->request->isAjax
        ) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }

        $model = Code::loadModel($id);

        if (!$model->checkOwner()) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'ERROR_WRONG_OWNER'));
        }

        $result = $model->deleteModel();

        if ($result) {
            UserLog::create(UserLogAction::ACTION_CODE_DELETE);
        }

        return json_encode([
            'message' => $result ? 'success' : 'error',
        ]);
    }

    /**
     * Returns code list
     *
     * @throws NotAcceptableHttpException
     * @return string json
     */
    public function actionSearchCode()
    {
        $post = Yii::$app->request->post();

        if (!Yii::$app->request->isPost ||
            !Yii::$app->request->isAjax ||
            !isset($post['Code'])
        ) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }

        $model = new Code(['scenario' => Code::SCENARIO_SEARCH]);
        $model->load($post);

        $response = $model->search();
        return json_encode($response);
    }

    /**
     * Shows code list demo view
     *
     * @return string
     */
    public function actionDemo()
    {
        $model = new Code;
        $list = $model->getDemoList();
        $shareToken = $model->getShareTokenString();

        $myData = [
            'data' => $list,
            'messages' => PhpMessageSource::getMessages('frontend'),
            'shareLink' => Url::to(['/code/shared-demo', 'token' => $shareToken], true),
            'defaultLanguageId' => Language::DEFAULT_ID,
        ];

        $myData = Util::stringifyCode($myData);

        $categoryList = Category::getDemoList();
        $languageList = Language::getMappedList();

        return $this->render('demo', [
            'model' => $model,
            'myData' => $myData,
            'languageList' => $languageList,
            'categoryList' => $categoryList,
        ]);
    }

    /**
     * Displays shared code view.
     *
     * @param string $token
     * @throws NotFoundHttpException
     * @return Response|string
     */
    public function actionShared($token)
    {
        $model = Code::findByShareToken($token);

        $myData = [
            'code' => $model->code,
            'messages' => PhpMessageSource::getMessages('frontend'),
        ];

        $myData = Util::stringifyCode($myData);

        return $this->render('shared', [
            'model' => $model,
            'myData' => $myData,
        ]);
    }

    /**
     * Displays shared code view.
     *
     * @param string $token
     * @return Response|string
     */
    public function actionSharedDemo($token)
    {
        $model = Code::getDemoModel();
        $userName = "Adam Smith";

        $myData = [
            'code' => $model->code,
            'messages' => PhpMessageSource::getMessages('frontend'),
        ];

        $myData = Util::stringifyCode($myData);

        return $this->render('shared-demo', [
            'model' => $model,
            'userName' => $userName,
            'myData' => $myData,
        ]);
    }

    /**
     * Returns sharing link for modal.
     *
     * @param integer $id Code id
     * @throws NotAcceptableHttpException
     * @return string
     */
    public function actionGetShareLink($id)
    {
        if (!Yii::$app->request->isPost || !Yii::$app->request->isAjax) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }

        $model = Code::loadModel($id);

        if (!$model->checkOwner()) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'ERROR_WRONG_OWNER'));
        }

        $token = $model->getShareToken();

        return json_encode([
            'message' => 'success',
            'itemShareLink' => Url::to(['code/shared', 'token' => $token], true),
            'itemShareToken' => $token,
            'itemShareData' => Yii::$app->security->maskToken($token),
        ]);
    }

    /**
     * Action saves share link.
     *
     * @throws NotAcceptableHttpException
     * @return string
     */
    public function actionShare()
    {
        $post = Yii::$app->request->post();

        if (!Yii::$app->request->isPost || !Yii::$app->request->isAjax ||
            !isset($post['Code'])
        ) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }

        $model = new Code(['scenario' => Code::SCENARIO_UPDATE]);
        $model->load($post);

        $token =  Yii::$app->security->unmaskToken($model->share_token);

        if (is_null($token) || strlen($token) !== 16) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'ERROR_MESSAGE_INVALID_TOKEN'));
        }

        $model = Code::loadModel($model->id);

        if (!$model->checkOwner()) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'ERROR_WRONG_OWNER'));
        }

        $model->setScenario(Code::SCENARIO_UPDATE);
        $model->share_token = $token;
        $result = $model->save();

        $response = [
            'message' => $result ? 'success' : 'error',
        ];

        if (!$result) {
            $response['errors'] = $model->firstErrors;
        }

        return json_encode($response);
    }

    /**
     * Action removes code share link.
     *
     * @param integer $id Code id
     * @throws NotAcceptableHttpException
     * @return string
     */
    public function actionStopSharing($id)
    {
        if (!Yii::$app->request->isPost || !Yii::$app->request->isAjax) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }

        $model = Code::loadModel($id);

        if (!$model->checkOwner()) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'ERROR_WRONG_OWNER'));
        }

        $model->setScenario(Code::SCENARIO_UPDATE);
        $model->share_token = null;
        $result = $model->save();

        $response = [
            'message' => $result ? 'success' : 'error',
        ];

        if (!$result) {
            $response['errors'] = $model->firstErrors;
        }

        return json_encode($response);
    }
}
