<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\web\NotAcceptableHttpException;
use app\models\User;
use yii\bootstrap4\ActiveForm;
use app\models\UserLog;
use app\models\UserLogAction;
use app\components\PhpMessageSource;
use yii\helpers\Url;

class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'account',
                            'update-account',
                            'change-password',
                            'logout',
                            'verify-email',
                            'email-verification-form',
                            'send-verification-email',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [
                            'login',
                            'validate-email',
                            'signup',
                            'password-reset-request',
                            'password-reset-form',
                            'password-reset-post',
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [
                            'error',
                            'language',
                        ],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'password-reset-post' => ['post'],
                    'send-new-verification-email' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['code/list']);
        }

        $model = new LoginForm;
        $post = Yii::$app->request->post();
        
        if ($model->load($post) && $model->login()) {
            UserLog::create(UserLogAction::ACTION_LOGIN);
            Yii::$app->user->identity->setAccountLanguage();
            return $this->redirect(['code/list']);
        }
        
        if (Yii::$app->request->isPost) {
            $model->password = '';
            
            $user = User::findByUsername($model->username);
            if ($user === null) {
                UserLog::create(UserLogAction::ACTION_LOGIN_FAIL_EMAIL);
            } else {
                UserLog::create(UserLogAction::ACTION_LOGIN_FAIL_PASSWORD, $user->id);
            }
        }
        
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        UserLog::create(UserLogAction::ACTION_LOGOUT);
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays signup view
     *
     * @return Response|string
     */
    public function actionSignup()
    {
        $model = new User(['scenario' => User::SCENARIO_SIGNUP]);
        
        $languageId = User::getLanguageId(Yii::$app->session->get('lang'));
        if (!is_null($languageId)) {
            $model->language = $languageId;
        }
        $post = Yii::$app->request->post();
        
        if ($model->load($post) && $model->validate()) {
            $model->create();
            UserLog::create(UserLogAction::ACTION_SIGNUP, $model->id);
            Yii::$app->session->setFlash(
                'messageSuccess', Yii::t('app', 'SIGNUP_SUCCESS_MESSAGE'));
            return $this->redirect('login');
        }
        
        $model->password = '';
        $model->passwordConfirm = '';
        $languageList = User::getMappedLanguages();
        
        return $this->render('signup', [
            'model' => $model,
            'languageList' => $languageList,
        ]);
    }
    
    /**
     * Returns json response with email validation errors
     *
     * @return string
     */
    public function actionValidateEmail()
    {
        $model = new User;
        $model->scenario = User::SCENARIO_VALIDATE_EMAIL;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            UserLog::create(UserLogAction::ACTION_SIGNUP_VALIDATE_EMAIL);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
   
    /**
     * Changes language and refreshes page.
     *
     * @param string $lang
     * @throws NotAcceptableHttpException
     * @return Response|string
     */
    public function actionLanguage($lang)
    {
        $languageId = User::getLanguageId($lang);
        if (is_null($languageId)) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_REQUEST_ARGUMENT'));
        }
        Yii::$app->session->set('lang', $lang);
        
        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;
            $user->setScenario(User::SCENARIO_UPDATE);
            $user->language = $languageId;
            $user->save();
        }
        
        UserLog::create(UserLogAction::ACTION_CHANGE_LANGUAGE);
        
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    /**
     * Password reset request view
     *
     * @params string|null $email
     * @throws NotAcceptableHttpException
     * @return Response|string
     */
    public function actionPasswordResetRequest($email = null)
    {
        $model = new User(['scenario' => User::SCENARIO_PASSWORD_RESET_REQUEST]);
        
        if (!Yii::$app->request->isPost) {
            $myData = [
                'messages' => PhpMessageSource::getMessages('frontend'),
                'email' => base64_decode($email),
            ];

            return $this->render('password-reset-request', [
                'model' => $model,
                'myData' => json_encode($myData),
            ]);
        }
        
        if (!Yii::$app->request->isAjax) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }
        
        $post = Yii::$app->request->post();
        
        if ($model->load($post)) {
            $model->validate();
        }
        
        if ($model->hasErrors()) {
            return json_encode([
                'message' => 'error',
                'errors' => $model->firstErrors['email'],
            ]);
        }
        
        if ($model->sendRecoveryEmail()) {
            $user = User::findByUsername($model->email);
            UserLog::create(UserLogAction::ACTION_PASSWORD_RESET_REQUEST, $user->id);
            
            Yii::$app->session->setFlash('tokenSent',
                Yii::t('app', 'PASSWORD_REQUEST_SENT_TEXT'));
            return json_encode([
                'message' => 'success',
            ]);
        }
        
        return json_encode([
            'message' => 'error',
        ]);
    }
    
    /**
     * Checks password reset token and shows password change form
     *
     * @return Response|string
     */
    public function actionPasswordResetForm($token)
    {
        $model = User::findByToken('password_reset_token', $token, true);
        $expired = is_null($model) ? null : $model->isTokenExpired('password_reset_token');
        
        if ($expired === null) {
            Yii::$app->session->setFlash('errorMessage',
                Yii::t('app', 'ERROR_MESSAGE_INVALID_TOKEN'));
            return $this->redirect(['/site/message']);
        }
        
        if ($expired) {
            Yii::$app->session->setFlash('errorMessage',
                Yii::t('app', 'ERROR_MESSAGE_EXPIRED_TOKEN'));
            return $this->redirect(['/site/message']);
        }

        $model->setScenario(User::SCENARIO_SECURITY);
        $model->password_reset_token = Yii::$app->security
            ->generateRandomString();
        $model->save();
        
        $model->setScenario(User::SCENARIO_PASSWORD_RESET);
        
        $model->setAccountLanguage();
        
        return $this->render('password-reset', [
            'model' => $model,
            'key' => Yii::$app->security->maskToken($model->password_reset_token),
        ]);
    }

    /**
     * Password reset request view
     *
     * @return Response|string
     */
    public function actionPasswordResetPost()
    {        
        $post = Yii::$app->request->post();
        $token = Yii::$app->request->post('tag', '');
        
        $model = User::findByToken('password_reset_token', $token, true);
        
        if ($model === null) {
            Yii::$app->session->setFlash('errorMessage',
                Yii::t('app', 'INVALID_REQUEST'));
            return $this->redirect(['/site/message']);
        }
        
        $model->setScenario(User::SCENARIO_PASSWORD_RESET);
        $model->password_reset_token = null;
        
        if ($model->load($post) && $model->validate()) {
            UserLog::create(UserLogAction::ACTION_PASSWORD_RESET, $model->id);
            $model->setPassword();
            Yii::$app->session->setFlash('messageSuccess',
                Yii::t('app', 'PASSWORD_CHANGE_SUCCESS_MESSAGE'));
            return $this->redirect(['/user/login']);
        }
    }
    
    /**
     * Shows email verification message view
     *
     * @throws NotAcceptableHttpException
     * @return Response|string
     */
    public function actionEmailVerificationForm()
    {
        $model = User::findIdentity(Yii::$app->user->identity->id);
        $model->setScenario(User::SCENARIO_RESEND_EMAIL_VERIFICATION);
        
        if (!Yii::$app->request->isPost) {
            $myData = [
                'messages' => PhpMessageSource::getMessages('frontend'),
                'email' => $model->email,
            ];

            return $this->render('email-verification-form', [
                'model' => $model,
                'myData' => json_encode($myData),
            ]);
        }
    }
    
    /**
     * Sends another email verification link to user
     *
     * @throws NotAcceptableHttpException
     * @return Response|string
     */
    public function actionSendVerificationEmail()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }
        
        $post = Yii::$app->request->post();
        $model = User::findIdentity(Yii::$app->user->identity->id);
        $model->setScenario(User::SCENARIO_RESEND_EMAIL_VERIFICATION);
        $currentEmail = $model->email;
        if ($model->load($post) && $model->validate() &&
            $currentEmail !== $model->email
        ) {
            $model->save();
        }
        
        if ($model->hasErrors()) {
            return json_encode([
                'message' => 'error',
                'error' => $model->firstErrors['email'],
            ]);
        }
        
        if ($model->sendVerificationEmail()) {
            return json_encode([
                'message' => 'success',
            ]);
        }
        
        return json_encode([
            'message' => 'error',
            'error' => Yii::t('app', 'EMAIL_SEND_FAILED'),
        ]);
    }
    
    /**
     * Verifies user's email by specified token
     *
     * @return Response|string
     */
    public function actionVerifyEmail($token)
    {
        $model = User::findByToken('email_verification_token', $token , true);
        
        if (!is_null($model) && $model->verifyIdentity()) {
            $model->email_verification_token = null;
            $model->setScenario(User::SCENARIO_SECURITY);
            $model->save();
            return $this->redirect(['code/list']);
        }
        
        Yii::$app->session->setFlash('errorMessage',
            Yii::t('app', 'ERROR_MESSAGE_INVALID_TOKEN'));
        return $this->redirect(['/site/message']);
    }
    
    /**
     * Render user's account view
     *
     * @return Response|string
     */
    public function actionAccount()
    {
        $model = User::findIdentity(Yii::$app->user->identity->id);
        $model->setScenario(User::SCENARIO_UPDATE);
        
        $languageList = User::getMappedLanguages();

        $myData = [
            'messages' => PhpMessageSource::getMessages('frontend'),
            'values' => [
                'name' => $model->name,
                'email' => $model->email,
                'language' => $model->language,
            ],
        ];

        return $this->render('account', [
            'model' => $model,
            'languageList' => $languageList,
            'myData' => addslashes(json_encode($myData)),
        ]);
    }
    
    /**
     * Updates user account details
     *
     * @throws NotAcceptableHttpException
     * @return Response|string
     */
    public function actionUpdateAccount()
    {
        if (!Yii::$app->request->isPost || !Yii::$app->request->isAjax) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_REQUEST'));
        }
        
        $post = Yii::$app->request->post();
        $model = User::findIdentity(Yii::$app->user->identity->id);
        $model->setScenario(User::SCENARIO_UPDATE);
        $currentEmail = $model->email;
        
        
        if ($model->load($post) && $model->validate() &&
            !empty($model->getDirtyAttributes())
        ) {
            $model->save();
            UserLog::create(UserLogAction::ACTION_UPDATE_ACCOUNT);
        }
        
        if ($model->hasErrors()) {
            $errorList = '<ul class="error-list">';
            
            $i = 0;
            foreach ($model->firstErrors as $error) {
                $errorList .= "<li>{$error}</li>";
                $i++;
                if ($i == 2) {
                    break;
                }
            }
            $errorList .= '</ul>';
          
            return json_encode([
                'message' => 'error',
                'errors' => $errorList,
            ]);
        }
        
        $emailChanged = $currentEmail !== $model->email;
        
        if (!$emailChanged || $model->sendVerificationEmail()) {
            return json_encode([
                'message' => 'success',
                'redirect' => $emailChanged ? Url::to(['/user/email-verification-form']) : null,
            ]);
        }
        
        return json_encode([
            'message' => 'error',
            'error' => Yii::t('app', 'EMAIL_SEND_FAILED'),
        ]);
    }
    
    /**
     * Changes user's password
     *
     * @throws NotAcceptableHttpException
     * @return Response|string
     */
    public function actionChangePassword()
    {
        if (!Yii::$app->request->isPost || !Yii::$app->request->isAjax) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_REQUEST'));
        }
        
        $post = Yii::$app->request->post();
        $model = User::findIdentity(Yii::$app->user->identity->id);
        $model->setScenario(User::SCENARIO_CHANGE_PASSWORD);
        
        if ($model->load($post) && $model->validate()) {
            $model->setPassword();
            UserLog::create(UserLogAction::ACTION_PASSWORD_CHANGE);
        }
        
        if ($model->hasErrors()) {
            $errorList = '<ul class="error-list">';
            
            $i = 0;
            foreach ($model->firstErrors as $error) {
                $errorList .= "<li>{$error}</li>";
                $i++;
                if ($i == 2) {
                    break;
                }
            }
            $errorList .= '</ul>';
          
            return json_encode([
                'message' => 'error',
                'errors' => $errorList,
            ]);
        }
        
        return json_encode([
            'message' => 'success',
        ]);
    }
}
