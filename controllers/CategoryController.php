<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\Category;
use yii\web\NotAcceptableHttpException;
use app\components\PhpMessageSource;
use app\models\UserLog;
use app\models\UserLogAction;
use app\components\Util;
use yii\widgets\ActiveForm;

class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'list',
                            'create',
                            'update',
                            'update-order',
                            'remove',
                            'validate',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
   
    /**
     * Displays code snippets page.
     *
     * @return Response|string
     */
    public function actionList()
    {
        Category::reorder();
        $model = new Category(['scenario' => Category::SCENARIO_CREATE]);
        
        $items = Category::getUserCategories();
        
        $myData = [
            'messages' => PhpMessageSource::getMessages('frontend'),
            'data' => $items,
        ];
        
        $myData = Util::stringifyCode($myData);        
        
        return $this->render('list', [
            'model' => $model,
            'myData' => $myData,
        ]);
    }
    
    /**
     * Creates category record
     *
     * @throws NotAcceptableHttpException
     * @return string json
     */
    public function actionCreate()
    {
        $post = Yii::$app->request->post();
        
        if (!Yii::$app->request->isPost || 
            !Yii::$app->request->isAjax ||
            !isset($post['Category'])
        ) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }
      
        $model = new Category(['scenario' => Category::SCENARIO_CREATE]);
        $model->load($post);
        $model->user_id = Util::currentUserId();
        $model->reorderForNew();
        $result = $model->save();
        
        if ($result) {
            UserLog::create(UserLogAction::ACTION_CATEGORY_CREATE);
        }
        
        $items = Category::getUserCategories();
        
        $response = [
            'message' => $result ? 'success' : 'error',
            'items' => $items,
            'id' => $model->id,
        ];
        
        return json_encode($response);
    }
    
    /**
     * Updates category record
     *
     * @throws NotAcceptableHttpException
     * @return string json
     */
    public function actionUpdate()
    {
        $post = Yii::$app->request->post();
        
        if (!Yii::$app->request->isPost || 
            !Yii::$app->request->isAjax ||
            !isset($post['Category'])
        ) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }
        
        $model = Category::loadModel($post['Category']['id']);
        
        if (!$model->checkOwner()) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'ERROR_WRONG_OWNER'));
        }
      
        $model->setScenario(Category::SCENARIO_UPDATE);
        $model->load($post);
        $result = $model->save();
        
        if ($result) {
            UserLog::create(UserLogAction::ACTION_CATEGORY_UPDATE);
        }
        
        $attributes = $model->attributes;
        $attributes['isAssigned'] = count($model->codes) !== 0;
        
        $response = [
            'message' => $result ? 'success' : 'error',
            'item' => $attributes,
        ];
        
        return json_encode($response);
    }
    
    /**
     * Removes code record
     *
     * @param integer $id Model id
     * @throws NotAcceptableHttpException
     * @return string json
     */
    public function actionRemove($id)
    {
        if (!Yii::$app->request->isPost || 
            !Yii::$app->request->isAjax
        ) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }
      
        $model = Category::loadModel($id);
        
        if (!$model->checkOwner()) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'ERROR_WRONG_OWNER'));
        }
        
        $result = $model->deleteModel();
        
        if ($result) {
            UserLog::create(UserLogAction::ACTION_CATEGORY_DELETE);
        }
        
        return json_encode([
            'message' => $result ? 'success' : 'error',
        ]);
    }
    
    /**
     * Validates category record
     *
     * @throws NotAcceptableHttpException
     * @return string json
     */
    public function actionValidate()
    {
        $post = Yii::$app->request->post();
        
        if (!Yii::$app->request->isPost || 
            !Yii::$app->request->isAjax ||
            !isset($post['Category'])
        ) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }
      
        $model = new Category(['scenario' => Category::SCENARIO_UPDATE]);
        $model->load($post);
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
    }
}
