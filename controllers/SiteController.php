<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\ContactForm;
use app\models\UserLog;
use app\models\UserLogAction;
use yii\web\NotAcceptableHttpException;
use app\components\PhpMessageSource;
use app\components\Util;

class SiteController extends Controller
{
    /** @const MESSAGE_SUCCESS Message type */
    const MESSAGE_SUCCESS = 1;
    
    /** @const MESSAGE_ERROR Message type */
    const MESSAGE_ERROR = 2;
  
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'error',
                            'message',
                            'contact',
                            'captcha',
                            'privacy-policy',
                            'cookie-consent',
                            'test-it',
                        ],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'app\components\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        UserLog::create(UserLogAction::ACTION_VIEW_INDEX);
        return $this->render('index');
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        if (Yii::$app->request->isPost && !Yii::$app->request->isAjax) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }
      
        $model = new ContactForm;
        $post = Yii::$app->request->post();
        
        if ($model->load($post) && $model->contact(Yii::$app->params['adminEmail'])) {
            return json_encode([
                'message' => 'success',
            ]);
        } elseif (Yii::$app->request->isPost) {
            return json_encode([
                'message' => 'error',
            ]);
        }
        
        $myData = [
            'messages' => PhpMessageSource::getMessages('frontend'),
        ];
        
        return $this->render('contact', [
            'model' => $model,
            'myData' => json_encode($myData),
        ]);
    }
    
    /**
     * Displays success or error message page.
     *
     * @return Response|string
     */
    public function actionMessage()
    {
        $type = null;
        
        if (Yii::$app->session->hasFlash('successMessage')) {
            $message = Yii::$app->session->getFlash('successMessage');
            $type = static::MESSAGE_SUCCESS;
        }
        
        if (Yii::$app->session->hasFlash('errorMessage')) {
            $message = Yii::$app->session->getFlash('errorMessage');
            $type = static::MESSAGE_ERROR;
        }
        
        if ($type !== null) {
            return $this->render('message', [
                'message' => $message,
                'type' => $type,
            ]);
        } else {
            return $this->redirect(['/site/index']);
        }
    }

    /**
     * Displays privacy policy page.
     *
     * @return Response|string
     */    
    public function actionPrivacyPolicy() {
        return $this->render('privacy-policy');
    }
    
    /**
     * Displays privacy policy page.
     *
     * @return Response|string
     */    
    public function actionCookieConsent() {
        if (Yii::$app->request->isPost && !Yii::$app->request->isAjax) {
            throw new NotAcceptableHttpException(
                Yii::t('app', 'BAD_POST_REQUEST'));
        }
        
        Yii::$app->session->set('cookieConsent', true);
        
        return json_encode([
            'message' => 'success',
        ]);
    }
    
    public function actionTestIt() {
        \app\models\Category::reorderDuplicated();
        exit;
        
        Util::sendMail([
            'email' => 'to@gmail.com',
            'name' => 'Name',
            'replyEmail' => 'reply@gmail.com',
            'replyName' => 'Webmaster',
            'subject' => 'Subject',
            'message' => 'Message',
        ]);
        
        exit;
        
        Yii::$app->mailer->compose()
            ->setTo("to@gmail.com")
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
            ->setReplyTo(["reply@gmail.com" => "reply@gmail.com"])
            ->setSubject("Subject")
            ->setTextBody("Message")
            ->send();
        
        exit;
    }
}
